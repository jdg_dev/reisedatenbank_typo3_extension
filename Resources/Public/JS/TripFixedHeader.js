/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 12.04.16
 * Time: 11:53
 */
var tripHeader = $('#tripHeader');
var tripHeaderObject = $(tripHeader.html());
var tripHeaderFixedOffset = $('#tripHeaderFixed').offset();
var tripHeaderShowOn = tripHeaderFixedOffset.top;
var tripHeaderIsShown = false;

if (tripHeader.length > 0) {
    $('body').append(tripHeaderObject);
}

$(window).scroll(function() {
    var scrollPos = $(window).scrollTop();
    if (scrollPos >= tripHeaderShowOn && !tripHeaderIsShown) {
        tripHeaderObject.addClass('show');
        tripHeaderIsShown = true;
    }
    if (scrollPos < tripHeaderShowOn && tripHeaderIsShown) {
        tripHeaderObject.removeClass('show');
        tripHeaderIsShown = false;
    }
});

