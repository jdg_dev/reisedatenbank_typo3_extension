/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 16.01.14
 * Time: 14:58
 */


jQuery = $;
var App = function() {
    var self = this;

    // Backbone model und collection für dates bauen
    var DateModel = Backbone.Model.extend({
        urlRoot: this.rootURL,
        idAttribute: 'uid'
    });

    var Dates = Backbone.Collection.extend({
        url: this.rootURL,
        model: DateModel
    });
    this.dates = new Dates();
    this.dates.on('change', function() { self.userListView.render(false); });

    var UserListView = Backbone.View.extend({
        el: '.app',
        events: {
            'click #previous': 'previousYear',
            'click #next': 'nextYear',
            'click .btn': 'switchTool',
            'mousemove .day.border': 'move',
            'click .day.border': 'click',
            'mouseover .day.title': 'mouseoverWeekday',
            'mouseout .day.title': 'mouseoutWeekday',
            'click .day.title': 'clickWeekday',
            'mouseover .monthTitle': 'mouseoverMonth',
            'mouseout .monthTitle': 'mouseoutMonth',
            'click .monthTitle': 'clickMonth',
        },

        mouseoverWeekday: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            var weekday = jQuery(ev.currentTarget).data('weekday');
            jQuery('.weekday'+weekday+'.month'+month).addClass('borderHover');
        },

        mouseoutWeekday: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            var weekday = jQuery(ev.currentTarget).data('weekday');
            jQuery('.weekday'+weekday+'.month'+month).removeClass('borderHover');
        },

        clickWeekday: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            var weekday = jQuery(ev.currentTarget).data('weekday');
            jQuery('.weekday'+weekday+'.month'+month).each(function() {
                jQuery(this).addClass('groupWait');
                jQuery('.month').addClass('halfTone');
                _.defer(function(that, self) {
                    var id = jQuery(that).data('id');
                    var model = self.dates.get(id);
                    var setStatus = jQuery('.btn.active').data('status');
                    var getStatus = model.get('status');
                    if (
                        parseInt(setStatus) != parseInt(getStatus)
                    ) {
                        model.set({
                            status: parseInt(setStatus)
                        }).save();
                    }
                }, this, self);
            });
        },

        mouseoverMonth: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            jQuery('.day.border.month'+month).addClass('borderHover');
        },

        mouseoutMonth: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            jQuery('.day.border.month'+month).removeClass('borderHover');
        },

        clickMonth: function(ev) {
            var month = jQuery(ev.currentTarget).data('month');
            jQuery('.day.border.month'+month).each(function() {
                jQuery(this).addClass('groupWait');
                jQuery('.month').addClass('halfTone');
                _.defer(function(that, self) {
                    var id = jQuery(that).data('id');
                    var model = self.dates.get(id);
                    var setStatus = jQuery('.btn.active').data('status');
                    var getStatus = model.get('status');
                    if (
                        parseInt(setStatus) != parseInt(getStatus)
                    ) {
                        model.set({
                            status: parseInt(setStatus)
                        }).save();
                    }
                }, this, self);
            });
        },

        move: function(ev) {
            if (ev.which == 1) {
                var id = jQuery(ev.currentTarget).data('id');
                var model = self.dates.get(id);
                var setStatus = jQuery('.btn.active').data('status');
                var getStatus = model.get('status');
                if (
                    parseInt(setStatus) != parseInt(getStatus)
                ) {
                    model.set({
                        status: parseInt(setStatus)
                    }).save();
                }
            }
        },
        click: function(ev) {
            var id = jQuery(ev.currentTarget).data('id');
            var model = self.dates.get(id);
            var setStatus = jQuery('.btn.active').data('status');
            var getStatus = model.get('status');
            if (
                parseInt(setStatus) != parseInt(getStatus)
                ) {
                model.set({
                    status: parseInt(setStatus)
                }).save();
            }
        },
        switchTool: function(ev) {
            ev.preventDefault();
            jQuery('.btn').removeClass('active');
            jQuery(ev.currentTarget).addClass('active');
            self.modeId = jQuery(ev.currentTarget).attr('id');
        },
        previousYear: function (ev) {
            ev.preventDefault();
            var year = self.year-1;
            self.setYear(year);
        },
        nextYear: function (ev) {
            ev.preventDefault();
            var year = self.year+1;
            self.setYear(year);
        },
        render: function (doFetch) {
            var that = this;
            if (doFetch) {
            self.dates.fetch({
                data: { eventPackage_uid: eventPackage_uid, year: self.year},
                success: function(dates) {
                    var template = _.template(jQuery('#eventPackageCalendarView').html());
                    var content = template({dates: dates.models, year: self.year, moment: moment});
                    that.$el.html(content);
                    jQuery('#'+self.modeId).addClass('active');
                }
            })
            } else {
                var template = _.template(jQuery('#eventPackageCalendarView').html());
                var content = template({dates: self.dates.models, year: self.year, moment: moment});
                that.$el.html(content);
                jQuery('#'+self.modeId).addClass('active');

            }
        }
    });
    this.userListView = new UserListView();

    // Initialjahr bestimmen
    var date = new Date();
    this.setYear(date.getFullYear());

}
App.prototype = {
    year: 0,
    rootURL: "/typo3conf/ext/reisedb/Resources/Public/JS/api/dates",
    modeId: 'clear',
    setYear: function(year) {
        this.year = year;
        this.userListView.render(true);
    }
}

if (eventPackage_uid > 0) {
    var app = new App();
}
