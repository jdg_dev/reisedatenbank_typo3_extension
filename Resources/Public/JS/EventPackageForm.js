/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 14.04.16
 * Time: 14:04
 */


$('.panelTriggerCheckbox').each(showHideEventPackageContainer);
$('.panelTriggerCheckbox').click(showHideEventPackageContainer);
function showHideEventPackageContainer() {
    var checkbox = $(this);
    if (checkbox.is(':checked')) {
        $('#'+checkbox.data('bind')).collapse('show');
    } else {
        $('#'+checkbox.data('bind')).collapse('hide');
    }
}

function showExtraDataBasedOnDate(uid) {
    $date = $('#extraDataDatepicker'+uid).val();
    var content = '';
    $('#panel'+uid).find('.extraDataSet').each(function() {
        var date = $(this).data('for');
        if (date == $date) {
            content = $(this).html();
        }
    });
    $('#panel'+uid).find('.extraDataContent').empty().append(content);
}

$('.extraDataGroup').each(function() {
    showExtraDataBasedOnDate($(this).data('uid'));
});

$('.extraDataDatepicker').change(function() {
    var uid = $(this).data('uid');
    showExtraDataBasedOnDate(uid);
});

function setAllParticipants() {
    if ($(this).is(':checked')) {
        $('.'+$(this).data('target')).prop('checked', true);
    } else {
        $('.'+$(this).data('target')).prop('checked', false);
    }
}

function checkAllParticipants() {
    var group = $('.'+$(this).data('group'));
    if (group.length == group.filter(':checked').length) {
        $('.'+$(this).data('target')).prop('checked', true);
    } else {
        $('.'+$(this).data('target')).prop('checked', false);
    }
}

$('.personChackbox').each(checkAllParticipants);
$('.allpersonChackbox').click(setAllParticipants);
$('.personChackbox').click(checkAllParticipants);


