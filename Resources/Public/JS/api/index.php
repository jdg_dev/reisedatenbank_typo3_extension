<?php

require 'Slim/Slim.php';
header("Cache-Control: no-cache, must-revalidate");
$app = new Slim();

$app->get('/dates',	'getDates');
$app->put('/dates/:uid', 'updateDate');
$app->get('/eventdates',	'getEventDates');
$app->put('/eventdates/:uid', 'updateEventDate');
$app->run();

function getDates() {

    $eventPackage_uid = $_GET['eventPackage_uid'];
    $year = $_GET['year'];



	$sql = "SELECT * FROM tx_reisedb_domain_model_bookabledate WHERE trip=:trip AND DATE_FORMAT(FROM_UNIXTIME(`date`), '%Y') = :year ORDER BY `date`";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("trip", $eventPackage_uid);
		$stmt->bindParam("year", $year);
		$stmt->execute();
		$wine = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (!$wine) {
            $db = null;
            createDefaultDatesForYear($eventPackage_uid, $year);
        } else {
            $db = null;
            echo json_encode($wine);
        }
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getEventDates() {

	$eventPackage_uid = $_GET['eventPackage_uid'];
	$year = $_GET['year'];



	$sql = "SELECT * FROM tx_reisedb_domain_model_eventpackageavailability WHERE event_package=:event_package AND DATE_FORMAT(FROM_UNIXTIME(dayofavailability), '%Y') = :year ORDER BY dayofavailability";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("event_package", $eventPackage_uid);
		$stmt->bindParam("year", $year);
		$stmt->execute();
		$wine = $stmt->fetchAll(PDO::FETCH_OBJ);
		if (!$wine) {
			$db = null;
			createDefaultEventDatesForYear($eventPackage_uid, $year);
		} else {
			$db = null;
			echo json_encode($wine);
		}
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function createDefaultDatesForYear($eventPackage_uid, $year) {
    $dayofavailability = mktime(0,0,0,1,1,$year);
    while (date('Y', $dayofavailability) == $year) {
        $sql = "INSERT INTO tx_reisedb_domain_model_bookabledate (trip, `date`) VALUES (:trip, :date)";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("trip", $eventPackage_uid);
            $stmt->bindParam("date", $dayofavailability);
            $stmt->execute();
            //$id = $db->lastInsertId();
            $db = null;
            $dayofavailability = $dayofavailability + 60 * 60 * 24;
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }
    getDates($eventPackage_uid, $year);
}

function fillDatesForYear($eventPackage_uid, $year) {
	$dayofavailability = mktime(0,0,0,1,1,$year);
	while (date('Y', $dayofavailability) == $year) {
		try {
			$db = getConnection();
			$sql = "SELECT * FROM tx_reisedb_domain_model_bookabledate WHERE `date` = :date";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("date", $dayofavailability);
			$stmt->execute();
			$day = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
			if (!$day) {
				echo date('d.m.Y', $dayofavailability).'-';
				/*
				$sql = "INSERT INTO tx_reisedb_domain_model_bookabledate (trip, `date`, status`) VALUES (:trip, :date` 1)";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("trip", $eventPackage_uid);
				$stmt->bindParam("date", $dayofavailability);
				$stmt->execute();
				*/
			}



			//$id = $db->lastInsertId();
			$db = null;
			$dayofavailability = $dayofavailability + 60 * 60 * 24;
		} catch(PDOException $e) {
			echo '{"error":{"text":'. $e->getMessage() .'}}';
		}
	}
	getDates($eventPackage_uid, $year);
}

function createDefaultEventDatesForYear($eventPackage_uid, $year) {
	$dayofavailability = mktime(0,0,0,1,1,$year);
	while (date('Y', $dayofavailability) == $year) {
		$sql = "INSERT INTO tx_reisedb_domain_model_eventpackageavailability (event_package, dayofavailability) VALUES (:event_package, :dayofavailability)";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("event_package", $eventPackage_uid);
			$stmt->bindParam("dayofavailability", $dayofavailability);
			$stmt->execute();
			//$id = $db->lastInsertId();
			$db = null;
			$dayofavailability = $dayofavailability + 60 * 60 * 24;
		} catch(PDOException $e) {
			echo '{"error":{"text":'. $e->getMessage() .'}}';
		}
	}
	getEventDates($eventPackage_uid, $year);
}

function updateDate($uid) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$date = json_decode($body);
	$sql = "UPDATE tx_reisedb_domain_model_bookabledate SET
	trip=:trip,
	`date`=:date,
	`status`=:status,
	`hidden`=:hidden
	WHERE uid=:uid";
	$hidden = 0;
	if ($date->status == 0) {
		$hidden = 1;
	}
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("trip", $date->trip);
		$stmt->bindParam("date", $date->date);
		$stmt->bindParam("status", $date->status);
		$stmt->bindParam("hidden", $hidden);
		$stmt->bindParam("uid", $uid);
		$stmt->execute();
		$db = null;
		echo json_encode($date);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function updateEventDate($uid) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$date = json_decode($body);
	$sql = "UPDATE tx_reisedb_domain_model_eventpackageavailability SET
	event_package=:event_package,
	dayofavailability=:dayofavailability,
	available=:available
	WHERE uid=:uid";
	$hidden = 0;
	if ($date->available == 0) {
		$hidden = 1;
	}
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("event_package", $date->event_package);
		$stmt->bindParam("dayofavailability", $date->dayofavailability);
		$stmt->bindParam("available", $date->available);
		$stmt->bindParam("uid", $uid);
		$stmt->execute();
		$db = null;
		echo json_encode($date);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getConnection() {
	$dirname = dirname(__FILE__);
	$config = include substr($dirname, 0, strpos($dirname, 'typo3conf')).'typo3conf/LocalConfiguration.php';
	$dbhost=$config['DB']['host'];
	$dbuser=$config['DB']['username'];
	$dbpass=$config['DB']['password'];
	$dbname=$config['DB']['database'];
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}