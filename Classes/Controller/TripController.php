<?php
namespace JAKOTA\Reisedb\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * TripController
 */
class TripController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	private $formconfig = array();

	/**
	 * tripRepository
	 *
	 * @var \JAKOTA\Reisedb\Domain\Repository\TripRepository
	 * @inject
	 */
	protected $tripRepository = NULL;

    /**
     * reviewRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\ReviewRepository
     * @inject
     */
    protected $reviewRepository = NULL;

    /**
     * pageRenderer
     *
     * @var \TYPO3\CMS\Core\Page\PageRenderer
     * @inject
     */
    protected $pageRenderer;

	/**
	 * categoriesRepository
	 *
	 * @var \JAKOTA\Reisedb\Domain\Repository\CategoriesRepository
	 * @inject
	 */
	protected $categoriesRepository = NULL;

    /**
     * roomcategoryRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\RoomcategoryRepository
     * @inject
     */
    protected $roomcategoryRepository = NULL;

    /**
     * pricecategoryRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\PricecategoryRepository
     * @inject
     */
    protected $pricecategoryRepository = NULL;

    /**
     * eventPackageExtraDataRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\EventPackageExtraDataRepository
     * @inject
     */
    protected $eventPackageExtraDataRepository = NULL;

    /**
     * eventPackageFixedPriceRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\EventPackageFixedPriceRepository
     * @inject
     */
    protected $eventPackageFixedPriceRepository = NULL;

    /**
     * eventPackageRoomPriceRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\EventPackageRoomPriceRepository
     * @inject
     */
    protected $eventPackageRoomPriceRepository = NULL;

    /**
     * eventPackageScaledPriceRepository
     *
     * @var \JAKOTA\Reisedb\Domain\Repository\EventPackageScaledPriceRepository
     * @inject
     */
    protected $eventPackageScaledPriceRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     * @inject
     */
    protected $imageService;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        $trips = $this->tripRepository->findAll();
		$this->view->assign('trips', $trips);
	}

	/**
	 * action wordExport
	 *
	 * @return void
	 */

	private function rp($string) {
		$string = str_replace(chr(150), '-', $string);
		$string = str_replace(chr(128), 'EUR', $string);
		$string = str_replace(chr(145), "'", $string);
		$string = str_replace(chr(146), "'", $string);
		$string = str_replace(chr(147), '"', $string);
		$string = str_replace(chr(148), '"', $string);
		return $string;
	}

    public function getPriceByRoomCategory($room, $category, $prices) {
        foreach($prices as $price) {
            if($price->getRoomcategory()->getUid() == $room->getUid() && $price->getPricecategory()->getUid() == $category->getUid()) {
                return $price;
            }
        }
        return null;
    }

	public function wordExportAction() {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        if($this->request->hasArgument('tripToRender')) {
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
            header("Cache-Control: no-cache");
            header("Pragma: no-cache");
            header("Cache-Control: post-check=0, pre-check=0", FALSE);
			/**
			 * @var $trip \JAKOTA\Reisedb\Domain\Model\Trip
			 */
			$trip = $this->tripRepository->findByUidNoEnableFields($this->request->getArgument('tripToRender'));
			require_once 'typo3conf/ext/reisedb/Resources/PHP/PHPWord/PHPWord.php';

			// Create a new PHPWord Object
			$PHPWord = new \PHPWord();

			//Einstellung
			$properties = $PHPWord->getProperties();
			$properties->setCreator('Mecklenburger Radtour');
			$properties->setCompany('Mecklenburger Radtour');
			$properties->setCreated(time());
			$properties->setModified(time());

			// Defautlt Schriftart und Schriftgröße bestimmen
			$PHPWord->setDefaultFontName('Verdana');
			$PHPWord->setDefaultFontSize(12);

			// Every element you want to append to the word document is placed in a section. So you need a section:
			$section = $PHPWord->createSection();
			if ($GLOBALS['TSFE']->sys_language_uid == 0) {
				// Header
				$headerSection = $section->createHeader();
				$headerSection->addText('Die Mecklenburger Radtour', array('size' => 16, 'bold' => true, 'color' => '008000'), array('align' => 'center', 'spaceAfter' => 0));
				$headerSection->addText('Rad - und Wanderreisen', array('size' => 11, 'bold' => true, 'color' => '008000'), array('align' => 'center', 'spaceAfter' => 0));
				$imageStyle = array('width'=>72, 'height'=>66, 'align'=>'center');
				$headerSection->addImage('typo3conf/ext/reisedb/Resources/Public/PDFAssets/mrt_logo.jpg', $imageStyle);

				// Footer
				$footerSection = $section->createFooter();
				$footerSection->addText('Die Mecklenburger Radtour GmbH, Inhaber Thomas Eberl', array('size' => 10, 'bold' => true, 'color' => '008000'), array('align' => 'left', 'spaceAfter' => 0));
				$footerSection->addText('Zunftstrasse 4, D-18437 Stralsund', array('size' => 10, 'bold' => true, 'color' => '008000'), array('align' => 'left', 'spaceAfter' => 0));
				$table = $footerSection->addTable();
				$table->addRow();
				$cell = $table->addCell(6000);
				$cell->addText('Telefon: 0049-3831-30676-0', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$cell = $table->addCell(6000);
				$cell->addText('Telefax: 0049-3831-30676-19', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$table->addRow();
				$cell = $table->addCell(6000);
				$cell->addText('Email: info@mecklenburger-radtour.de', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$cell = $table->addCell(6000);
				$cell->addText('Internet: www.mecklenburger-radtour.de', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
			} else {
				// Header
				$headerSection = $section->createHeader();
				$headerSection->addText('Mecklenburger Radtour', array('size' => 16, 'bold' => true, 'color' => '008000'), array('align' => 'center', 'spaceAfter' => 0));
				$headerSection->addText('Cycling and Walking Holidays', array('size' => 11, 'bold' => true, 'color' => '008000'), array('align' => 'center', 'spaceAfter' => 0));
				$imageStyle = array('width'=>72, 'height'=>66, 'align'=>'center');
				$headerSection->addImage('typo3conf/ext/reisedb/Resources/Public/PDFAssets/mrt_logo.jpg', $imageStyle);

				// Footer
				$footerSection = $section->createFooter();
				$footerSection->addText('Mecklenburger Radtour, Thomas Eberl', array('size' => 10, 'bold' => true, 'color' => '008000'), array('align' => 'left', 'spaceAfter' => 0));
				$footerSection->addText('Zunftstrasse 4, D-18437 Stralsund', array('size' => 10, 'bold' => true, 'color' => '008000'), array('align' => 'left', 'spaceAfter' => 0));
				$table = $footerSection->addTable();
				$table->addRow();
				$cell = $table->addCell(6000);
				$cell->addText('Phone: 0049-3831-30676-0', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$cell = $table->addCell(6000);
				$cell->addText('Fax: 0049-3831-30676-19', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$table->addRow();
				$cell = $table->addCell(6000);
				$cell->addText('Email: info@mecklenburger-radtour.de', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
				$cell = $table->addCell(6000);
				$cell->addText('Internet: www.mecklenburger-radtour.de', array('size' => 9, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
			}

			//Titel der Reise
			$section->addText(utf8_decode($trip->getTitle()), array('size' => 24, 'bold' => true, 'color' => '17365D'), array('align' => 'center'));

            // Reisedauer
            $nights = $trip->getNights();
            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                $section->addText(utf8_decode(($nights+1).' Tage / '.$nights.' Nächte'), array('size' => 12, 'bold' => true, 'color' => '17365D'), array('align' => 'center'));
            } else {
                $section->addText(utf8_decode(($nights+1).' Days / '.$nights.' Nights'), array('size' => 12, 'bold' => true, 'color' => '17365D'), array('align' => 'center'));
            }

            // Reisebild
            if ($trip->getImages()) {
                $c = 0;
                foreach($trip->getImages() as $image) {

                    /**
                     * @var $imageService \TYPO3\CMS\Extbase\Service\ImageService
                     */
                    $imageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Service\ImageService');
                    $imageReference = $imageService->getImage('', $image, true);
                    $processingInstructions = array(
                        'maxWidth' => 300,
                        'maxHeight' => 200,
                    );
                    $processedImage = $imageReference->getOriginalFile()->process(\TYPO3\CMS\Core\Resource\ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, $processingInstructions);
                    $imageProperties = $processedImage->getProperties();
                    $imageUri = $processedImage->getPublicUrl();
                    $section->addImage($_SERVER['DOCUMENT_ROOT'].'/'.$imageUri, array(
                        'width' => $imageProperties['width'],
                        'height' => $imageProperties['height']
                    ));
                    $c++;
                    if ($c > 0) break;
                }
            }

            // Einleitungstext
            $section->addText(utf8_decode(strip_tags($trip->getDescription())), array('size' => 12, 'bold' => false, 'color' => '000000'), array('align' => 'left'));

            //Tourenverlauf
            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                $section->addText('Tourenverlauf', array('size' => 24, 'bold' => true, 'color' => '000000'), array('align' => 'center'));
            } else {
                $section->addText('Route', array('size' => 24, 'bold' => true, 'color' => '000000'), array('align' => 'center'));
            }

            // Streckenkarte
            if ($trip->getMap()) {
                $image = $trip->getMap();

                /**
                 * @var $imageService \TYPO3\CMS\Extbase\Service\ImageService
                 */
                $imageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Service\ImageService');
                $imageReference = $imageService->getImage('', $image, true);
                $processingInstructions = array(
                    'maxWidth' => 300,
                    'maxHeight' => 200,
                );
                $processedImage = $imageReference->getOriginalFile()->process(\TYPO3\CMS\Core\Resource\ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, $processingInstructions);
                $imageProperties = $processedImage->getProperties();
                $imageUri = $processedImage->getPublicUrl();
                $section->addImage($_SERVER['DOCUMENT_ROOT'].'/'.$imageUri, array(
                    'width' => $imageProperties['width'],
                    'height' => $imageProperties['height']
                ));
            }

            //Reiseverlauf
             $roomRelations = array();
             $c = 0;
            /**
             * @var $station \JAKOTA\Reisedb\Domain\Model\Station
             */
             foreach($trip->getStations() as $station) {
                 //Überschrift
                 $heading = $station->getTitle();
                 if(strlen($station->getTracklengthApprox()) > 0) {
                     $heading .= ', ca. '.$station->getTracklengthApprox().'';
                 } else if ($station->getTracklength() > 0) {
                     $heading .= ', '.$station->getTracklength().' km';
                 }

                 if ($station->getAddtionalNightPrices()) {
                     $roomRelations[$station->getCity()] = $station->getAddtionalNightPrices();
                 }
                 $section->addText(utf8_decode($this->rp($heading)), array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                 $section->addText(utf8_decode($this->rp(strip_tags($station->getDescription()))), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left'));
                 $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                 $c++;
             }
            $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));

            //Reisetermine
            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
             $section->addText('Reisetermine', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
            } else {
             $section->addText('Dates', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
            }

            $section->addText(utf8_decode($this->rp($trip->getVerbalBookable())), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left'));

            $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
            //Infoboxen
            /**
             * @var $infobox \JAKOTA\Reisedb\Domain\Model\Infobox
             */
            foreach($trip->getInfoboxes() as $infobox) {
                $section->addText(utf8_decode($infobox->getTitle()), array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                $section->addText(utf8_decode($this->rp(strip_tags($infobox->getContent()))), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left'));
                $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
            }

            //Preise / Person
            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                $section->addText('Preise/Person', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
            } else {
                $section->addText('Price/Person', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
            }

            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                $preis_ab = 'ab ';
            } else {
                $preis_ab = 'from ';
            }

            $prices = $trip->getPrices();
            $rooms = array();
            $categories = array();
            foreach($prices as $price) {
                $room = $price->getRoomcategory();
                $category = $price->getPricecategory();

                $rooms[$room->getUid()] = $room;
                $categories[$category->getUid()] = $category;
            }

            $pricetable =array();
            foreach($rooms as $room) {
                $pricesInt = array();
                foreach($categories as $category) {
                    $pricesInt[] = array('category' => $category, 'price' => $this->getPriceByRoomCategory($room, $category, $prices));
                }

                $pricetable[] = array('room' => $room, 'prices' => $pricesInt);

            }

            $table = $section->addTable();
            $table->addRow();
            $cell = $table->addCell(3000);
            $cell->addText('', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
            foreach($categories as $category) {
               $cell = $table->addCell(2000);
               $cell->addText(utf8_decode($category->getTitle()), array('size' => 10, 'bold' => true, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
            }

            foreach($pricetable as $pricetablerow) {
               $table->addRow();
               $cell = $table->addCell(3000);
               $cell->addText(utf8_decode($pricetablerow['room']->getTitle()), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
               foreach($pricetablerow['prices'] as $price) {
                   $cell = $table->addCell(2000);
                   if ($price['price']) {
                       $priceString = round($price['price']->getPrice()).' EUR';
                       if ($price['price']->getFromPrice()) {
                           $priceString = $preis_ab.$priceString;
                       }
                   } else {
                       $priceString = '';
                   }

                   $cell->addText(utf8_decode($priceString), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
               }
            }
            $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));

            // Zuschläge / Abschläge
            $seasonPrices = $trip->getSeasonPrices();
            $seasonPricesTable = false;
            if ($seasonPrices) {
                $seasonPricesTable = array();
                foreach($seasonPrices as $seasonPrice) {
                    $key = md5($seasonPrice->getFromDate()->format(\DateTime::ISO8601).$seasonPrice->getToDate()->format(\DateTime::ISO8601));
                    $seasonPricesTable[$key]['from'] = $seasonPrice->getFromDate();
                    $seasonPricesTable[$key]['to'] = $seasonPrice->getToDate();
                    $seasonPricesTable[$key]['prices'][] = $seasonPrice;
                }
            }
            if ($seasonPricesTable) {
                if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                    $section->addText('Saisonpreise', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                } else {
                    $section->addText('Season Prices', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                }
                foreach($seasonPricesTable as $daterow) {
                    $dateText = $daterow['from']->format('d.m.Y').' - '.$daterow['to']->format('d.m.Y');
                    $section->addText(utf8_decode($dateText), array('size' => 10, 'bold' => true, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    foreach($daterow['prices'] as $price) {
                        if ($price->getType() == 0) {
                            $pricetext = number_format(abs($price->getValue()), 2, ',', '.');
                            $pricetext .= ' EUR';
                        } else {
                            $pricetext = number_format(abs($price->getValue()), 0, ',', '.');
                            $pricetext .= '%';
                        }
                        if ($price->getValue() >= 0) {
                            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                                $pricetext .= ' Zuschlag';
                            } else {
                                $pricetext .= ' surcharge';
                            }
                        } else {
                            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                                $pricetext .= ' Rabatt';
                            } else {
                                $pricetext .= ' discount';
                            }
                        }
                        if ($price->getInPricecategory()) {
                            $categories = array();
                            foreach($price->getInPricecategory() as $pricecategory) {
                                $categories[] = $pricecategory->getTitle();
                            }
                            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                                $pricetext .= ' in '.implode(', ', $categories);
                            } else {
                                $pricetext .= ' in '.implode(', ', $categories);
                            }

                        }
                        $section->addText(utf8_decode($pricetext), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    }
                }
            }
            $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));

            //Extras
            if($trip->getAdditionalServices()) {
                $section->addText('Extras', array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                $table = $section->addTable();
                foreach($trip->getAdditionalServices() as $service) {
                    $table->addRow();
                    $cell = $table->addCell(4000);
                    $cell->addText(utf8_decode($service->getName()), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    $cell = $table->addCell(3000);
                    $lastYear = 0;
                    $akPrice = false;
                    foreach($service->getPrice() as $price) {
                        if ($lastYear < $price->getYear()) {
                            $lastYear = $price->getYear();
                            $akPrice = $price;
                        }
                        if ($price->getYear() == date('Y')) {
                            $akPrice = $price; break;
                        }
                    }
                    $priceText = '';

                    if ($akPrice && $akPrice->getFromPrice()) {
                        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                            $priceText .= 'ab ';
                        } else {
                            $priceText .= 'from ';
                        }
                    }
                    if ($akPrice) {
                        $priceText .= number_format($akPrice->getPrice(), 2, ',', '.').' EUR';
                        $cell->addText(utf8_decode($priceText), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    } else {
                        $cell->addText(utf8_decode(''), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    }
                }
                $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
            }

            $newRoomRelations = array();
            foreach($roomRelations as $city => $roomRelation) {
                if (strlen($city) > 0) {
                    $newRoomRelations[$city] = $roomRelation;
                }
            }
            $roomRelations = $newRoomRelations;

            if (count($roomRelations) > 0) {
                if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                    $section->addText(utf8_decode('Zusatznächte'), array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                } else {
                    $section->addText(utf8_decode('Additional Nights'), array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));
                }
                foreach($roomRelations as $city => $roomRelation) {
                    $section->addText(utf8_decode($city), array('size' => 10, 'bold' => true, 'color' => '004B75'), array('align' => 'left', 'spaceAfter' => 0));

                    $prices = $roomRelation;
                    $rooms = array();
                    $categories = array();
                    foreach($prices as $price) {
                        $room = $price->getRoomcategory();
                        $category = $price->getPricecategory();

                        $rooms[$room->getUid()] = $room;
                        $categories[$category->getUid()] = $category;
                    }

                    $pricetable =array();
                    foreach($rooms as $room) {
                        $pricesInt = array();
                        foreach($categories as $category) {
                            $pricesInt[] = array('category' => $category, 'price' => $this->getPriceByRoomCategory($room, $category, $prices));
                        }
                        $pricetable[] = array('room' => $room, 'prices' => $pricesInt);
                    }
                    $table = $section->addTable();
                    $table->addRow();
                    $cell = $table->addCell(3000);
                    $cell->addText('', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    foreach($categories as $category) {
                        $cell = $table->addCell(2000);
                        $cell->addText(utf8_decode($category->getTitle()), array('size' => 10, 'bold' => true, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                    }

                    foreach($pricetable as $pricetablerow) {
                        $table->addRow();
                        $cell = $table->addCell(3000);
                        $cell->addText(utf8_decode($pricetablerow['room']->getTitle()), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                        foreach($pricetablerow['prices'] as $price) {
                            $cell = $table->addCell(2000);
                            if ($price['price']) {
                                $priceString = round($price['price']->getPrice()).' EUR';
                                if ($price['price']->getFromPrice()) {
                                    $priceString = $preis_ab.$priceString;
                                }
                            } else {
                                $priceString = '';
                            }
                            $cell->addText(utf8_decode($priceString), array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                        }
                    }
                    $section->addText(' ', array('size' => 10, 'bold' => false, 'color' => '000000'), array('align' => 'left', 'spaceAfter' => 0));
                }
            }

			$filename = $trip->getCode().' '.$trip->getTitle();
			$was = array("ä", "ö", "ü", "Ä", "Ö", "Ü", "ß");
			$wie = array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
			$filename = str_replace($was, $wie, $filename);
			$filename = preg_replace('/([^a-zA-Z0-9])/', '_', $filename);
			$filename .= '_'.date("YmdHis");
			$filename .= '.docx';
			$objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
			$objWriter->save('uploads/wordexport/'.$filename);
            header('Location: /uploads/wordexport/'.urlencode($filename).'?q='.md5(rand(0,10000000)));
		}
	}

	/**
	 * action categoryList
	 *
	 * @return void
	 */
	public function categoryListAction() {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        $trips = $this->tripRepository->findByCategories($this->settings['showCategories'], $this->settings['categoryLogic']);
		$this->view->assign('trips', $trips);
	}

	/**
	 * action singleTrip
	 *
	 * @return void
	 */
	public function singleTripAction() {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        $trip = $this->tripRepository->findByUid($this->settings['showTrip']);
		$this->view->assign('trip', $trip);
	}




	/**
	 * action search
	 *
	 * @return void
	 */
	public function searchAction() {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        $search = '';
		//
		$radwege = $this->loadChildCategories(47);
		$regionen = $this->loadChildCategories(3);

		if($this->request->hasArgument('search')) {
			$search = $this->request->getArgument('search');
		}

		$trips = $this->tripRepository->findBySearchterm($search);
		$tripsFound = true;

		if (count($trips) == 0) {
			$tripsFound = false;
			$trips = $this->tripRepository->findRandom();
		}

		$this->view->assign('trips', $trips);
		$this->view->assign('tripsfound', $tripsFound);
		$this->view->assign('search', $search);
		$this->view->assign('radwege', $radwege);
		$this->view->assign('regionen', $regionen);
	}

	/**
	 * action show
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Trip $trip
	 * @return void
	 */
	public function showAction(\JAKOTA\Reisedb\Domain\Model\Trip $trip) {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/CSS/tripFixedHeader.css');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JS/TripFixedHeader.js', 'text/javascript');

        /** @var Conne $connector */
        $connector = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TQ\\TqSeo\\Connector');


		// Title
		$connector->setPageTitle($trip->getSeoTitle() ? $trip->getSeoTitle() : $trip->getTitle());
		$connector->setMetaTag('title', $trip->getSeoTitle() ? $trip->getSeoTitle() : $trip->getTitle());
		$GLOBALS['TSFE']->page['title'] = $trip->getSeoTitle() ? $trip->getSeoTitle() : $trip->getTitle();
		$GLOBALS['TSFE']->indexedDocTitle = $trip->getSeoTitle() ? $trip->getSeoTitle() : $trip->getTitle();
		// Description
		$connector->setMetaTag('description', $trip->getSeoDescription() ? $trip->getSeoDescription() : $trip->getShortDescription());

		// Keywords
		$connector->setMetaTag('keywords', $trip->getSeoKeywords());

		// Short/Description
		$connector->setMetaTag('description', $trip->getSeoDescription() ? $trip->getSeoDescription() : $trip->getShortDescription());

        $canonical = $trip->getSeoCanonical();
        if ($canonical) {
            $this->response->addAdditionalHeaderData('<link rel="canonical" href="'.$canonical.'" />');
        }

        if ($trip->getSeoNoindex() == false){
			$connector->setMetaTag('robotsIndex', 'index');
			$connector->setMetaTag('robotsFollow', 'follow');
		}

		$this->view->assign('trip', $trip);
	}

	/**
	 * action review
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Trip $trip
	 * @return void
	 */
	public function reviewAction(\JAKOTA\Reisedb\Domain\Model\Trip $trip) {

        $this->view->assign('debug', $this->isFeDebugEnabled());

        // Load and INIT Formhandler Extension
		require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath("formhandler")."pi1/class.tx_formhandler_pi1.php");
		$formhandler = $this->initFormhandler();
		$this->formconfig = $GLOBALS["TSFE"]->tmpl->setup['plugin.']['tx_reisedb.']['formhandler.']['review.'];

		$gp = $this->loadFormhandlerValues($this->formconfig["settings."]["formValuesPrefix"]);
		$this->gp = $gp;

		// Load and Render Marker Templates
		$templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($GLOBALS["TSFE"]->tmpl->setup['plugin.']['tx_reisedb.']['view.']['templateRootPath'].'Formhandler/Markers/Review/');
		if ($handle = opendir($templateRootPath)) {
			while (false !== ($entry = readdir($handle))) {
				$info = pathinfo($templateRootPath.$entry);
				if ($info['extension'] == 'html') {
					$fluidRenderer = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
					$fluidRenderer->setFormat('html');
					$fluidRenderer->assign('trip', $trip);
					$fluidRenderer->assign('gp', $this->gp);
					$fluidRenderer->setTemplatePathAndFilename($templateRootPath.$entry);
					$this->addFormMarker(lcfirst($info['filename']), $fluidRenderer->render());
					unset($fluidRenderer);
				}
			}
			closedir($handle);
		}

		$form = $formhandler->main("reviewForm",$this->formconfig);

		$this->view->assign("form",$form);
		$this->view->assign('trip', $trip);
	}

    /**
     * action showReview
     *
     * @param \JAKOTA\Reisedb\Domain\Model\Trip $trip
     * @param \JAKOTA\Reisedb\Domain\Model\Review $review
     * @return void
     */
    public function showReviewAction(\JAKOTA\Reisedb\Domain\Model\Trip $trip) {

        $arguments = $this->request->getArguments(); // OR
        $review = null;
        if ($this->request->hasArgument('review')) {
            $review = $this->request->getArgument('review');
            $reviewObject = $this->reviewRepository->findByUid($review);
            if ($reviewObject) {
                $review = $reviewObject;
            } else {
                $review = false;
            }
        }

        $this->view->assign('review', $review);
        $this->view->assign('trip', $trip);
    }

	/**
	 * action booking
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Trip $trip
	 * @return void
	 */
	public function bookingAction(\JAKOTA\Reisedb\Domain\Model\Trip $trip) {

        $this->view->assign('debug', $this->isFeDebugEnabled());
        $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/CSS/mrtIconfont.css');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JS/EventPackageForm.js', 'text/javascript');

		$connector = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TQ\\TqSeo\\Connector');
		$GLOBALS['TSFE']->tmpl->setup['plugin.']['tq_seo.']['metaTags.']['robotsFollow'] = '';
		$GLOBALS['TSFE']->tmpl->setup['plugin.']['tq_seo.']['metaTags.']['robotsIndex'] = '';

		// Load and INIT Formhandler Extension
		require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath("formhandler")."pi1/class.tx_formhandler_pi1.php");
		$formhandler = $this->initFormhandler();
		$this->formconfig = $GLOBALS["TSFE"]->tmpl->setup['plugin.']['tx_reisedb.']['formhandler.']['booking.'];

		// Load and Render Marker Templates
		$templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($GLOBALS["TSFE"]->tmpl->setup['plugin.']['tx_reisedb.']['view.']['templateRootPath'].'Formhandler/Markers/Booking/');


		$gp = $this->loadFormhandlerValues($this->formconfig["settings."]["formValuesPrefix"]);
		$this->gp = $gp;

		$this->gp['persons'] = array();
		if (isset($this->gp['numberOfPersons'])) {

			$this->formconfig['settings.']['4.']['validators.']['1.']['config.']['fieldConf.']['roomcount.']['errorCheck.']['1'] = 'betweenValue';
			$this->formconfig['settings.']['4.']['validators.']['1.']['config.']['fieldConf.']['roomcount.']['errorCheck.']['1.']['minValue'] = $this->gp['numberOfPersons'];
			$this->formconfig['settings.']['4.']['validators.']['1.']['config.']['fieldConf.']['roomcount.']['errorCheck.']['1.']['maxValue'] = $this->gp['numberOfPersons'];


			for($i=1; $i <= $this->gp['numberOfPersons']; $i++) {
				$this->gp['persons'][$i] = array(
					'firstname' => $this->gp['participant'.$i.'firstname'],
					'lastname' => $this->gp['participant'.$i.'lastname'],
					'address' => $this->gp['participant'.$i.'address'],
					'postcode' => $this->gp['participant'.$i.'postcode'],
					'city' => $this->gp['participant'.$i.'city'],
					'country' => $this->gp['participant'.$i.'country'],
					'birthday' => $this->gp['participant'.$i.'birthday'],
					'rentalbike' => $this->gp['participant'.$i.'rentalbikeq'],
					'rentalbiketype' => $this->gp['participant'.$i.'rentalbiketype'],
					'rentalbikesex' => $this->gp['participant'.$i.'rentalbikesex'],
					'rentalbikegear' => $this->gp['participant'.$i.'rentalbikegear'],
					'rentalbikeheight' => $this->gp['participant'.$i.'rentalbikeheight'],
					'uid' => $i
				);
			}
		}

		$prices = $trip->getPrices();
		$rooms = array();
		$categories = array();
		foreach($prices as $price) {
			$room = $price->getRoomcategory();
			$category = $price->getPricecategory();

			$rooms[$room->getUid()] = $room;
			$categories[$category->getUid()] = $category;
		}

		$roomObject = array();
		foreach($rooms as $room) {
			$uid = $room->getUid();
			if (array_key_exists('roomcount'.$uid, $this->gp)) {

				$bedCount = $room->getBeds();
				$additionalBed = $room->getAdditionalBed();
				$roomPersons = array();
				$additionalPersons = array();

				$roomExist = false;
				for($i = 1; $i <= 10; $i++) {
					for($c = 1; $c <= $bedCount; $c++) {
						if ($this->gp['roomperson'.$uid.$i.'bed'.$c] > 0) {
							$roomPersons[] = $this->gp['persons'][$this->gp['roomperson'.$uid.$i.'bed'.$c]];
							$roomExist = true;
						}
					}
					if ($additionalBed) {
						if ($this->gp['roomperson'.$uid.$i.'bedaddtional'] > 0) {
							$additionalPersons[] = $this->gp['persons'][$this->gp['roomperson'.$uid.$i.'bedaddtional']];
							$roomExist = true;
						}
					}
                    if ($roomExist) {
                        $roomObject[] = array(
                            'additionalBedStatus' => $room->getAdditionalBed(),
                            'uid' => $room->getUid(),
                            'name' => $room->getTitle(),
                            'beds' => $roomPersons,
                            'additionalBed' => $additionalPersons,
                        );
                    }
                    $roomPersons = array();
                    $additionalPersons = array();
                    $roomExist = false;
				}

			}
		}
		$this->gp['rooms'] = $roomObject;
        $this->gp['eventPackages'] = array();
        $gpKeys = array_keys($this->gp);
        foreach($trip->getEventPackages() as $eventPackage) {
            /**
             * @var $eventPackage \JAKOTA\Reisedb\Domain\Model\EventPackage
             */
            $epKey = 'eventPackage_'.$eventPackage->getUid();
            $epDateKey = 'eventPackage_'.$eventPackage->getUid().'_date';
            if (array_key_exists($epKey, $this->gp)) {
                if ($this->gp[$epKey] == 1) {
                    $epObj = array();
                    $epObj['uid'] = $eventPackage->getUid();
                    $epObj['title'] = $eventPackage->getTitle();
                    $epObj['date'] = $this->gp[$epDateKey];
                    $epObj['city'] = $eventPackage->getCity();
                    $epObj['isGroup'] = $eventPackage->getGroupPackage();
                    $epObj['typeofprice'] = $eventPackage->getTypeofprice();
                    $epObj['extraData'] = array();
                    $extraDataKeys = $this->findKeys('/eventPackage_'.$eventPackage->getUid().'_extraData[0-9]{1,}/', $gpKeys);
                    foreach($extraDataKeys as $extraDataKey) {
                        preg_match('/eventPackage_'.$eventPackage->getUid().'_extraData([0-9]{1,})/', $extraDataKey, $matches);
                        $extraDataUid = $matches[1];
                        $extraData = $this->eventPackageExtraDataRepository->findByUid($extraDataUid);
                        array_push($epObj['extraData'], array('extraData' => $extraData, 'value' => $this->gp[$extraDataKey]));
                    }

                    $epObj['participants'] = array();
                    if ($epObj['isGroup']) {
                        foreach($this->gp['persons'] as $person) {
                            array_push($epObj['participants'], array('person' => $person));
                        }
                    } else {
                        $personKeys = $this->findKeys('/eventPackage_'.$eventPackage->getUid().'_person[0-9]{1,}/', $gpKeys);
                        foreach($personKeys as $personKey) {
                            preg_match('/eventPackage_'.$eventPackage->getUid().'_person([0-9]{1,})/', $personKey, $matches);
                            $personUid = $matches[1]+1;
                            array_push($epObj['participants'], array('person' => $this->gp['persons'][$personUid]));
                        }
                    }
                    $epObj['rooms'] = array();
                    $roomKeys = $this->findKeys('/eventPackage_'.$eventPackage->getUid().'_room[0-9]{1,}/', $gpKeys);
                    foreach($roomKeys as $roomKey) {
                        preg_match('/eventPackage_'.$eventPackage->getUid().'_room([0-9]{1,})/', $roomKey, $matches);
                        $roomUid = $matches[1];
                        $room = $this->roomcategoryRepository->findByUid($roomUid);
                        if ($room) {
                            array_push($epObj['rooms'], array('room' => $room, 'count' => $this->gp[$roomKey]));
                        }
                    }
                    switch($eventPackage->getTypeofprice()) {
                        case 1:
                            $price = $eventPackage->getFixprice()->toArray();
                            foreach($epObj['participants'] as $key => $value) { $epObj['participants'][$key]['price'] = $price[0]->getPrice(); }
                            break;
                        case 2:
                            $prices = $eventPackage->getScaledprice()->toArray();
                            foreach($epObj['participants'] as $key => $value) {
                                $price = 0;
                                $age = $this->personAge($this->gp, $value['person']['uid']);
                                foreach($prices as $priceTire) {
                                    if ($age >= $priceTire->getFromAge() && $age <= $priceTire->getToAge()) {
                                        $price = $priceTire->getPrice();
                                    }
                                }
                                $epObj['participants'][$key]['price'] = $price;
                            }
                            break;
                        case 3:
                            $prices = $eventPackage->getRoomprice()->toArray();
                            foreach($epObj['rooms'] as $key => $value) {
                                foreach($prices as $price) {
                                    if ($price->getRoomcategory()->getUid() == $value['room']->getUid() && $price->getPricecategory()->getUid() == $this->gp['pricecategory']) {
                                        $epObj['rooms'][$key]['price'] = $price->getPrice();
                                        $epObj['rooms'][$key]['pricetotal'] = $epObj['rooms'][$key]['price'] * $epObj['rooms'][$key]['count'];
                                    }
                                }
                            }
                            break;
                        default:
                            foreach($epObj['participants'] as $key => $value) { $epObj['participants'][$key]['price'] = 0; }
                            foreach($epObj['rooms'] as $key => $value) { $epObj['rooms'][$key]['price'] = 0; }
                            break;
                    }
                    $this->gp['eventPackages'][] = $epObj;
                }
            }
        }


		if ($handle = opendir($templateRootPath)) {
			while (false !== ($entry = readdir($handle))) {
				$info = pathinfo($templateRootPath.$entry);
				if ($info['extension'] == 'html') {
					$fluidRenderer = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
					$fluidRenderer->setFormat('html');
					$fluidRenderer->assign('trip', $trip);
					$fluidRenderer->assign('gp', $this->gp);
					$fluidRenderer->setTemplatePathAndFilename($templateRootPath.$entry);
					$this->addFormMarker(lcfirst($info['filename']), $fluidRenderer->render());
					unset($fluidRenderer);
				}
			}
			closedir($handle);
		}

		$filename = 'uploads/bookingpdf/'.md5(serialize($this->gp)).'.pdf';
		if (is_file($_SERVER['DOCUMENT_ROOT'].'/'.$filename)) {

			$this->formconfig['settings.']['finishers.']['1.']['config.']['admin.']['attachment'] = $filename;
			$this->formconfig['settings.']['finishers.']['1.']['config.']['user.']['attachment'] = $filename;
		}

        if ($this->isFeDebugEnabled()) {
            $this->formconfig['settings.']['masterTemplateFile.']['2'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/noTracking.html';
            $this->formconfig['settings.']['finishers.']['1.']['config.']['admin.']['to_email'] = $this->formconfig['settings.']['finishers.']['1.']['config.']['admin.']['to_email_debug'];
        }

        if (count($trip->getEventPackages()) > 0) {
            $this->formconfig['settings.']['1.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-1-6.html';
            $this->formconfig['settings.']['2.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-2-6.html';
            $this->formconfig['settings.']['3.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-3-6.html';
            $this->formconfig['settings.']['4.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-4-6.html';
            $this->formconfig['settings.']['5.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-5-6.html';
            $this->formconfig['settings.']['6.']['templateFile'] = 'EXT:reisedb/Resources/Private/Templates/Formhandler/booking-step-6-6.html';
            $this->formconfig['settings.']['6.']['validators.'] = $this->formconfig['settings.']['5.']['validators.'];
            $this->formconfig['settings.']['5.']['validators.'] = NULL;
        }

		$personNumber = 1;
		$participantInputs = '';

		if (!is_null($gp['numberOfPersons'])) {


			for($i = 0; $i < $gp['numberOfPersons']; $i++) {
				$isChild = false;
				if ($personNumber > $gp['numberOfPersons']-$gp['numberOfChildren']) {
					$isChild = true;
				}

				$valueFirstname = '###value_participant'.$personNumber.'firstname###';
				$valueLastname = '###value_participant'.$personNumber.'lastname###';
				$valueAddress = '###value_participant'.$personNumber.'address###';
				$valuePostcode = '###value_participant'.$personNumber.'postcode###';
				$valueCity = '###value_participant'.$personNumber.'city###';
				$valueCountry = '###value_participant'.$personNumber.'country###';

				if ($personNumber == 1) {
					if (!isset($gp['participant1firstname'])) $valueFirstname = $gp['firstname'];
					if (!isset($gp['participant1lastname'])) $valueLastname = $gp['lastname'];
					if (!isset($gp['participant1address'])) $valueAddress = $gp['address'];
					if (!isset($gp['participant1postcode'])) $valuePostcode = $gp['postcode'];
					if (!isset($gp['participant1city'])) $valueCity = $gp['city'];
					if (!isset($gp['participant1country'])) $valueCountry = $gp['country'];
				}

				$aditionalServices = $trip->getAdditionalServices();
				$rentalBikesTemplate = '';
				if ($aditionalServices) {

					$rentalBikesTemplate .= '<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			<label class="einzeiler-ausrichtung" for="">###LLL:rentalbikeneeded###</label>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<select id="participant'.$personNumber.'rentalbikeq" class="form-control select" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikeq]">
			  <option value="no" ###selected_participant'.$personNumber.'rentalbikeq_no###>###LLL:no###</option>
			  <option value="yes" ###selected_participant'.$personNumber.'rentalbikeq_yes###>###LLL:yes###</option>
			</select>
		</div>
	</div>
</div>
<div class="formFrame" id="rentalBikeSettings'.$personNumber.'">';

					$c =1;
					foreach($aditionalServices as $aditionalService) {
						if ($aditionalService->getIsRentalBike()) {
							$name = $aditionalService->getName();
							$uid = $aditionalService->getUid();

							$checked = '';
							if ($c==1 && !isset($gp['participant'.$personNumber.'rentalbiketype'])) {
								$checked = 'checked';
							}

							$rentalBikesTemplate .= '<div class="radio">
								<label>
									<input type="radio" name="###formValuesPrefix###[participant'.$personNumber.'rentalbiketype]" id="optionsRadios'.$personNumber.$c.'" value="'.$uid.'" ###checked_participant'.$personNumber.'rentalbiketype_'.$uid.'### '.$checked.'/>
									<strong>'.$name.'</strong>
								</label>
							</div>';
							$c++;
						}
					}
					$rentalBikesTemplate .= '<hr>';

					$checked = '';
					if (!isset($gp['participant'.$personNumber.'rentalbikesex'])) {
						$checked = 'checked';
					}

					$rentalBikesTemplate .= '<div class="radio">
								<label>
									<input type="radio" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikesex]" id="optionsRadiosSex'.$personNumber.'0" value="Herrenrad" ###checked_participant'.$personNumber.'rentalbikesex_Herrenrad### '.$checked.'/>
									<strong>###LLL:mensbike###</strong>
								</label>
							</div>';

					$rentalBikesTemplate .= '<div class="radio">
								<label>
									<input type="radio" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikesex]" id="optionsRadiosSex'.$personNumber.'1" value="Damenrad" ###checked_participant'.$personNumber.'rentalbikesex_Damenrad###/>
									<strong>###LLL:womensbike###</strong>
								</label>
							</div>';

					$rentalBikesTemplate .= '<hr>';

					$checked = '';
					if (!isset($gp['participant'.$personNumber.'rentalbikegear'])) {
						$checked = 'checked';
					}

					$rentalBikesTemplate .= '<div class="radio">
								<label>
									<input type="radio" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikegear]" id="optionsRadiosGear'.$personNumber.'0" value="24gang" ###checked_participant'.$personNumber.'rentalbikegear_24gang### '.$checked.'/>
									<strong>###LLL:24gear###</strong>
								</label>
							</div>';

					$rentalBikesTemplate .= '<div class="radio">
								<label>
									<input type="radio" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikegear]" id="optionsRadiosGear'.$personNumber.'1" value="7gang" ###checked_participant'.$personNumber.'rentalbikegear_7gang###/>
									<strong>###LLL:7gear###</strong>
								</label>
							</div>';

					$rentalBikesTemplate .= '<hr>';

					$rentalBikesTemplate .= '<div class="form-group">
					  <label class="" for="participant'.$personNumber.'rentalbikeheight">###LLL:bodyheight###</label>
					  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'rentalbikeheight" name="###formValuesPrefix###[participant'.$personNumber.'rentalbikeheight]" value="###value_participant'.$personNumber.'rentalbikeheight###">
					</div>';

					$rentalBikesTemplate .= '</div>


<script type="text/javascript">
							$("#participant'.$personNumber.'rentalbikeq").change(function(event) {

								if ($(this).val() == "yes") {
									$("#rentalBikeSettings'.$personNumber.'").slideDown();
								} else {
									$("#rentalBikeSettings'.$personNumber.'").slideUp();
								}
							});
							if ($("#participant'.$personNumber.'rentalbikeq").val() == "no") {
								$("#rentalBikeSettings'.$personNumber.'").hide();
							}

							</script>';
				}



				$this->formconfig['settings.']['3.']['validators.']['1.']['config.']['fieldConf.']['participant'.$personNumber.'firstname.']['errorCheck.']['1'] = 'required';
				$this->formconfig['settings.']['3.']['validators.']['1.']['config.']['fieldConf.']['participant'.$personNumber.'lastname.']['errorCheck.']['1'] = 'required';



				if ($isChild) {
					$participantInputs .= '<h4><strong>###LLL:participant### '.$personNumber.'</strong> (###LLL:child###)</h4>';
				} else {
					$participantInputs .= '<h4><strong>###LLL:participant### ' . $personNumber . '</strong> (###LLL:adult###)</h4>';
				}

				$datepicker = '';
				if ($isChild) {
					$this->formconfig['settings.']['3.']['validators.']['1.']['config.']['fieldConf.']['participant'.$personNumber.'birthday.']['errorCheck.']['1'] = 'required';
					$this->formconfig['settings.']['3.']['validators.']['1.']['config.']['fieldConf.']['participant'.$personNumber.'birthday.']['errorCheck.']['2'] = 'date';
					$this->formconfig['settings.']['3.']['validators.']['1.']['config.']['fieldConf.']['participant'.$personNumber.'birthday.']['errorCheck.']['2.']['pattern'] = 'd.m.y';
					$datepicker = '<div class="row">
	<div class="col-md-12">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'birthday### ###is_error_participant'.$personNumber.'birthday###">
			<label class="" for="participant'.$personNumber.'birthday">###LLL:birthday### ###required_participant'.$personNumber.'birthday###</label>
			<div class="input-group bdaypicker">
				<input class="form-control " type="text" size="20" id="participant'.$personNumber.'birthday" name="###formValuesPrefix###[participant'.$personNumber.'birthday]" value="###value_participant'.$personNumber.'birthday###">
				<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			</div>
			###error_participant'.$personNumber.'birthday###
		</div>
	</div>
</div>';
				} else {
					$datepicker = '<input class="form-control " type="hidden" size="20" id="participant'.$personNumber.'birthday" name="###formValuesPrefix###[participant'.$personNumber.'birthday]" value="">';
				}

				$participantInputs .= '<div class="row"><div class="col-md-7"><div class="row">
	  <div class="col-md-6">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'firstname### ###is_error_participant'.$personNumber.'firstname###">
		  <label class="einzeiler-ausrichtung" for="participant'.$personNumber.'firstname">###LLL:firstname### ###required_participant'.$personNumber.'firstname###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'firstname" name="###formValuesPrefix###[participant'.$personNumber.'firstname]" value="'.$valueFirstname.'">
		  ###error_participant'.$personNumber.'firstname###
		</div>
	  </div>
	  <div class="col-md-6">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'lastname### ###is_error_participant'.$personNumber.'lastname###">
		  <label class="einzeiler-ausrichtung" for="participant'.$personNumber.'lastname">###LLL:lastname### ###required_participant'.$personNumber.'lastname###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'lastname" name="###formValuesPrefix###[participant'.$personNumber.'lastname]" value="'.$valueLastname.'">
		  ###error_participant'.$personNumber.'lastname###
		</div>
	  </div>
	</div>'.$datepicker.'
	<div class="row">
	  <div class="col-md-12">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'address### ###is_error_participant'.$personNumber.'address###">
		  <label class="" for="participant'.$personNumber.'address">###LLL:address### ###required_participant'.$personNumber.'address###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'address" name="###formValuesPrefix###[participant'.$personNumber.'address]" value="'.$valueAddress.'">
		  ###error_participant'.$personNumber.'address###
		</div>
	  </div>
	</div>
	<div class="row">
	  <div class="col-md-3">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'postcode### ###is_error_participant'.$personNumber.'postcode###">
		  <label class="" for="participant'.$personNumber.'postcode">###LLL:postcode### ###required_participant'.$personNumber.'postcode###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'postcode" name="###formValuesPrefix###[participant'.$personNumber.'postcode]" value="'.$valuePostcode.'">
		  ###error_participant'.$personNumber.'postcode###
		</div>
	  </div>
	  <div class="col-md-9">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'city### ###is_error_participant'.$personNumber.'city###">
		  <label class="" for="participant'.$personNumber.'city">###LLL:city### ###required_participant'.$personNumber.'city###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'city" name="###formValuesPrefix###[participant'.$personNumber.'city]" value="'.$valueCity.'">
		  ###error_participant'.$personNumber.'city###
		</div>
	  </div>
	</div>
	<div class="row">
	  <div class="col-md-12">
		<div class="form-group ###requiredMarker_participant'.$personNumber.'country### ###is_error_participant'.$personNumber.'country###">
		  <label class="" for="participant'.$personNumber.'country">###LLL:country### ###required_participant'.$personNumber.'country###</label>
		  <input class="form-control" type="text" size="20" id="participant'.$personNumber.'country" name="###formValuesPrefix###[participant'.$personNumber.'country]" value="'.$valueCountry.'">
		  ###error_participant'.$personNumber.'country###
		</div>
	  </div>
	</div>
</div>
	<div class="col-md-5">
		'.$rentalBikesTemplate.'
	</div>
</div>';

				$isLast = $i+1;
				if ($isLast < $gp['numberOfPersons']-$gp['numberOfChildren']) {
					$participantInputs .= '<hr style="border-color:#52991f;" />';
				} else {

				}


				$personNumber++;
			}
		}

		$this->addFormMarker('participants', $participantInputs);

		// Load Formhander GP Vars



		/*


				if ($personNumber == 1) {
					if (is_null($gp['participant1firstname'])) $valueFirstname = $gp['firstname'];
					if (is_null($gp['participant1lastname'])) $valueLastname = $gp['lastname'];
					if (is_null($gp['participant1address'])) $valueAddress = $gp['address'];
					if (is_null($gp['participant1postcode'])) $valuePostcode = $gp['postcode'];
					if (is_null($gp['participant1city'])) $valueCity = $gp['city'];
					if (is_null($gp['participant1country'])) $valueCountry = $gp['country'];
				}
		 */






		// Render Form
		$form = $formhandler->main("bookingForm",$this->formconfig);

		$GLOBALS['TSFE']->tmpl->setup['plugin.']['tq_seo.']['metaTags.']['robotsFollow'] = '';
		$GLOBALS['TSFE']->tmpl->setup['plugin.']['tq_seo.']['metaTags.']['robotsIndex'] = '';

		$this->view->assign("form",$form);
		$this->view->assign('trip', $trip);
	}

	private function addFormMarker($name, $value) {
		$this->formconfig['settings.']['markers.'][$name] = 'TEXT';
		$this->formconfig['settings.']['markers.'][$name.'.']['value'] = $value;
	}

	private function initFormhandler() {
		$formhandler = new \tx_formhandler_pi1();
		$formhandler->cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('tslib_cObj');
		return $formhandler;
	}

	private function loadFormhandlerValues($prefix) {

		session_start();
		$formhandlerSession = $_SESSION['formhandler'][$_POST[$prefix]['randomID']];

		$gp = array();
        if (is_array($formhandlerSession['values'])) {
            foreach($formhandlerSession['values'] as $step) {
                if (is_array($step)) {
                    $gp = array_merge($gp, $step);
                }

            }
        }

		if (isset($formhandlerSession['currentStep'])) {
			$gp['currentStep'] = $formhandlerSession['currentStep'];
		} else {
			$gp['currentStep'] = 0;
		}

		if (isset($formhandlerSession['totalSteps'])) {
			$gp['totalSteps'] = $formhandlerSession['totalSteps'];
		} else {
			$gp['totalSteps'] = 99;
		}





		$newGp = array_merge(\TYPO3\CMS\Core\Utility\GeneralUtility::_GET(), \TYPO3\CMS\Core\Utility\GeneralUtility::_POST());
		if (is_array($newGp[$prefix])) {
			$gp = array_merge($gp, $newGp[$prefix]);
		}


		return $gp;
	}

	private function loadChildCategories($categoryUid) {
		$return = array();
		$categories = $this->categoriesRepository->findByParent($categoryUid);
		if (count($categories) > 0) {
			foreach($categories as $category) {
				$return[] = array(
					'category' => $category,
					'children' => $this->loadChildCategories($category->getUid())
				);
			}
			return $return;
		} else {
			return null;
		}
	}

    private function isFeDebugEnabled() {
        return ($GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] === true);
    }

    private function findKeys($regexp, $array) {
        $returnArray = array();
        foreach($array as $item) {
            if (preg_match($regexp, $item)) array_push($returnArray, $item);
        }
        asort($returnArray);
        return $returnArray;
    }

    private function personAge($gp, $personUid) {
        foreach($gp['persons'] as $person) {
            if ($person['uid'] == $personUid) {
                if (preg_match('/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/', $person['birthday']) && preg_match('/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/', $gp['arrival'])) {

                    $tz  = new \DateTimeZone('Europe/Brussels');
                    $birthDate = \DateTime::createFromFormat('d.m.Y', $person['birthday'], $tz);
                    $arrivalDate = \DateTime::createFromFormat('d.m.Y', $gp['arrival'], $tz);

                    return $birthDate->diff($arrivalDate)->y;
                }
                return 99;
            }
        }
        return 99;
    }
}