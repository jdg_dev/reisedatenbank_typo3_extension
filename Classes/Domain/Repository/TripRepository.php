<?php
namespace JAKOTA\Reisedb\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Trips
 */
class TripRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    /**
     * Overload Find by UID to also get hidden records
     *
     * @param int $uid
     */
    public function findByUidNoEnableFields($uid) {
        $query = $this->createQuery();
        $querySettings = $query->getQuerySettings();
        $querySettings->setIgnoreEnableFields(TRUE);
        $querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime','endtime'));
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->setQuerySettings($querySettings);
        $and = array(
            $query->equals('uid', $uid),
            //$query->equals('deleted', 0)
        );
        $object = $query->matching($query->logicalAnd($and))->execute()->getFirst();
        return $object;
    }


    public function findRandom() {
        $rows = $this->createQuery()->execute()->count();
        $row_number = mt_rand(0, max(0, ($rows - 1)));
        return $this->createQuery()->setOffset($row_number)->setLimit(5)->execute();
    }


    /**
     * @param $search
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findBySearchterm($search) {

        $query = $this->createQuery();

        $queryPart = array();

        // Searchword
        if (strlen($search['keyword']) > 0) {
            if (!preg_match('/^[A-z|Ü|ü|Ä|ä|Ö|ö|ß]{2,5}[ |-]{0,1}[0-9|A-z|Ü|ü|Ä|ä|Ö|ö|ß]{0,3}/', $search['keyword'])) {
                $keywords = explode(' ', $search['keyword']);
            } else {
                // Wenne s sich um einen reisecode handeln könnte dann nicht bei den leerzeichen trennen
                $keywords = array($search['keyword']);
            }
            $searchParts = array();
            foreach ($keywords as $keyword) {
                $searchParts[] = $query->logicalOr( array(
                    $query->like('title', "%".$keyword."%"),
                    $query->like('description', "%".$keyword."%"),
                    $query->like('code', "%".$keyword."%"),
                    $query->like('seo_title', "%".$keyword."%"),
                    $query->like('seo_keywords', "%".$keyword."%"),
                    $query->like('seo_description', "%".$keyword."%"),
                ));
            }
            $queryPart[] = $query->logicalAnd($searchParts);
        }

        // Trip Period
        $search['arrival'] = trim($search['arrival']);
        $search['departure'] = trim($search['departure']);
        if (preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $search['arrival']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $search['departure'])) {
            $dateStart = substr($search['arrival'], 6, 4).'-'.substr($search['arrival'], 3, 2).'-'.substr($search['arrival'], 0, 2);
            $dateEnd = substr($search['departure'], 6, 4).'-'.substr($search['departure'], 3, 2).'-'.substr($search['departure'], 0, 2);
            $result = $GLOBALS['TYPO3_DB']->sql_query("SELECT `tx_reisedb_domain_model_bookabledate`.`trip` as trip, `tx_reisedb_domain_model_trip`.`nights` as nights
            FROM `tx_reisedb_domain_model_bookabledate`
            LEFT JOIN `tx_reisedb_domain_model_trip`
            ON `tx_reisedb_domain_model_trip`.`uid` = `tx_reisedb_domain_model_bookabledate`.`trip`
            WHERE  `tx_reisedb_domain_model_bookabledate`.`date` BETWEEN UNIX_TIMESTAMP('".$dateStart." 00:00:00') AND UNIX_TIMESTAMP('".$dateStart." 23:59:59')
            AND `tx_reisedb_domain_model_bookabledate`.`status` > 0
            AND `tx_reisedb_domain_model_bookabledate`.`trip` > 0
            GROUP BY `tx_reisedb_domain_model_bookabledate`.`trip`
            HAVING nights <= DATEDIFF('".$dateEnd."','".$dateStart."')");
            $searchParts = array();
            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
                $searchParts[] = $query->equals('uid', $row['trip']);
            }
            if (count($searchParts) > 0) {
                $queryPart[] = $query->logicalOr($searchParts);
            } else {
                // Erzwinge leeres Ergebnis wenn keine freien Termine gefunden wurden
                $queryPart[] = $query->equals('uid', '0');
            }
        }

        // Region
        if ($search['region'] > 0) {
            $queryPart[] = $query->contains('categories', $search['region']);
        }

        // Difficulty
        if (count($search['difficulty']) > 0 && is_array($search['difficulty'])) {
            $subqueryParty = array();
            foreach($search['difficulty'] as $value) {
                $subqueryParty[] = $query->contains('categories', $value);
            }
            if (count($subqueryParty) > 0) $queryPart[] = $query->logicalOr($subqueryParty);
        }

        // Type
        if (count($search['type']) > 0 && is_array($search['type'])) {
            $subqueryParty = array();
            foreach($search['type'] as $value) {
                $subqueryParty[] = $query->contains('categories', $value);
            }
            if (count($subqueryParty) > 0) $queryPart[] = $query->logicalOr($subqueryParty);
        }

        // Deleted & Hidden
        $queryPart[] = $query->logicalAnd( array(
            $query->equals('deleted', 0),
            $query->equals('hidden', 0),
        ) );
        $query->matching($query->logicalAnd($queryPart));
        return $query->execute();
    }

    public function findBySearchtermOr($search) {

        $query = $this->createQuery();

        $queryPart = array();

        // Searchword
        if (strlen($search['keyword']) > 0) {
            if (!preg_match('/^[A-z|Ü|ü|Ä|ä|Ö|ö|ß]{2,5}[ |-]{0,1}[0-9|A-z|Ü|ü|Ä|ä|Ö|ö|ß]{0,3}/', $search['keyword'])) {
                $keywords = explode(' ', $search['keyword']);
            } else {
                // Wenne s sich um einen reisecode handeln könnte dann nicht bei den leerzeichen trennen
                $keywords = array($search['keyword']);
            }
            $searchParts = array();
            foreach ($keywords as $keyword) {
                $searchParts[] = $query->logicalOr( array(
                    $query->like('title', "%".$keyword."%"),
                    $query->like('description', "%".$keyword."%"),
                    $query->like('code', "%".$keyword."%"),
                    $query->like('seo_title', "%".$keyword."%"),
                    $query->like('seo_keywords', "%".$keyword."%"),
                    $query->like('seo_description', "%".$keyword."%"),
                ));
            }
            $queryPart[] = $query->logicalAnd($searchParts);
        }

        // Trip Period
        $search['arrival'] = trim($search['arrival']);
        $search['departure'] = trim($search['departure']);
        if (preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $search['arrival']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $search['departure'])) {
            $dateStart = substr($search['arrival'], 6, 4).'-'.substr($search['arrival'], 3, 2).'-'.substr($search['arrival'], 0, 2);
            $dateEnd = substr($search['departure'], 6, 4).'-'.substr($search['departure'], 3, 2).'-'.substr($search['departure'], 0, 2);
            $result = $GLOBALS['TYPO3_DB']->sql_query("SELECT `tx_reisedb_domain_model_bookabledate`.`trip` as trip, `tx_reisedb_domain_model_trip`.`nights` as nights
            FROM `tx_reisedb_domain_model_bookabledate`
            LEFT JOIN `tx_reisedb_domain_model_trip`
            ON `tx_reisedb_domain_model_trip`.`uid` = `tx_reisedb_domain_model_bookabledate`.`trip`
            WHERE  `tx_reisedb_domain_model_bookabledate`.`date` BETWEEN UNIX_TIMESTAMP('".$dateStart." 00:00:00') AND UNIX_TIMESTAMP('".$dateStart." 23:59:59')
            AND `tx_reisedb_domain_model_bookabledate`.`status` > 0
            AND `tx_reisedb_domain_model_bookabledate`.`trip` > 0
            GROUP BY `tx_reisedb_domain_model_bookabledate`.`trip`
            HAVING nights <= DATEDIFF('".$dateEnd."','".$dateStart."')");
            $searchParts = array();
            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
                $searchParts[] = $query->equals('uid', $row['trip']);
            }
            if (count($searchParts) > 0) {
                $queryPart[] = $query->logicalOr($searchParts);
            } else {
                // Erzwinge leeres Ergebnis wenn keine freien Termine gefunden wurden
                $queryPart[] = $query->equals('uid', '0');
            }
        }

        // Region
        if ($search['region'] > 0) {
            $queryPart[] = $query->contains('categories', $search['region']);
        }

        // Difficulty
        if (count($search['difficulty']) > 0) {
            $subqueryParty = array();
            foreach($search['difficulty'] as $value) {
                $subqueryParty[] = $query->contains('categories', $value);
            }
            if (count($subqueryParty) > 0) $queryPart[] = $query->logicalOr($subqueryParty);

        }

        // Type
        if (count($search['type']) > 0) {
            $subqueryParty = array();
            foreach($search['type'] as $value) {
                $subqueryParty[] = $query->contains('categories', $value);
            }
            if (count($subqueryParty) > 0) $queryPart[] = $query->logicalOr($subqueryParty);

        }

        // Deleted & Hidden
        $queryPart[] = $query->logicalAnd( array(
            $query->equals('deleted', 0),
            $query->equals('hidden', 0),
        ) );

        $query->matching($query->logicalOr($queryPart));
        return $query->execute();
    }

    public function findByCategories($categories, $logic) {
        $categories = explode(',',$categories);
        $query = $this->createQuery();

        $queryPart = array();
        foreach($categories as $cateory) {
            $queryPart[] = $query->contains('categories', $cateory);
        }

        switch($logic) {
            case 'AND': $query->matching($query->logicalAnd($queryPart)); break;
            case 'OR': $query->matching($query->logicalOr($queryPart)); break;
        }

        return $query->execute();
    }
}