<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Trip
 */
class Trip extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
	 * @cascade remove
	 */
	protected $categories = NULL;

	/**
	 * description
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * code
	 *
	 * @var string
	 */
	protected $code = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * highlights
	 *
	 * @var string
	 */
	protected $highlights = '';

	/**
	 * seoTitle
	 *
	 * @var string
	 */
	protected $seoTitle = '';

	/**
	 * seoKeywords
	 *
	 * @var string
	 */
	protected $seoKeywords = '';

	/**
	 * seoDescription
	 *
	 * @var string
	 */
	protected $seoDescription = '';

	/**
	 * seoCanonical
	 *
	 * @var string
	 */
	protected $seoCanonical;

	/**
	 * seoNoindex
	 *
	 * @var boolean
	 */
	protected $seoNoindex;
	/**
	 * nights
	 *
	 * @var integer
	 */
	protected $nights = 0;

	/**
	 * type
	 *
	 * @var integer
	 */
	protected $type = 0;

	/**
	 * documents
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @cascade remove
	 */
	protected $documents = NULL;

	/**
	 * images
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @cascade remove
	 */
	protected $images = NULL;

	/**
	 * map
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $map = NULL;

	/**
	 * isStartour
	 *
	 * @var boolean
	 */
	protected $isStartour = FALSE;

	/**
	 * shortDescription
	 *
	 * @var string
	 */
	protected $shortDescription = '';

	/**
	 * infoboxes
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Infobox>
	 * @cascade remove
	 */
	protected $infoboxes = NULL;

	/**
	 * stations
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Station>
	 * @cascade remove
	 */
	protected $stations = NULL;

	/**
	 * prices
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price>
	 * @cascade remove
	 */
	protected $prices = NULL;

	/**
	 * additionalServices
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\AdditionalService>
	 * @cascade remove
	 */
	protected $additionalServices = NULL;

	/**
	 * seasonPrices
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\SeasonPrice>
	 * @cascade remove
	 */
	protected $seasonPrices = NULL;

	/**
	 * groupDiscounts
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\GroupDiscount>
	 * @cascade remove
	 */
	protected $groupDiscounts = NULL;

	/**
	 * childDiscounts
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\ChildDiscount>
	 * @cascade remove
	 */
	protected $childDiscounts = NULL;

	/**
	 * bookableDates
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\BookableDate>
	 * @cascade remove
	 * @lazy
	 */
	protected $bookableDates = NULL;

	/**
	 * reviews
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Review>
	 * @cascade remove
	 * @lazy
	 */
	protected $reviews = NULL;

	/**
	 * eventPackages
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackage>
	 * @cascade remove
	 * @lazy
	 */
	protected $eventPackages = NULL;

	/**
	 * verbalBookable
	 *
	 * @var string
	 */
	protected $verbalBookable = '';

	/**
	 * additionalInfo
	 *
	 * @var int
	 */
	protected $additionalInfo = '';

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->documents = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->infoboxes = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->stations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->prices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->additionalServices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->seasonPrices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->groupDiscounts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->childDiscounts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->bookableDates = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->reviews = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->eventPackages = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the code
	 *
	 * @return string $code
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * Sets the code
	 *
	 * @param string $code
	 * @return void
	 */
	public function setCode($code) {
		$this->code = $code;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the highlights
	 *
	 * @return string $highlights
	 */
	public function getHighlights() {
		return $this->highlights;
	}

	/**
	 * Sets the highlights
	 *
	 * @param string $highlights
	 * @return void
	 */
	public function setHighlights($highlights) {
		$this->highlights = $highlights;
	}


	/**
	 * Returns the seoTitle
	 *
	 * @return string $seoTitle
	 */
	public function getSeoTitle() {
		return $this->seoTitle;
	}

	/**
	 * Sets the seoTitle
	 *
	 * @param string $seoTitle
	 * @return void
	 */
	public function setSeoTitle($seoTitle) {
		$this->seoTitle = $seoTitle;
	}

	/**
	 * Returns the seoKeywords
	 *
	 * @return string $seoKeywords
	 */
	public function getSeoKeywords() {
		return $this->seoKeywords;
	}

	/**
	 * Sets the seoKeywords
	 *
	 * @param string $seoKeywords
	 * @return void
	 */
	public function setSeoKeywords($seoKeywords) {
		$this->seoKeywords = $seoKeywords;
	}

	/**
	 * Returns the seoDescription
	 *
	 * @return string $seoDescription
	 */
	public function getSeoDescription() {
		return $this->seoDescription;
	}

	/**
	 * Sets the seoDescription
	 *
	 * @param string $seoDescription
	 * @return void
	 */
	public function setSeoDescription($seoDescription) {
		$this->seoDescription = $seoDescription;
	}

	/**
	 * Returns the seoCanonical
	 *
	 * @return string $seoCanonical
	 */
	public function getSeoCanonical()
	{
		return $this->seoCanonical;
	}

	/**
	 * Sets the seoCanonical
	 *
	 * @param string $seoCanonical
	 * @return void
	 */
	public function setSeoCanonical($seoCanonical)
	{
		$this->seoCanonical = $seoCanonical;
	}

	/**
	 * Returns the seoNoindex
	 *
	 * @return boolean $seoNoindex
	 */
	public function getSeoNoindex()
	{
		return $this->seoNoindex;
	}

	/**
	 * Sets the seoNoindex
	 *
	 * @param boolean $seoNoindex
	 * @return void
	 */
	public function setSeoNoindex($seoNoindex)
	{
		$this->seoNoindex = $seoNoindex;
	}

	/**
	 * Returns the nights
	 *
	 * @return integer $nights
	 */
	public function getNights() {
		return $this->nights;
	}

	/**
	 * Sets the nights
	 *
	 * @param integer $nights
	 * @return void
	 */
	public function setNights($nights) {
		$this->nights = $nights;
	}

	/**
	 * Returns the type
	 *
	 * @return integer $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param integer $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Adds a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
	 * @return void
	 */
	public function addDocument(\TYPO3\CMS\Extbase\Domain\Model\FileReference $document) {
		$this->documents->attach($document);
	}

	/**
	 * Removes a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $documentToRemove The FileReference to be removed
	 * @return void
	 */
	public function removeDocument(\TYPO3\CMS\Extbase\Domain\Model\FileReference $documentToRemove) {
		$this->documents->detach($documentToRemove);
	}

	/**
	 * Returns the documents
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $documents
	 */
	public function getDocuments() {
		return $this->documents;
	}

	/**
	 * Sets the documents
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $documents
	 * @return void
	 */
	public function setDocuments(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $documents) {
		$this->documents = $documents;
	}

	/**
	 * Adds a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
		$this->images->attach($image);
	}

	/**
	 * Removes a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
	 * @return void
	 */
	public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove) {
		$this->images->detach($imageToRemove);
	}

	/**
	 * Returns the images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Sets the images
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
	 * @return void
	 */
	public function setImages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $images) {
		$this->images = $images;
	}

	/**
	 * Returns the map
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $map
	 */
	public function getMap() {
		return $this->map;
	}

	/**
	 * Sets the map
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $map
	 * @return void
	 */
	public function setMap(\TYPO3\CMS\Extbase\Domain\Model\FileReference $map) {
		$this->map = $map;
	}

	/**
	 * Adds a Infobox
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Infobox $infobox
	 * @return void
	 */
	public function addInfobox(\JAKOTA\Reisedb\Domain\Model\Infobox $infobox) {
		$this->infoboxes->attach($infobox);
	}

	/**
	 * Removes a Infobox
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Infobox $infoboxToRemove The Infobox to be removed
	 * @return void
	 */
	public function removeInfobox(\JAKOTA\Reisedb\Domain\Model\Infobox $infoboxToRemove) {
		$this->infoboxes->detach($infoboxToRemove);
	}

	/**
	 * Returns the infoboxes
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Infobox> $infoboxes
	 */
	public function getInfoboxes() {
		return $this->infoboxes;
	}

	/**
	 * Sets the infoboxes
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Infobox> $infoboxes
	 * @return void
	 */
	public function setInfoboxes(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $infoboxes) {
		$this->infoboxes = $infoboxes;
	}

	/**
	 * Adds a Station
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Station $station
	 * @return void
	 */
	public function addStation(\JAKOTA\Reisedb\Domain\Model\Station $station) {
		$this->stations->attach($station);
	}

	/**
	 * Removes a Station
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Station $stationToRemove The Station to be removed
	 * @return void
	 */
	public function removeStation(\JAKOTA\Reisedb\Domain\Model\Station $stationToRemove) {
		$this->stations->detach($stationToRemove);
	}

	/**
	 * Returns the stations
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Station> $stations
	 */
	public function getStations() {
		return $this->stations;
	}

	/**
	 * Sets the stations
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Station> $stations
	 * @return void
	 */
	public function setStations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $stations) {
		$this->stations = $stations;
	}

	/**
	 * Adds a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $price
	 * @return void
	 */
	public function addPrice(\JAKOTA\Reisedb\Domain\Model\Price $price) {
		$this->prices->attach($price);
	}

	/**
	 * Removes a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $priceToRemove The Price to be removed
	 * @return void
	 */
	public function removePrice(\JAKOTA\Reisedb\Domain\Model\Price $priceToRemove) {
		$this->prices->detach($priceToRemove);
	}

	/**
	 * Returns the prices
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $prices
	 */
	public function getPrices() {
		return $this->prices;
	}

	/**
	 * Sets the prices
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $prices
	 * @return void
	 */
	public function setPrices(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $prices) {
		$this->prices = $prices;
	}

	/**
	 * Adds a AdditionalService
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\AdditionalService $additionalService
	 * @return void
	 */
	public function addAdditionalService(\JAKOTA\Reisedb\Domain\Model\AdditionalService $additionalService) {
		$this->additionalServices->attach($additionalService);
	}

	/**
	 * Removes a AdditionalService
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\AdditionalService $additionalServiceToRemove The AdditionalService to be removed
	 * @return void
	 */
	public function removeAdditionalService(\JAKOTA\Reisedb\Domain\Model\AdditionalService $additionalServiceToRemove) {
		$this->additionalServices->detach($additionalServiceToRemove);
	}

	/**
	 * Returns the additionalServices
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\AdditionalService> $additionalServices
	 */
	public function getAdditionalServices() {
		return $this->additionalServices;
	}

	/**
	 * Sets the additionalServices
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\AdditionalService> $additionalServices
	 * @return void
	 */
	public function setAdditionalServices(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $additionalServices) {
		$this->additionalServices = $additionalServices;
	}

	/**
	 * Adds a SeasonPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\SeasonPrice $seasonPrice
	 * @return void
	 */
	public function addSeasonPrice(\JAKOTA\Reisedb\Domain\Model\SeasonPrice $seasonPrice) {
		$this->seasonPrices->attach($seasonPrice);
	}

	/**
	 * Removes a SeasonPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\SeasonPrice $seasonPriceToRemove The SeasonPrice to be removed
	 * @return void
	 */
	public function removeSeasonPrice(\JAKOTA\Reisedb\Domain\Model\SeasonPrice $seasonPriceToRemove) {
		$this->seasonPrices->detach($seasonPriceToRemove);
	}

	/**
	 * Returns the seasonPrices
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\SeasonPrice> $seasonPrices
	 */
	public function getSeasonPrices() {
		return $this->seasonPrices;
	}

	/**
	 * Sets the seasonPrices
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\SeasonPrice> $seasonPrices
	 * @return void
	 */
	public function setSeasonPrices(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $seasonPrices) {
		$this->seasonPrices = $seasonPrices;
	}

	/**
	 * Adds a GroupDiscount
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\GroupDiscount $groupDiscount
	 * @return void
	 */
	public function addGroupDiscount(\JAKOTA\Reisedb\Domain\Model\GroupDiscount $groupDiscount) {
		$this->groupDiscounts->attach($groupDiscount);
	}

	/**
	 * Removes a GroupDiscount
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\GroupDiscount $groupDiscountToRemove The GroupDiscount to be removed
	 * @return void
	 */
	public function removeGroupDiscount(\JAKOTA\Reisedb\Domain\Model\GroupDiscount $groupDiscountToRemove) {
		$this->groupDiscounts->detach($groupDiscountToRemove);
	}

	/**
	 * Returns the groupDiscounts
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\GroupDiscount> $groupDiscounts
	 */
	public function getGroupDiscounts() {
		return $this->groupDiscounts;
	}

	/**
	 * Sets the groupDiscounts
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\GroupDiscount> $groupDiscounts
	 * @return void
	 */
	public function setGroupDiscounts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $groupDiscounts) {
		$this->groupDiscounts = $groupDiscounts;
	}

	/**
	 * Adds a ChildDiscount
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\ChildDiscount $childDiscount
	 * @return void
	 */
	public function addChildDiscount(\JAKOTA\Reisedb\Domain\Model\ChildDiscount $childDiscount) {
		$this->childDiscounts->attach($childDiscount);
	}

	/**
	 * Removes a ChildDiscount
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\ChildDiscount $childDiscountToRemove The ChildDiscount to be removed
	 * @return void
	 */
	public function removeChildDiscount(\JAKOTA\Reisedb\Domain\Model\ChildDiscount $childDiscountToRemove) {
		$this->childDiscounts->detach($childDiscountToRemove);
	}

	/**
	 * Returns the childDiscounts
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\ChildDiscount> $childDiscounts
	 */
	public function getChildDiscounts() {
		return $this->childDiscounts;
	}

	/**
	 * Sets the childDiscounts
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\ChildDiscount> $childDiscounts
	 * @return void
	 */
	public function setChildDiscounts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $childDiscounts) {
		$this->childDiscounts = $childDiscounts;
	}

	/**
	 * Returns the isStartour
	 *
	 * @return boolean $isStartour
	 */
	public function getIsStartour() {
		return $this->isStartour;
	}

	/**
	 * Sets the isStartour
	 *
	 * @param boolean $isStartour
	 * @return void
	 */
	public function setIsStartour($isStartour) {
		$this->isStartour = $isStartour;
	}

	/**
	 * Returns the boolean state of isStartour
	 *
	 * @return boolean
	 */
	public function isIsStartour() {
		return $this->isStartour;
	}

	/**
	 * Adds a BookableDate
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\BookableDate $bookableDate
	 * @return void
	 */
	public function addBookableDate(\JAKOTA\Reisedb\Domain\Model\BookableDate $bookableDate) {
		$this->bookableDates->attach($bookableDate);
	}

	/**
	 * Removes a BookableDate
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\BookableDate $bookableDateToRemove The BookableDate to be removed
	 * @return void
	 */
	public function removeBookableDate(\JAKOTA\Reisedb\Domain\Model\BookableDate $bookableDateToRemove) {
		$this->bookableDates->detach($bookableDateToRemove);
	}

	/**
	 * Returns the bookableDates
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\BookableDate> $bookableDates
	 */
	public function getBookableDates() {
		return $this->bookableDates;
	}

	/**
	 * Sets the bookableDates
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\BookableDate> $bookableDates
	 * @return void
	 */
	public function setBookableDates(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $bookableDates) {
		$this->bookableDates = $bookableDates;
	}

	/**
	 * Returns the shortDescription
	 *
	 * @return string $shortDescription
	 */
	public function getShortDescription() {
		return $this->shortDescription;
	}

	/**
	 * Sets the shortDescription
	 *
	 * @param string $shortDescription
	 * @return void
	 */
	public function setShortDescription($shortDescription) {
		$this->shortDescription = $shortDescription;
	}

	/**
	 * Adds a Review
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Review $review
	 * @return void
	 */
	public function addReview(\JAKOTA\Reisedb\Domain\Model\Review $review) {
		$this->reviews->attach($review);
	}

	/**
	 * Removes a Review
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Review $reviewToRemove The Review to be removed
	 * @return void
	 */
	public function removeReview(\JAKOTA\Reisedb\Domain\Model\Review $reviewToRemove) {
		$this->reviews->detach($reviewToRemove);
	}

	/**
	 * Returns the reviews
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Review> $reviews
	 */
	public function getReviews() {
		return $this->reviews;
	}

	/**
	 * Sets the reviews
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Review> $reviews
	 * @return void
	 */
	public function setReviews(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $reviews) {
		$this->reviews = $reviews;
	}

	/**
	 * Adds a EventPackage
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackage $eventPackage
	 * @return void
	 */
	public function addEventPackage(\JAKOTA\Reisedb\Domain\Model\EventPackage $eventPackage) {
		$this->eventPackages->attach($eventPackage);
	}

	/**
	 * Removes a EventPackage
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackage $eventPackageToRemove The EventPackage to be removed
	 * @return void
	 */
	public function removeEventPackage(\JAKOTA\Reisedb\Domain\Model\EventPackage $eventPackageToRemove) {
		$this->eventPackages->detach($eventPackageToRemove);
	}

	/**
	 * Returns the EventPackages
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackage> $eventPackages
	 */
	public function getEventPackages() {
		return $this->eventPackages;
	}

	/**
	 * Sets the EventPackages
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackage> $eventPackages
	 * @return void
	 */
	public function setEventPackages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $eventPackages) {
		$this->eventPackages = $eventPackages;
	}

	/**
	 * Adds a category
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category) {
		$this->categories->attach($category);
	}

	/**
	 * Removes a category
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove The BookableDate to be removed
	 * @return void
	 */
	public function removeCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove) {
		$this->categories->detach($categoryToRemove);
	}

	/**
	 * Returns the categories
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 * @return void
	 */
	public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories) {
		$this->categories = $categories;
	}

	/**
	 * Returns the verbalBookable
	 *
	 * @return string $verbalBookable
	 */
	public function getVerbalBookable() {
		return $this->verbalBookable;
	}

	/**
	 * Sets the verbalBookable
	 *
	 * @param string $verbalBookable
	 * @return void
	 */
	public function setVerbalBookable($verbalBookable) {
		$this->verbalBookable = $verbalBookable;
	}

	public function getRentalBikeName($id) {
		$aditionalServices = $this->getAdditionalServices();

		foreach($aditionalServices as $aditionalService) {
			if ($aditionalService->getIsRentalBike()) {
				$name = $aditionalService->getName();
				$uid = $aditionalService->getUid();

				if ($uid == $id) {
					return $name;
				}
			}
		}
		return '';
	}

	public function getRentalBikePrice($id, $date) {
		$aditionalServices = $this->getAdditionalServices();

		foreach($aditionalServices as $aditionalService) {
			if ($aditionalService->getIsRentalBike()) {
				$name = $aditionalService->getName();
				$uid = $aditionalService->getUid();

				if ($uid == $id) {
					$prices = $aditionalService->getPrice();
					$lastYear = 0;
					$lastPrice = null;
					$year = date('Y', strtotime($date));
					foreach($prices as $price) {
						if ($price->getYear() > $lastYear) $lastPrice = $price;
						if ($price->getYear() == $year) return $price->getPrice();
					}
					if ($lastPrice) {
						return $lastPrice->getPrice();
					} else {
						return '';
					}
				}
			}
		}
		return '';
	}

	public function calculatePersonsPrice($gp, $personUid) {
		$prices = $this->getPrices();
		$lastYear = 0;
		$lastPrice = null;

		$roomPrice = false;
		foreach($prices as $price) {
			if ($gp['pricecategory'] == $price->getPricecategory()->getUid()) {
				if ($price->getYear() > $lastYear) { $lastPrice = $price; $lastYear = $price->getYear(); }
				if ($price->getYear() == date('Y')) $roomPrice = $price->getPrice();
			}
		}
		if (!$roomPrice && $lastPrice) {
			$roomPrice = $lastPrice->getPrice();
		} else {
			return false;
		}
		return $roomPrice;
	}

	public function earlyBirdDiscount($gp, $personUid) {
		$thisMonth = date('n');
		$thisYear = date('Y');

		$tz  = new \DateTimeZone('Europe/Brussels');
		$arrivalDate = \DateTime::createFromFormat('d.m.Y', $gp['arrival'], $tz);
		$arrivalYear = $arrivalDate->format('Y');

		if ($thisMonth <= 2) {
			return array('value' => 3, 'type' => 'percent');
		} else if ($thisYear < $arrivalYear) {
			return array('value' => 3, 'type' => 'percent');
		}
		return null;
	}

	public function seasonPrice($gp, $personUid) {

		$seasonPrices = $this->getSeasonPrices();

		$arrivalDate = \DateTime::createFromFormat('d.m.Y H:i:s', $gp['arrival'].' 00:00:00');

		foreach($seasonPrices as $seasonPrice) {
			if ($arrivalDate >= $seasonPrice->getFromDate() && $arrivalDate <= $seasonPrice->getToDate()) {
				foreach($seasonPrice->getInPricecategory() as $pricecategory) {
					if ($gp['pricecategory'] == $pricecategory->getUid()) {

						return array('value' => abs($seasonPrice->getValue()), 'type' => $seasonPrice->getType() == 0 ? 'total':'percent', 'operator' => $seasonPrice->getValue() <= 0 ? '-':'+');
					}
				}
			}
		}
		foreach($seasonPrices as $seasonPrice) {
			if ($arrivalDate >= $seasonPrice->getFromDate() && $arrivalDate <= $seasonPrice->getToDate()) {
				if (count($seasonPrice->getInPricecategory()) == 0) {
					return array('value' => abs($seasonPrice->getValue()), 'type' => $seasonPrice->getType() == 0 ? 'total':'percent', 'operator' => $seasonPrice->getValue() <= 0 ? '-':'+');
				}
			}
		}
		return null;
	}

	public function groupDiscount($gp, $personUid) {

		$groupDiscounts = $this->getGroupDiscounts();

		$arrivalDate = \DateTime::createFromFormat('d.m.Y H:i:s', $gp['arrival'].' 00:00:00');

		foreach($groupDiscounts as $groupDiscount) {
			if ($arrivalDate >= $groupDiscount->getFromDay() && $arrivalDate <= $groupDiscount->getToDay()) {
				if ($gp['numberOfPersons'] >= 5 && $gp['numberOfPersons'] <= 6) {
					return array('value' => 4, 'type' => 'percent');
				}
				if ($gp['numberOfPersons'] >= 7 && $gp['numberOfPersons'] <= 8) {
					return array('value' => 6, 'type' => 'percent');
				}
				if ($gp['numberOfPersons'] >= 9) {
					return array('value' => 8, 'type' => 'percent');
				}
			}
		}
		return null;
	}

	public function roomDiscount($gp, $personUid) {
		$isInAdditionalBed = $this->isInAdditionalBed($gp, $personUid);
		$age = $this->personAge($gp, $personUid);

		$numberOfAdults = $gp['numberOfPersons']-$gp['numberOfChildren'];
		$numberOfChildren = $gp['numberOfChildren'];
		$numberOfAdultsInRoom = $this->numberOfAdultsInRoom($gp, $personUid);

		if ($this->getType() == 0) {
			$childDiscounts = $this->getChildDiscounts();
			if (count($childDiscounts) > 0) {
				$childDiscounts = $this->getChildDiscounts();
				foreach($childDiscounts as $childDiscount) {
					if ($age >= $childDiscount->getFromAge() && $age <= $childDiscount->getToAge()) {
						if ($childDiscount->getTypeOfBed() == 0 && $numberOfAdultsInRoom == 2) {
							return array('value' => abs($childDiscount->getValue()), 'type' => $childDiscount->getType() == 0 ? 'total':'percent');
						}
						if ($childDiscount->getTypeOfBed() == 1 && $numberOfAdults >= $childDiscount->getNumberAdult()) {
							return array('value' => abs($childDiscount->getValue()), 'type' => $childDiscount->getType() == 0 ? 'total':'percent');
						}
					}
				}
				return null;
			} else {
				// Wenn es eine Originaltour ist
				if ($age <= 4) {
					return array('value' => 10, 'type' => 'fix', 'operator' => '+');
				} else if ($age < 15) {
					if ($isInAdditionalBed) {
						// ist das Kind in der Aufbettung
						if ($numberOfAdultsInRoom >= 2) {
							return array('value' => 40, 'type' => 'percent', 'operator' => '-');
						} else {
							return array('value' => 15, 'type' => 'percent', 'operator' => '-');
						}
					} else {
						// Kind ist nicht in der Aufbettung
						if ($numberOfAdults >= $numberOfChildren) {
							return array('value' => 15, 'type' => 'percent', 'operator' => '-');
						}
					}
				} else {
					if ($isInAdditionalBed) {
						if ($numberOfAdultsInRoom > 2) {
							return array('value' => 10, 'type' => 'percent', 'operator' => '-');
						}
					}
					return null;
				}
			}


		} else {
			$childDiscounts = $this->getChildDiscounts();
			foreach($childDiscounts as $childDiscount) {
				if ($age >= $childDiscount->getFromAge() && $age <= $childDiscount->getToAge()) {
					if ($childDiscount->getTypeOfBed() == 0 && $numberOfAdultsInRoom == 2) {
						return array('value' => abs($childDiscount->getValue()), 'type' => $childDiscount->getType() == 0 ? 'total':'percent');
					}
					if ($childDiscount->getTypeOfBed() == 1 && $numberOfAdults >= $childDiscount->getNumberAdult()) {
						return array('value' => abs($childDiscount->getValue()), 'type' => $childDiscount->getType() == 0 ? 'total':'percent');
					}
				}
			}
			return null;
		}
	}

	public function calculatePersonsPriceFinal($gp, $personUid) {

		$roomUid = $this->sleepsInRoom($gp, $personUid);

		$tz  = new \DateTimeZone('Europe/Brussels');
		$arrivalDate = \DateTime::createFromFormat('d.m.Y', $gp['arrival'], $tz);
		$arrivalYear = $arrivalDate->format('Y');
		$roomPrice = $this->roomPrice($roomUid, $arrivalYear, $gp['pricecategory']);

		$groupDiscount = $this->groupDiscount($gp, $personUid);
		$seasonPrice = $this->seasonPrice($gp, $personUid);
		$roomDiscount = $this->roomDiscount($gp, $personUid);
		$earlyBirdDiscount = $this->earlyBirdDiscount($gp, $personUid);


		if ($seasonPrice) {
			switch ($seasonPrice['type']) {
				case 'percent':
					if ($seasonPrice['operator'] == '+') {
						$roomPrice = $roomPrice + ($roomPrice * ($seasonPrice['value'] / 100));
					} else {
						$roomPrice = $roomPrice - ($roomPrice * ($seasonPrice['value'] / 100));
					}

					break;
				case 'total':
					if ($seasonPrice['operator'] == '+') {
						$roomPrice = $roomPrice + $seasonPrice['value'];
					} else {
						$roomPrice = $roomPrice - $seasonPrice['value'];
					}
					break;
				case 'fix':
					$roomPrice = $seasonPrice['value']*$gp['nights'];
					break;
			}
		}

        if ($roomDiscount) {
            switch ($roomDiscount['type']) {
                case 'percent':  $roomPrice = $roomPrice - ($roomPrice * ($roomDiscount['value'] / 100)); break;
                case 'total':  $roomPrice = $roomPrice - $roomDiscount['value']; break;
                case 'fix':  $roomPrice = $roomDiscount['value']*$gp['nights']; break;
            }
        }

		if ($groupDiscount && !$roomDiscount) {
			switch ($groupDiscount['type']) {
				case 'percent':  $roomPrice = $roomPrice - ($roomPrice * ($groupDiscount['value'] / 100)); break;
				case 'total':  $roomPrice = $roomPrice - $groupDiscount['value']; break;
				case 'fix':  $roomPrice = $groupDiscount['value']*$gp['nights']; break;
			}
		}

		if ($earlyBirdDiscount && !$roomDiscount) {
			switch ($earlyBirdDiscount['type']) {
				case 'percent':  $roomPrice = $roomPrice - ($roomPrice * ($earlyBirdDiscount['value'] / 100)); break;
				case 'total':  $roomPrice = $roomPrice - $earlyBirdDiscount['value']; break;
				case 'fix':  $roomPrice = $earlyBirdDiscount['value']*$gp['nights']; break;
			}
		}
		return $roomPrice;
	}

	private function roomPrice($roomUid, $arrivalYear, $pricecategory) {
		$prices = $this->getPrices();
		$lastYear = 0;
		$lastPrice = null;

		$roomPrice = false;
		foreach($prices as $price) {
			if ($pricecategory == $price->getPricecategory()->getUid() && $roomUid == $price->getRoomcategory()->getUid()) {
				if ($price->getYear() > $lastYear) { $lastPrice = $price; $lastYear = $price->getYear(); }
				if ($price->getYear() == $arrivalYear) return $price->getPrice();
			}
		}

		if (!$roomPrice && $lastPrice) {
			return $lastPrice->getPrice();
		} else {
			return false;
		}
	}

	private function numberOfAdultsInRoom($gp, $personUid) {
		$isTheRoom = false;
		$numberOfAdults = 0;
		foreach($gp['rooms'] as $room) {
			foreach($room['beds'] as $bed) {
				if ($bed['uid'] == $personUid) {
					$isTheRoom = true;
				};
				if ($this->personAge($gp, $bed['uid']) >= 99) {
					$numberOfAdults++;
				}
			}
			if (array_key_exists('additionalBed', $room)) {
				foreach($room['additionalBed'] as $bed) {
					if ($bed['uid'] == $personUid) {
						$isTheRoom = true;
					};
					if ($this->personAge($gp, $bed['uid']) >= 99) {
						$numberOfAdults++;
					}
				}
			}
			if ($isTheRoom) {
				return $numberOfAdults;
			}
			$numberOfAdults = 0;

		}
		return null;
	}

	private function isInAdditionalBed($gp, $personUid) {
		foreach($gp['rooms'] as $room) {
			foreach($room['beds'] as $bed) {
				if ($bed['uid'] == $personUid) return false;
			}
			if (array_key_exists('additionalBed', $room)) {
				foreach($room['additionalBed'] as $bed) {
					if ($bed['uid'] == $personUid) return true;
				}
			}

		}
		return null;
	}

	private function sleepsInRoom($gp, $personUid) {
		foreach($gp['rooms'] as $room) {
			foreach($room['beds'] as $bed) {
				if ($bed['uid'] == $personUid) return $room['uid'];
			}
			if (array_key_exists('additionalBed', $room)) {
				foreach($room['additionalBed'] as $bed) {
					if ($bed['uid'] == $personUid) return $room['uid'];
				}
			}

		}
		return null;
	}

	private function personAge($gp, $personUid) {
		foreach($gp['persons'] as $person) {
			if ($person['uid'] == $personUid) {
				if (preg_match('/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/', $person['birthday']) && preg_match('/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/', $gp['arrival'])) {

					$tz  = new \DateTimeZone('Europe/Brussels');
					$birthDate = \DateTime::createFromFormat('d.m.Y', $person['birthday'], $tz);
					$arrivalDate = \DateTime::createFromFormat('d.m.Y', $gp['arrival'], $tz);

					return $birthDate->diff($arrivalDate)->y;
				}
				return 99;
			}
		}
		return 99;
	}

	public function getRoomPrice($room, $pricecategory, $bed, $date, $birthday, $persons) {
		$date = strtotime($date);
		$prices = $this->getPrices();
		$lastYear = 0;
		$lastPrice = null;

		$roomPrice = false;
		foreach($prices as $price) {



			if ($pricecategory == $price->getPricecategory()->getUid() && $room['uid'] == $price->getRoomcategory()->getUid()) {
				if ($price->getYear() > $lastYear) { $lastPrice = $price; $lastYear = $price->getYear(); }
				if ($price->getYear() == date('Y', $date)) $roomPrice = $price->getPrice();
			}

		}
		if (!$roomPrice && $lastPrice) {
			$roomPrice = $lastPrice->getPrice();
		} else {
			return false;
		}

		// Kinderermäßigung
		$birthday = strtotime($birthday);

		if ($birthday) {
			$age = floor(($date - $birthday) / (3600 * 24 * 365));
			if ($this->getType() == 0) {

			} else {
				if ($age >= 0 && $age <= 4) {
					$roomPrice = 0;
				} else if ($age >= 5 && $age <= 15) {

				}
			}
		}

		if ($roomPrice) {
			return $roomPrice;
		} else {
			return false;
		}


	}

	/**
	 * Returns the additionalInfo
	 *
	 * @return int $additionalInfo
	 */
	public function getAdditionalInfo()
	{
		return $this->additionalInfo;
	}

	/**
	 * Sets the additionalInfo
	 *
	 * @param int $additionalInfo
	 * @return void
	 */
	public function setAdditionalInfo($additionalInfo)
	{
		$this->additionalInfo = $additionalInfo;
	}


}