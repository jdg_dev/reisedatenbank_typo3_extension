<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Station
 */
class Station extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * code
	 *
	 * @var string
	 */
	protected $code = '';

	/**
	 * tracklength
	 *
	 * @var float
	 */
	protected $tracklength = 0.0;

	/**
	 * tracklengthApprox
	 *
	 * @var string
	 */
	protected $tracklengthApprox = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * image
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image = NULL;

	/**
	 * additionalNightDescription
	 *
	 * @var string
	 */
	protected $additionalNightDescription = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * addtionalNightPrices
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price>
	 * @cascade remove
	 */
	protected $addtionalNightPrices = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->addtionalNightPrices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the code
	 *
	 * @return string $code
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * Sets the code
	 *
	 * @param string $code
	 * @return void
	 */
	public function setCode($code) {
		$this->code = $code;
	}

	/**
	 * Returns the tracklength
	 *
	 * @return float $tracklength
	 */
	public function getTracklength() {
		return $this->tracklength;
	}

	/**
	 * Sets the tracklength
	 *
	 * @param float $tracklength
	 * @return void
	 */
	public function setTracklength($tracklength) {
		$this->tracklength = $tracklength;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
		$this->image = $image;
	}

	/**
	 * Returns the additionalNightDescription
	 *
	 * @return string $additionalNightDescription
	 */
	public function getAdditionalNightDescription() {
		return $this->additionalNightDescription;
	}

	/**
	 * Sets the additionalNightDescription
	 *
	 * @param string $additionalNightDescription
	 * @return void
	 */
	public function setAdditionalNightDescription($additionalNightDescription) {
		$this->additionalNightDescription = $additionalNightDescription;
	}

	/**
	 * Adds a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $addtionalNightPrice
	 * @return void
	 */
	public function addAddtionalNightPrice(\JAKOTA\Reisedb\Domain\Model\Price $addtionalNightPrice) {
		$this->addtionalNightPrices->attach($addtionalNightPrice);
	}

	/**
	 * Removes a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $addtionalNightPriceToRemove The Price to be removed
	 * @return void
	 */
	public function removeAddtionalNightPrice(\JAKOTA\Reisedb\Domain\Model\Price $addtionalNightPriceToRemove) {
		$this->addtionalNightPrices->detach($addtionalNightPriceToRemove);
	}

	/**
	 * Returns the addtionalNightPrices
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $addtionalNightPrices
	 */
	public function getAddtionalNightPrices() {
		return $this->addtionalNightPrices;
	}

	/**
	 * Sets the addtionalNightPrices
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $addtionalNightPrices
	 * @return void
	 */
	public function setAddtionalNightPrices(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $addtionalNightPrices) {
		$this->addtionalNightPrices = $addtionalNightPrices;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the tracklengthApprox
	 *
	 * @return string $tracklengthApprox
	 */
	public function getTracklengthApprox() {
		return $this->tracklengthApprox;
	}

	/**
	 * Sets the tracklengthApprox
	 *
	 * @param string $tracklengthApprox
	 * @return void
	 */
	public function setTracklengthApprox($tracklengthApprox) {
		$this->tracklengthApprox = $tracklengthApprox;
	}

}