<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Review
 */
class Review extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * ratingLuggage
	 *
	 * @var integer
	 */
	protected $ratingLuggage = 0;

	/**
	 * ratingDocuments
	 *
	 * @var integer
	 */
	protected $ratingDocuments = 0;

	/**
	 * ratingCare
	 *
	 * @var integer
	 */
	protected $ratingCare = 0;

	/**
	 * ratingConsultation
	 *
	 * @var integer
	 */
	protected $ratingConsultation = 0;

	/**
	 * ratingHotels
	 *
	 * @var integer
	 */
	protected $ratingHotels = 0;

	/**
	 * ratingQuality
	 *
	 * @var integer
	 */
	protected $ratingQuality = 0;

	/**
	 * ratingOverall
	 *
	 * @var integer
	 */
	protected $ratingOverall = 0;

	/**
	 * quality
	 *
	 * @var integer
	 */
	protected $quality = 0;

	/**
	 * images
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @cascade remove
	 */
	protected $images = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the ratingLuggage
	 *
	 * @return integer $ratingLuggage
	 */
	public function getRatingLuggage() {
		return $this->ratingLuggage;
	}

	/**
	 * Sets the ratingLuggage
	 *
	 * @param integer $ratingLuggage
	 * @return void
	 */
	public function setRatingLuggage($ratingLuggage) {
		$this->ratingLuggage = $ratingLuggage;
	}

	/**
	 * Returns the ratingDocuments
	 *
	 * @return integer $ratingDocuments
	 */
	public function getRatingDocuments() {
		return $this->ratingDocuments;
	}

	/**
	 * Sets the ratingDocuments
	 *
	 * @param integer $ratingDocuments
	 * @return void
	 */
	public function setRatingDocuments($ratingDocuments) {
		$this->ratingDocuments = $ratingDocuments;
	}

	/**
	 * Returns the ratingCare
	 *
	 * @return integer $ratingCare
	 */
	public function getRatingCare() {
		return $this->ratingCare;
	}

	/**
	 * Sets the ratingCare
	 *
	 * @param integer $ratingCare
	 * @return void
	 */
	public function setRatingCare($ratingCare) {
		$this->ratingCare = $ratingCare;
	}

	/**
	 * Returns the ratingConsultation
	 *
	 * @return integer $ratingConsultation
	 */
	public function getRatingConsultation() {
		return $this->ratingConsultation;
	}

	/**
	 * Sets the ratingConsultation
	 *
	 * @param integer $ratingConsultation
	 * @return void
	 */
	public function setRatingConsultation($ratingConsultation) {
		$this->ratingConsultation = $ratingConsultation;
	}

	/**
	 * Returns the ratingHotels
	 *
	 * @return integer $ratingHotels
	 */
	public function getRatingHotels() {
		return $this->ratingHotels;
	}

	/**
	 * Sets the ratingHotels
	 *
	 * @param integer $ratingHotels
	 * @return void
	 */
	public function setRatingHotels($ratingHotels) {
		$this->ratingHotels = $ratingHotels;
	}

	/**
	 * Returns the ratingQuality
	 *
	 * @return integer $ratingQuality
	 */
	public function getRatingQuality() {
		return $this->ratingQuality;
	}

	/**
	 * Sets the ratingQuality
	 *
	 * @param integer $ratingQuality
	 * @return void
	 */
	public function setRatingQuality($ratingQuality) {
		$this->ratingQuality = $ratingQuality;
	}

	/**
	 * Returns the ratingOverall
	 *
	 * @return integer $ratingOverall
	 */
	public function getRatingOverall() {
		return $this->ratingOverall;
	}

	/**
	 * Sets the ratingOverall
	 *
	 * @param integer $ratingOverall
	 * @return void
	 */
	public function setRatingOverall($ratingOverall) {
		$this->ratingOverall = $ratingOverall;
	}

	/**
	 * Returns the quality
	 *
	 * @return integer $quality
	 */
	public function getQuality() {
		return $this->quality;
	}

	/**
	 * Sets the quality
	 *
	 * @param integer $quality
	 * @return void
	 */
	public function setQuality($quality) {
		$this->quality = $quality;
	}

	/**
	 * Adds a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
		$this->images->attach($image);
	}

	/**
	 * Removes a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
	 * @return void
	 */
	public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove) {
		$this->images->detach($imageToRemove);
	}

	/**
	 * Returns the images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Sets the images
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
	 * @return void
	 */
	public function setImages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $images) {
		$this->images = $images;
	}

}