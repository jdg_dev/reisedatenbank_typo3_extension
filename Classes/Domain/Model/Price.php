<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Price
 */
class Price extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * fromPrice
	 *
	 * @var boolean
	 */
	protected $fromPrice = FALSE;

	/**
	 * price
	 *
	 * @var float
	 */
	protected $price = 0.0;

	/**
	 * type
	 *
	 * @var integer
	 */
	protected $type = 0;

	/**
	 * year
	 *
	 * @var integer
	 */
	protected $year = 0;

	/**
	 * halfboard
	 *
	 * @var float
	 */
	protected $halfboard = 0.0;

	/**
	 * pricecategory
	 *
	 * @var \JAKOTA\Reisedb\Domain\Model\Pricecategory
	 */
	protected $pricecategory = NULL;

	/**
	 * roomcategory
	 *
	 * @var \JAKOTA\Reisedb\Domain\Model\Roomcategory
	 */
	protected $roomcategory = NULL;

	/**
	 * Returns the fromPrice
	 *
	 * @return boolean $fromPrice
	 */
	public function getFromPrice() {
		return $this->fromPrice;
	}

	/**
	 * Sets the fromPrice
	 *
	 * @param boolean $fromPrice
	 * @return void
	 */
	public function setFromPrice($fromPrice) {
		$this->fromPrice = $fromPrice;
	}

	/**
	 * Returns the boolean state of fromPrice
	 *
	 * @return boolean
	 */
	public function isFromPrice() {
		return $this->fromPrice;
	}

	/**
	 * Returns the price
	 *
	 * @return float $price
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * Sets the price
	 *
	 * @param float $price
	 * @return void
	 */
	public function setPrice($price) {
		$this->price = $price;
	}

	/**
	 * Returns the type
	 *
	 * @return integer $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param integer $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Returns the year
	 *
	 * @return integer $year
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * Sets the year
	 *
	 * @param integer $year
	 * @return void
	 */
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	 * Returns the pricecategory
	 *
	 * @return \JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory
	 */
	public function getPricecategory() {
		return $this->pricecategory;
	}

	/**
	 * Sets the pricecategory
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory
	 * @return void
	 */
	public function setPricecategory(\JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory) {
		$this->pricecategory = $pricecategory;
	}

	/**
	 * Returns the roomcategory
	 *
	 * @return \JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory
	 */
	public function getRoomcategory() {
		return $this->roomcategory;
	}

	/**
	 * Sets the roomcategory
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory
	 * @return void
	 */
	public function setRoomcategory(\JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory) {
		$this->roomcategory = $roomcategory;
	}

	/**
	 * Returns the halfboard
	 *
	 * @return float $halfboard
	 */
	public function getHalfboard() {
		return $this->halfboard;
	}

	/**
	 * Sets the halfboard
	 *
	 * @param float $halfboard
	 * @return void
	 */
	public function setHalfboard($halfboard) {
		$this->halfboard = $halfboard;
	}

}