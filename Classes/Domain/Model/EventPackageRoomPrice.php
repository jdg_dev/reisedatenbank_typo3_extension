<?php
namespace JAKOTA\Reisedb\Domain\Model;

    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * EventPackageRoomPrice
 */
class EventPackageRoomPrice extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * roomcategory
     *
     * @var \JAKOTA\Reisedb\Domain\Model\Roomcategory
     */
    protected $roomcategory = NULL;

    /**
     * pricecategory
     *
     * @var \JAKOTA\Reisedb\Domain\Model\Pricecategory
     */
    protected $pricecategory = NULL;

    /**
     * price
     *
     * @var \double
     */
    protected $price;

    /**
     * dategroup
     *
     * @var \int
     */
    protected $dategroup;

    /**
     * Returns the roomcategory
     *
     * @return \JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory
     */
    public function getRoomcategory() {
        return $this->roomcategory;
    }

    /**
     * Sets the roomcategory
     *
     * @param \JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory
     * @return void
     */
    public function setRoomcategory(\JAKOTA\Reisedb\Domain\Model\Roomcategory $roomcategory) {
        $this->roomcategory = $roomcategory;
    }

    /**
     * Returns the pricecategory
     *
     * @return \JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory
     */
    public function getPricecategory() {
        return $this->pricecategory;
    }

    /**
     * Sets the pricecategory
     *
     * @param \JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory
     * @return void
     */
    public function setPricecategory(\JAKOTA\Reisedb\Domain\Model\Pricecategory $pricecategory) {
        $this->pricecategory = $pricecategory;
    }

    /**
     * Returns the price
     *
     * @return \double $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param \double $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Returns the dategroup
     *
     * @return \int $dategroup
     */
    public function getDategroup()
    {
        return $this->dategroup;
    }

    /**
     * Sets the dategroup
     *
     * @param \int $dategroup
     * @return void
     */
    public function setDategroup($dategroup)
    {
        $this->dategroup = $dategroup;
    }

}