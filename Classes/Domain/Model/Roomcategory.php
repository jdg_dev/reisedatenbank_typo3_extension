<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Roomcategory
 */
class Roomcategory extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * beds
	 *
	 * @var integer
	 */
	protected $beds = 0;

	/**
	 * additionalBed
	 *
	 * @var boolean
	 */
	protected $additionalBed = FALSE;

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the beds
	 *
	 * @return integer $beds
	 */
	public function getBeds() {
		return $this->beds;
	}

	/**
	 * Sets the beds
	 *
	 * @param integer $beds
	 * @return void
	 */
	public function setBeds($beds) {
		$this->beds = $beds;
	}

	/**
	 * Returns the additionalBed
	 *
	 * @return boolean $additionalBed
	 */
	public function getAdditionalBed() {
		return $this->additionalBed;
	}

	/**
	 * Sets the additionalBed
	 *
	 * @param boolean $additionalBed
	 * @return void
	 */
	public function setAdditionalBed($additionalBed) {
		$this->additionalBed = $additionalBed;
	}

	/**
	 * Returns the boolean state of additionalBed
	 *
	 * @return boolean
	 */
	public function isAdditionalBed() {
		return $this->additionalBed;
	}

}