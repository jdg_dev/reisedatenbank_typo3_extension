<?php
namespace JAKOTA\Reisedb\Domain\Model;

    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * EventPackageExtraData
 */
class EventPackageExtraData extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var \string
     */
    protected $title;

    /**
     * fieldtype
     *
     * @var \string
     */
    protected $fieldtype;

    /**
     * selectFields
     *
     * @var \string
     */
    protected $selectFields;

    /**
     * dategroup
     *
     * @var \int
     */
    protected $dategroup;

    /**
     * Returns the title
     *
     * @return \string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param \string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the fieldtype
     *
     * @return \string $fieldtype
     */
    public function getFieldtype()
    {
        return $this->fieldtype;
    }

    /**
     * Sets the fieldtype
     *
     * @param \string $fieldtype
     * @return void
     */
    public function setFieldtype($fieldtype)
    {
        $this->fieldtype = $fieldtype;
    }

    /**
     * Returns the selectFields
     *
     * @return \string $selectFields
     */
    public function getSelectFields()
    {
        return $this->selectFields;
    }

    /**
     * Sets the selectFields
     *
     * @param \string $selectFields
     * @return void
     */
    public function setSelectFields($selectFields)
    {
        $this->selectFields = $selectFields;
    }

    /**
     * Returns the dategroup
     *
     * @return \int $dategroup
     */
    public function getDategroup()
    {
        return $this->dategroup;
    }

    /**
     * Sets the dategroup
     *
     * @param \int $dategroup
     * @return void
     */
    public function setDategroup($dategroup)
    {
        $this->dategroup = $dategroup;
    }

}