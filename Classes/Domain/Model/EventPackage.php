<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * EventPackage
 */
class EventPackage extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * description
	 *
	 * @var \string
	 */
	protected $description;

	/**
	 * city
	 *
	 * @var \string
	 */
	protected $city;

	/**
	 * detailpage
	 *
	 * @var \int
	 */
	protected $detailpage;

	/**
	 * extraNights
	 *
	 * @var \int
	 */
	protected $extraNights;

	/**
	 * groupPackage
	 *
	 * @var \boolean
	 */
	protected $groupPackage;

	/**
	 * typeofprice
	 *
	 * @var \int
	 */
	protected $typeofprice;

	/**
	 * extraData
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageExtraData>
	 * @cascade remove
	 * @lazy
	 */
	protected $extraData;

	/**
	 * fixprice
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice>
	 * @cascade remove
	 * @lazy
	 */
	protected $fixprice;

	/**
	 * roomprice
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice>
	 * @cascade remove
	 * @lazy
	 */
	protected $roomprice;

	/**
	 * scaledprice
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice>
	 * @cascade remove
	 * @lazy
	 */
	protected $scaledprice;

	/**
	 * availability
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageAvailability>
	 * @cascade remove
	 * @lazy
	 */
	protected $availability;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->extraData = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->fixprice = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->roomprice = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->scaledprice = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return \string $description
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param \string $description
	 * @return void
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	

	/**
	 * Returns the city
	 *
	 * @return \string $city
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param \string $city
	 * @return void
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * Returns the detailpage
	 *
	 * @return \int $detailpage
	 */
	public function getDetailpage()
	{
		return $this->detailpage;
	}

	/**
	 * Sets the detailpage
	 *
	 * @param \int $detailpage
	 * @return void
	 */
	public function setDetailpage($detailpage)
	{
		$this->detailpage = $detailpage;
	}

	/**
	 * Returns the extraNights
	 *
	 * @return \int $extraNights
	 */
	public function getExtraNights()
	{
		return $this->extraNights;
	}

	/**
	 * Sets the extraNights
	 *
	 * @param \int $extraNights
	 * @return void
	 */
	public function setExtraNights($extraNights)
	{
		$this->extraNights = $extraNights;
	}

	/**
	 * Returns the typeofprice
	 *
	 * @return \int $typeofprice
	 */
	public function getTypeofprice()
	{
		return $this->typeofprice;
	}

	/**
	 * Sets the typeofprice
	 *
	 * @param \int $typeofprice
	 * @return void
	 */
	public function setTypeofprice($typeofprice)
	{
		$this->typeofprice = $typeofprice;
	}

	/**
	 * Returns the groupPackage
	 *
	 * @return \boolean $groupPackage
	 */
	public function getGroupPackage()
	{
		return $this->groupPackage;
	}

	/**
	 * Sets the groupPackage
	 *
	 * @param \boolean $groupPackage
	 * @return void
	 */
	public function setGroupPackage($groupPackage)
	{
		$this->groupPackage = $groupPackage;
	}

	/**
	 * Adds a EventPackageExtraData
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageExtraData $eventPackageExtraData
	 * @return void
	 */
	public function addExtraData(\JAKOTA\Reisedb\Domain\Model\EventPackageExtraData $eventPackageExtraData) {
		$this->extraData->attach($eventPackageExtraData);
	}

	/**
	 * Removes a EventPackageExtraData
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageExtraData $eventPackageExtraDataToRemove The EventPackageExtraData to be removed
	 * @return void
	 */
	public function removeExtraData(\JAKOTA\Reisedb\Domain\Model\EventPackageExtraData $eventPackageExtraData) {
		$this->extraData->detach($eventPackageExtraData);
	}

	/**
	 * Returns the EventPackageExtraData
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageExtraData> $eventPackageExtraData
	 */
	public function getExtraData() {
		return $this->extraData;
	}

	/**
	 * Sets the EventPackageExtraData
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageExtraData> $eventPackageExtraData
	 * @return void
	 */
	public function setExtraData(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $eventPackageExtraData) {
		$this->extraData = $eventPackageExtraData;
	}

	/**
	 * Adds a EventPackageFixedPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice $eventPackageExtraData
	 * @return void
	 */
	public function addFixprice(\JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice $fixprice) {
		$this->fixprice->attach($fixprice);
	}

	/**
	 * Removes a EventPackageFixedPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice $eventPackageExtraDataToRemove The EventPackageExtraData to be removed
	 * @return void
	 */
	public function removeFixprice(\JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice $fixprice) {
		$this->fixprice->detach($fixprice);
	}

	/**
	 * Returns the EventPackageFixedPrice
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice> $fixprice
	 */
	public function getFixprice() {
		return $this->fixprice;
	}

	/**
	 * Sets the EventPackageFixedPrice
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageFixedPrice> $fixprice
	 * @return void
	 */
	public function setFixprice(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $fixprice) {
		$this->fixprice = $fixprice;
	}

	/**
	 * Adds a EventPackageRoomPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice roomprice
	 * @return void
	 */
	public function addRoomprice(\JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice $roomprice) {
		$this->roomprice->attach($roomprice);
	}

	/**
	 * Removes a EventPackageRoomPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice $roomprice The EventPackageExtraData to be removed
	 * @return void
	 */
	public function removeRoomprice(\JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice $roomprice) {
		$this->roomprice->detach($roomprice);
	}

	/**
	 * Returns the EventPackageRoomPrice
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice> $roomprice
	 */
	public function getRoomprice() {
		return $this->roomprice;
	}

	/**
	 * Sets the EventPackageRoomPrice
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageRoomPrice> $roomprice
	 * @return void
	 */
	public function setRoomprice(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $roomprice) {
		$this->roomprice = $roomprice;
	}

	/**
	 * Adds a EventPackageScaledPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice $scaledprice
	 * @return void
	 */
	public function addScaledprice(\JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice $scaledprice) {
		$this->scaledprice->attach($scaledprice);
	}

	/**
	 * Removes a EventPackageScaledPrice
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice $scaledprice The EventPackageScaledPrice to be removed
	 * @return void
	 */
	public function removeScaledprice(\JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice $scaledprice) {
		$this->scaledprice->detach($scaledprice);
	}

	/**
	 * Returns the EventPackageScaledPrice
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice> $scaledprice
	 */
	public function getScaledprice() {
		return $this->scaledprice;
	}

	/**
	 * Sets the EventPackageScaledPrice
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageScaledPrice> $scaledprice
	 * @return void
	 */
	public function setScaledprice(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $scaledprice) {
		$this->scaledprice = $scaledprice;
	}

	/**
	 * Returns the Availability
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\EventPackageAvailability>
	 */
	public function getAvailability() {
		return $this->availability;
	}

}