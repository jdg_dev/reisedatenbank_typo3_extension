<?php
namespace JAKOTA\Reisedb\Domain\Model;

    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * EventPackageScaledPrice
 */
class EventPackageScaledPrice extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * title
     *
     * @var \string
     */
    protected $title;

    /**
     * fromAge
     *
     * @var \int
     */
    protected $fromAge;

    /**
     * toAge
     *
     * @var \int
     */
    protected $toAge;

    /**
     * price
     *
     * @var \double
     */
    protected $price;

    /**
     * dategroup
     *
     * @var \int
     */
    protected $dategroup;

    /**
     * Returns the title
     *
     * @return \string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param \string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the fromAge
     *
     * @return \int $fromAge
     */
    public function getFromAge()
    {
        return $this->fromAge;
    }

    /**
     * Sets the fromAge
     *
     * @param \int $fromAge
     * @return void
     */
    public function setFromAge($fromAge)
    {
        $this->fromAge = $fromAge;
    }

    /**
     * Returns the toAge
     *
     * @return \int $toAge
     */
    public function getToAge()
    {
        return $this->toAge;
    }

    /**
     * Sets the toAge
     *
     * @param \int $toAge
     * @return void
     */
    public function setToAge($toAge)
    {
        $this->toAge = $toAge;
    }

    /**
     * Returns the price
     *
     * @return \double $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param \double $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Returns the dategroup
     *
     * @return \int $dategroup
     */
    public function getDategroup()
    {
        return $this->dategroup;
    }

    /**
     * Sets the dategroup
     *
     * @param \int $dategroup
     * @return void
     */
    public function setDategroup($dategroup)
    {
        $this->dategroup = $dategroup;
    }

}