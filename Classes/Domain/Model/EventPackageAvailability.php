<?php
namespace JAKOTA\Reisedb\Domain\Model;

    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * EventPackageAvailability
 */
class EventPackageAvailability extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * dayofavailability
     *
     * @var \DateTime
     */
    protected $dayofavailability;

    /**
     * available
     *
     * @var \int
     */
    protected $available;

    /**
     * dataGroup
     *
     * @var \int
     */
    protected $dataGroup;

    /**
     * priceGroup
     *
     * @var \int
     */
    protected $priceGroup;

    /**
     * Returns the dayofavailability
     *
     * @return \DateTime $dayofavailability
     */
    public function getDayofavailability()
    {
        return $this->dayofavailability;
    }

    /**
     * Sets the dayofavailability
     *
     * @param \DateTime $dayofavailability
     * @return void
     */
    public function setDayofavailability($dayofavailability)
    {
        $this->dayofavailability = $dayofavailability;
    }

    /**
     * Returns the available
     *
     * @return \int $available
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Sets the available
     *
     * @param \int $available
     * @return void
     */
    public function setAvailable($available)
    {
        $this->available = $available;
    }

    /**
     * Returns the dataGroup
     *
     * @return \int $dataGroup
     */
    public function getDataGroup()
    {
        return $this->dataGroup;
    }

    /**
     * Sets the dataGroup
     *
     * @param \int $dataGroup
     * @return void
     */
    public function setDataGroup($dataGroup)
    {
        $this->dataGroup = $dataGroup;
    }

    /**
     * Returns the priceGroup
     *
     * @return \int $priceGroup
     */
    public function getPriceGroup()
    {
        return $this->priceGroup;
    }

    /**
     * Sets the priceGroup
     *
     * @param \int $priceGroup
     * @return void
     */
    public function setPriceGroup($priceGroup)
    {
        $this->priceGroup = $priceGroup;
    }


}