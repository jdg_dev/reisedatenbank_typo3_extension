<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SeasonPrice
 */
class SeasonPrice extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * value
	 *
	 * @var float
	 */
	protected $value = 0.0;

	/**
	 * type
	 *
	 * @var integer
	 */
	protected $type = 0;

	/**
	 * fromDate
	 *
	 * @var \DateTime
	 */
	protected $fromDate = NULL;

	/**
	 * toDate
	 *
	 * @var \DateTime
	 */
	protected $toDate = NULL;

	/**
	 * inPricecategory
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Pricecategory>
	 */
	protected $inPricecategory = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->inPricecategory = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the value
	 *
	 * @return float $value
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Sets the value
	 *
	 * @param float $value
	 * @return void
	 */
	public function setValue($value) {
		$this->value = $value;
	}

	/**
	 * Returns the type
	 *
	 * @return integer $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param integer $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Adds a Pricecategory
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Pricecategory $inPricecategory
	 * @return void
	 */
	public function addInPricecategory(\JAKOTA\Reisedb\Domain\Model\Pricecategory $inPricecategory) {
		$this->inPricecategory->attach($inPricecategory);
	}

	/**
	 * Removes a Pricecategory
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Pricecategory $inPricecategoryToRemove The Pricecategory to be removed
	 * @return void
	 */
	public function removeInPricecategory(\JAKOTA\Reisedb\Domain\Model\Pricecategory $inPricecategoryToRemove) {
		$this->inPricecategory->detach($inPricecategoryToRemove);
	}

	/**
	 * Returns the inPricecategory
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Pricecategory> $inPricecategory
	 */
	public function getInPricecategory() {
		return $this->inPricecategory;
	}

	/**
	 * Sets the inPricecategory
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Pricecategory> $inPricecategory
	 * @return void
	 */
	public function setInPricecategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $inPricecategory) {
		$this->inPricecategory = $inPricecategory;
	}

	/**
	 * Returns the fromDate
	 *
	 * @return \DateTime $fromDate
	 */
	public function getFromDate() {
		return $this->fromDate;
	}

	/**
	 * Sets the fromDate
	 *
	 * @param \DateTime $fromDate
	 * @return void
	 */
	public function setFromDate(\DateTime $fromDate) {
		$this->fromDate = $fromDate;
	}

	/**
	 * Returns the toDate
	 *
	 * @return \DateTime $toDate
	 */
	public function getToDate() {
		return $this->toDate;
	}

	/**
	 * Sets the toDate
	 *
	 * @param \DateTime $toDate
	 * @return void
	 */
	public function setToDate(\DateTime $toDate) {
		$this->toDate = $toDate;
	}

}