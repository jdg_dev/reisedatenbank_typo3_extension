<?php
namespace JAKOTA\Reisedb\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * AdditionalService
 */
class AdditionalService extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * isRentalBike
	 *
	 * @var boolean
	 */
	protected $isRentalBike = FALSE;

	/**
	 * price
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price>
	 * @cascade remove
	 */
	protected $price = NULL;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the isRentalBike
	 *
	 * @return boolean $isRentalBike
	 */
	public function getIsRentalBike() {
		return $this->isRentalBike;
	}

	/**
	 * Sets the isRentalBike
	 *
	 * @param boolean $isRentalBike
	 * @return void
	 */
	public function setIsRentalBike($isRentalBike) {
		$this->isRentalBike = $isRentalBike;
	}

	/**
	 * Returns the boolean state of isRentalBike
	 *
	 * @return boolean
	 */
	public function isIsRentalBike() {
		return $this->isRentalBike;
	}

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->price = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $price
	 * @return void
	 */
	public function addPrice(\JAKOTA\Reisedb\Domain\Model\Price $price) {
		$this->price->attach($price);
	}

	/**
	 * Removes a Price
	 *
	 * @param \JAKOTA\Reisedb\Domain\Model\Price $priceToRemove The Price to be removed
	 * @return void
	 */
	public function removePrice(\JAKOTA\Reisedb\Domain\Model\Price $priceToRemove) {
		$this->price->detach($priceToRemove);
	}

	/**
	 * Returns the price
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $price
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * Sets the price
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JAKOTA\Reisedb\Domain\Model\Price> $price
	 * @return void
	 */
	public function setPrice(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $price) {
		$this->price = $price;
	}

}