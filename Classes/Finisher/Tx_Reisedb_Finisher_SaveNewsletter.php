<?php
/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *
 * $Id: Tx_Formhandler_Finisher_SubmittedOK.php 27790 2009-12-17 09:28:42Z reinhardfuehricht $
 *                                                                        */

require_once('typo3conf/ext/reisedb/Resources/PHP/mailchimp/MCAPI.class.php');
require_once('typo3conf/ext/reisedb/Resources/PHP/mailchimp/config.inc.php');

/**
 * A finisher showing the content of ###TEMPLATE_SUBMITTEDOK### replacing all common Formhandler markers
 * plus ###PRINT_LINK###, ###PDF_LINK### and ###CSV_LINK###.
 *
 * The finisher sets a flag in session, so that Formhandler will only call this finisher and nothing else if the user reloads the page.
 *
 *
 * @author	Martin Fünning <fuenning@jakota.de>
 */
class Tx_Reisedb_Finisher_SaveNewsletter extends Tx_Formhandler_AbstractFinisher {

    public $test = 'foobar';

    /**
     * The main method called by the controller
     *
     * @return array The probably modified GET/POST parameters
     */
    public function process() {
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump('ddd'); die;
        if ($this->gp['newsletter'] == 1) {
            //API Key - see http://admin.mailchimp.com/account/api
            $apikey = '25b1f429d0633f093bb725ad58650aa6-us5';

            // List ID "MRT Newsletter"
            $listId = '09b5c878f6';

            //just used in xml-rpc examples
            $apiUrl = 'http://api.mailchimp.com/1.3/';


            //*****************************************************************************************
            // Verbindung zu Mailchimp aufbauen
            //*****************************************************************************************
            $api = new MCAPI($apikey);

            //*****************************************************************************************
            // Email an Mailchimp übertragen
            //*****************************************************************************************
            $retval = $api->listSubscribe( $listId, $this->gp['email'], array());

        }

        return $this->gp;
    }

}