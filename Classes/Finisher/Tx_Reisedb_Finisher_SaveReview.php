<?php
/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *
 * $Id: Tx_Formhandler_Finisher_SubmittedOK.php 27790 2009-12-17 09:28:42Z reinhardfuehricht $
 *                                                                        */

/**
 * A finisher showing the content of ###TEMPLATE_SUBMITTEDOK### replacing all common Formhandler markers
 * plus ###PRINT_LINK###, ###PDF_LINK### and ###CSV_LINK###.
 *
 * The finisher sets a flag in session, so that Formhandler will only call this finisher and nothing else if the user reloads the page.
 *
 *
 * @author	Martin Fünning <fuenning@jakota.de>
 */
class Tx_Reisedb_Finisher_SaveReview extends Tx_Formhandler_AbstractFinisher {

    /**
     * The main method called by the controller
     *
     * @return array The probably modified GET/POST parameters
     */
    public function process() {
        $sql = 'INSERT INTO tx_reisedb_domain_model_review SET

        pid = 13,
        trip = '.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['trip']).',
        title = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['topic']).'",
        description = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['content']).'",
        `name` = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['name']).'",
        email = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['email']).'",
        rating_luggage = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['luggage']).'",
        rating_documents = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['documents']).'",
        rating_care = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['care']).'",
        rating_consultation = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['consultation']).'",
        rating_hotels = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['hotels']).'",
        rating_overall = "'.$GLOBALS['TYPO3_DB']->quoteStr(round(((intval($this->gp['luggage'])+intval($this->gp['documents'])+intval($this->gp['care'])+intval($this->gp['consultation'])+intval($this->gp['hotels'])+intval($this->gp['quality']))/6))).'",
        rating_quality = "'.$GLOBALS['TYPO3_DB']->quoteStr($this->gp['quality']).'",
        tstamp = '.time().',
        crdate = '.time().',
        hidden = 1,
        sys_language_uid = '.$GLOBALS['TSFE']->sys_language_uid.'
        ';

        $GLOBALS['TYPO3_DB']->sql_query($sql);
        $reviewUid=$GLOBALS['TYPO3_DB']->sql_insert_id();

        $files = explode(',', $this->gp['images']);
        $c = 1;
        foreach($files as $file) {

            $fullPath = $_SERVER['DOCUMENT_ROOT'].'/'.$file;

            if (is_file($fullPath)) {

                $fileContent = file_get_contents($fullPath);
                $filename = basename($fullPath);
                $path_parts = pathinfo($fullPath);

                $currentTime = time();

                $mimeType = mime_content_type($fullPath);
                $type = explode('/',$mimeType);
                $type = $type[0];
                switch ($type) {
                    case 'image': $type=2; break;
                    case 'application': $type=5; break;
                    default: $type=0; break;
                }


                $sysFile = array(
                    ':tstamp' => $currentTime,
                    ':last_indexed' => $currentTime,
                    ':storage' => 0,
                    ':type' => $type,
                    ':identifier' => '/'.$file,
                    ':identifier_hash' => sha1('/'.$file),
                    ':folder_hash' => sha1($path_parts['dirname']),
                    ':extension' => $path_parts['extension'],
                    ':mime_type' => $mimeType,
                    ':name' => $filename,
                    ':sha1' => sha1($fileContent),
                    ':size' => filesize ($fullPath),
                    ':creation_date' => filectime($fullPath),
                    ':modification_date' => filemtime($fullPath),
                );

                $sql = 'INSERT INTO sys_file SET
                tstamp = "'.$sysFile[':tstamp'].'",
                last_indexed = "'.$sysFile[':last_indexed'].'",
                metadata = 0,
                `storage` = "'.$sysFile[':filestorage'].'",
                type = "'.$sysFile[':type'].'",
                identifier = "'.$sysFile[':identifier'].'",
                identifier_hash = "'.$sysFile[':identifier_hash'].'",
                folder_hash = "'.$sysFile[':folder_hash'].'",
                extension = "'.$sysFile[':extension'].'",
                mime_type = "'.$sysFile[':mime_type'].'",
                `name` = "'.$sysFile[':filename'].'",
                sha1 = "'.$sysFile[':sha1'].'",
                size = "'.$sysFile[':size'].'",
                creation_date = "'.$sysFile[':creation_date'].'",
                modification_date = "'.$sysFile[':modification_date'].'"';

                $GLOBALS['TYPO3_DB']->sql_query($sql);
                $fileUid=$GLOBALS['TYPO3_DB']->sql_insert_id();

                $sql = 'INSERT INTO sys_file_reference SET
                tstamp = '.$sysFile[':tstamp'].',
                crdate = '.$sysFile[':tstamp'].',
                sorting = '.$c.',
                sys_language_uid = '.$GLOBALS['TSFE']->sys_language_uid.',
                uid_local = '.$fileUid.',
                uid_foreign = '.$reviewUid.',
                tablenames = "tx_reisedb_domain_model_review",
                fieldname = "images",
                sorting_foreign = '.$c.',
                table_local = "sys_file",
                title = "",
                description = ""';


                $GLOBALS['TYPO3_DB']->sql_query($sql);

                $c++;
            }
        }

        return $this->gp;
    }

}