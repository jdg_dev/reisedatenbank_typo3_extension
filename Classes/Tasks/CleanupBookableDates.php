<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 04.11.15
 * Time: 16:10
 */

namespace JAKOTA\Reisedb\Tasks;


class CleanupBookableDates extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    public function execute() {

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_bookabledate SET hidden = 1
            WHERE ((FROM_UNIXTIME(date) < CURDATE()) OR YEAR(FROM_UNIXTIME(date)) > YEAR(DATE_ADD(NOW(), INTERVAL 1 YEAR))) AND hidden = 0
        ");

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_bookabledate SET hidden = 0
            WHERE FROM_UNIXTIME(date) >= CURDATE() AND YEAR(FROM_UNIXTIME(date)) <= YEAR(DATE_ADD(NOW(), INTERVAL 1 YEAR)) AND hidden = 1
        ");

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_bookabledate SET hidden = 1
            WHERE status = 0 AND hidden = 0
        ");

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_eventpackageavailability SET hidden = 1
            WHERE ((FROM_UNIXTIME(dayofavailability) < CURDATE()) OR YEAR(FROM_UNIXTIME(dayofavailability)) > YEAR(DATE_ADD(NOW(), INTERVAL 1 YEAR))) AND hidden = 0
        ");

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_eventpackageavailability SET hidden = 0
            WHERE FROM_UNIXTIME(dayofavailability) >= CURDATE() AND YEAR(FROM_UNIXTIME(dayofavailability)) <= YEAR(DATE_ADD(NOW(), INTERVAL 1 YEAR)) AND hidden = 1
        ");

        $GLOBALS['TYPO3_DB']->sql_query("
            UPDATE tx_reisedb_domain_model_eventpackageavailability SET hidden = 1
            WHERE available = 0 AND hidden = 0
        ");

        return true;
    }
}