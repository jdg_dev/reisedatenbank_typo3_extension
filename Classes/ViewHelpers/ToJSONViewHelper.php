<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 13:52
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class ToJSONViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param mixed $source
     * @return float|int
     */
    public function render($source = NULL) {
        return json_encode($source);
    }
}