<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class EventPackageFromPriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param object $trip The Trip
     * @return string
     */
    public function render($eventPackage) {
        switch ($eventPackage->getTypeofprice()) {
            case 1: // Fixedprice
                $prices = $eventPackage->getFixprice();
                $minPrice = 99999;
                foreach($prices as $price) {
                    if ($minPrice > $price->getPrice() && $price->getPrice() > 0) {
                        $minPrice = $price->getPrice();
                    }
                }
                if ($minPrice < 99999) {
                    return $minPrice;
                } else {
                    return 0;
                }
                break;
            case 2: // Scaledprice
                $prices = $eventPackage->getScaledprice();
                $minPrice = 99999;
                foreach($prices as $price) {
                    if ($minPrice > $price->getPrice() && $price->getPrice() > 0) {
                        $minPrice = $price->getPrice();
                    }
                }
                if ($minPrice < 99999) {
                    return $minPrice;
                } else {
                    return 0;
                }
                break;
            case 3: // Roomprice
                $prices = $eventPackage->getRoomprice();
                $minPrice = 99999;
                foreach($prices as $price) {
                    if ($minPrice > $price->getPrice() && $price->getPrice() > 0) {
                        $minPrice = $price->getPrice();
                    }
                }
                if ($minPrice < 99999) {
                    return $minPrice;
                } else {
                    return 0;
                }
                break;
            default: return 0; break;
        }
    }
}