<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class TourByCategoryViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param int $syscategory The uid of the sys_category
     * @return array
     */
    public function render($syscategory) {
		
				
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows ( 
			'tx_reisedb_domain_model_trip.uid, tx_reisedb_domain_model_trip.title',
			'tx_reisedb_domain_model_trip JOIN sys_category_record_mm ON tx_reisedb_domain_model_trip.uid = sys_category_record_mm.uid_foreign JOIN sys_category ON sys_category.uid = sys_category_record_mm.uid_local',
			'tx_reisedb_domain_model_trip.sys_language_uid = '.$GLOBALS['TSFE']->sys_language_uid.' AND tx_reisedb_domain_model_trip.deleted = 0 AND tx_reisedb_domain_model_trip.hidden = 0 AND tx_reisedb_domain_model_trip.pid = 13 AND sys_category_record_mm.uid_local ='.$syscategory
		);

        return $res;
    }
}