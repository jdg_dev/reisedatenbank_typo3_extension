<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class GroupSeasonPricesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('as', 'string', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $seasonPrices = $this->arguments['trip']->getSeasonPrices();

        $return = false;
        if ($seasonPrices) {
            $return = array();
            foreach($seasonPrices as $seasonPrice) {
                $key = md5($seasonPrice->getFromDate()->format(\DateTime::ISO8601).$seasonPrice->getToDate()->format(\DateTime::ISO8601));
                $return[$key]['from'] = $seasonPrice->getFromDate();
                $return[$key]['to'] = $seasonPrice->getToDate();
                $return[$key]['prices'][] = $seasonPrice;
            }
        }




        $this->templateVariableContainer->add($this->arguments['as'], $return);

        $output = $this->renderChildren();

        $this->templateVariableContainer->remove($this->arguments['as']);

        return $output;
    }
}