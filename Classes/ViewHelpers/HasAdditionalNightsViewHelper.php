<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class HasAdditionalNightsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


    /**
     * prices
     *
     * @var \JAKOTA\Reisedb\Domain\Model\Trip
     * @cascade remove
     */
    protected $trip = NULL;

    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $this->trip = $this->arguments['trip'];

        $stations = $this->trip->getStations();

        /**
         * @var $station \JAKOTA\Reisedb\Domain\Model\Station
         */
        foreach($stations as $station) {
            $additionalNightInfo = $station->getAdditionalNightDescription();
            $additionalNightPrices = $station->getAddtionalNightPrices();
            if ($additionalNightInfo || $additionalNightPrices) {
                return true;
            }
        }

        return false;
    }
}