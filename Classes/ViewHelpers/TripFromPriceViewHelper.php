<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class TripFromPriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param object $trip The Trip
     * @return string
     */
    public function render($trip) {

        $seasonPrices = $trip->getSeasonPrices();
        $sasonPriceEuro = 0;
        $sasonPricePercent = 0;
        foreach($seasonPrices as $seasonPrice) {
            if ($seasonPrice->getValue() < 0) {
                if ($seasonPrice->getType() == 1) {
                    if ($seasonPrice->getValue() < $sasonPricePercent) {
                        $sasonPricePercent = $seasonPrice->getValue();
                    }
                } else {
                    if ($seasonPrice->getValue() < $sasonPriceEuro) {
                        $sasonPriceEuro = $seasonPrice->getValue();
                    }
                }
            }
        }

        $prices = $trip->getPrices();
        $minPrice = 99999;
        foreach($prices as $price) {

            $priceValueEuro = $price->getPrice()+$sasonPriceEuro;
            $priceValuePercent = $price->getPrice()+($price->getPrice()/100*$sasonPricePercent);

            if ($priceValueEuro < $minPrice) {
                $minPrice =  $price->getPrice();
            }
            if ($priceValuePercent < $minPrice) {
                $minPrice =  $price->getPrice();
            }
        }



        return round($minPrice);
    }
}