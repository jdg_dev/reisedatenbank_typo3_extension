<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class TripDaysViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param int $nights The number nights
     * @return string
     */
    public function render($nights) {

        $days = $nights+1;

        if ($days == 1) {
            return $days.' '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('day', 'reisedb');
        } else {
            return $days.' '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('days', 'reisedb');
        }
    }
}