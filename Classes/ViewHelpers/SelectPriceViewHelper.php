<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class SelectPriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('prices', 'mixed', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $prices = $this->arguments['prices'];
        $year = date('Y');
        $lastprice = 100;
        $lastYear = 0;
        foreach($prices as $price) {
            if ($lastYear < $price->getYear()) {
                $lastYear = $price->getYear();
                $lastprice = $price->getPrice();
            }
            if ($year == $price->getYear()) {
                return $price->getPrice();
            }
        }

        return $lastprice;
    }
}