<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 14.04.16
 * Time: 14:16
 */

namespace JAKOTA\Reisedb\ViewHelpers;


use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class EventPackageExtraDataOptionsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    protected $fluidVars = array();

    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument('options', 'string', '', TRUE);
    }

    /**
     * @return string
     */
    public function render()
    {
        $options = $this->arguments['options'];
        $output = '';
        foreach(explode("\n", $options) as $option) {
            $option = trim($option);
            $this->assign('option', $option);
            $output .= $this->renderOutput();
        }

        return $output;
    }

    private function renderOutput() {
        foreach ($this->fluidVars as $name => $value) {
            $this->templateVariableContainer->add($name, $value);
        }
        $output = $this->renderChildren();
        foreach ($this->fluidVars as $name => $value) {
            $this->templateVariableContainer->remove($name);
        }
        return $output;
    }

    private function assign($name, $value) {
        $this->fluidVars[$name] = $value;
    }
}