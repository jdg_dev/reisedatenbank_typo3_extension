<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class PriceTableAllgViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('prices', 'mixed', '', TRUE);
        $this->registerArgument('as', 'string', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $prices = $this->arguments['prices'];
        $rooms = array();
        $categories = array();
        foreach($prices as $price) {
            $room = $price->getRoomcategory();
            $category = $price->getPricecategory();

            $rooms[$room->getUid()] = $room;
            $categories[$category->getUid()] = $category;
        }

        $return =array();
        foreach($rooms as $room) {
            $pricesInt = array();
            foreach($categories as $category) {
                $pricesInt[] = array('category' => $category, 'price' => $this->getPriceByRoomCategory($room, $category, $prices));
            }

            $return[] = array('room' => $room, 'prices' => $pricesInt);

        }



        $this->templateVariableContainer->add($this->arguments['as'], $return);

        $output = $this->renderChildren();

        $this->templateVariableContainer->remove($this->arguments['as']);

        return $output;
    }

    public function getPriceByRoomCategory($room, $category, $prices) {
        foreach($prices as $price) {
            if($price->getRoomcategory()->getUid() == $room->getUid() && $price->getPricecategory()->getUid() == $category->getUid()) {
                return $price;
            }
        }
        return null;
    }
}