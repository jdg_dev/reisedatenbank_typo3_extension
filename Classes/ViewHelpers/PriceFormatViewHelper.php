<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class PriceFormatViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param null $price bla
     * @param string $currency bla
     * @param bool|true $round bla
     * @return string
     */
    public function render($price = NULL, $currency = '&euro;', $round = true) {

        if ($price === NULL) {
            $price = $this->renderChildren();
        }

        $price = floatval($price);

        if ($GLOBALS['TSFE']->sys_language_uid == 1) {
            $thousandSeperator = ',';
            $digitSeperator = '.';
        } else {
            $thousandSeperator = '.';
            $digitSeperator = ',';
        }

        return number_format(
            $price,
            $round ? 0 : 2,
            $GLOBALS['TSFE']->sys_language_uid == 1 ? '.' : ',',
            $GLOBALS['TSFE']->sys_language_uid == 1 ? ',' : '.'
        ).' '.$currency;


    }
}