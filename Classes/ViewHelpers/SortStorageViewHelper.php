<?php
/**
 * Created by PhpStorm.
 * User: sektlaune
 * Date: 08.11.15
 * Time: 11:17
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class SortStorageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('subject', '\TYPO3\CMS\Extbase\Persistence\ObjectStorage', '', TRUE);
        $this->registerArgument('as', 'string', '', TRUE);
        $this->registerArgument('sortBy', 'string', '', TRUE);
        $this->registerArgument('order', 'string', '', TRUE);
        $this->registerArgument('sortFlags', 'string', '', FALSE, 'SORT_REGULAR');
    }

    /**
     * @param $test string huhu
     * @return string
     */
    public function render() {

        $sortedStorage = $this->sortObjectStorage($this->arguments['subject']);

        $this->templateVariableContainer->add($this->arguments['as'], $sortedStorage);

        $output = $this->renderChildren();

        $this->templateVariableContainer->remove($this->arguments['as']);

        return $output;
    }

    /**
     * Sort a Tx_Extbase_Persistence_ObjectStorage instance
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $storage
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    protected function sortObjectStorage($storage)
    {
        /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
        /** @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage $temp */
        $temp = $objectManager->get('\TYPO3\CMS\Extbase\Persistence\ObjectStorage');
        foreach ($storage as $item) {
            $temp->attach($item);
        }
        $sorted = array();
        foreach ($storage as $index => $item) {
            if ($this->arguments['sortBy']) {
                $index = $this->getSortValue($item);
            }
            while (isset($sorted[$index])) {
                $index .= '1';
            }
            $sorted[$index] = $item;
        }
        if ($this->arguments['order'] === 'ASC') {
            ksort($sorted, constant($this->arguments['sortFlags']));
        } else {
            krsort($sorted, constant($this->arguments['sortFlags']));
        }
        $storage = $objectManager->get('\TYPO3\CMS\Extbase\Persistence\ObjectStorage');
        foreach ($sorted as $item) {
            $storage->attach($item);
        }
        return $storage;
    }

    /**
     * Gets the value to use as sorting value from $object
     *
     * @param mixed $object
     * @return mixed
     */
    protected function getSortValue($object) {
        $field = $this->arguments['sortBy'];
        $value = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getProperty($object, $field);
        if ($value instanceof DateTime) {
            $value = $value->format('U');
        } elseif ($value instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage) {
            $value = $value->count();
        } elseif (is_array($value)) {
            $value = count($value);
        }
        return $value;
    }
}