<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class LengthFormatViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param null $price bla
     * @param string $currency bla
     * @param bool|true $round bla
     * @return string
     */
    public function render($length = NULL) {

        if ($length === NULL) {
            $length = $this->renderChildren();
        }

        $length = intval($length);

        if ($GLOBALS['TSFE']->sys_language_uid == 1) {
            return round($length).' km ('.round($length*0.621371).' m)';
        } else {
            return round($length).' km';
        }


    }
}