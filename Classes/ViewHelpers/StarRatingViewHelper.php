<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 12:15
 */
namespace JAKOTA\Reisedb\ViewHelpers;


class StarRatingViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * @param mixed $stars
     * @return string
     */
    public function render($stars = NULL) {
        if ($stars === NULL) {
            $stars = $this->renderChildren();
        }
        $stars = intval($stars);
        $output = '';
        if ($stars >= 1 && $stars <= 5) {
            $output .= '<div class="rating" style="margin-bottom: 10px; color: #005077;">';
            for ($i = 0; $i < $stars; $i++) {
                $output .= '<span class="glyphicon glyphicon-star"></span>';
            }
            for ($i = 5; $i > $stars; $i--) {
                $output .= '<span class="glyphicon glyphicon-star-empty"></span>';
            }
            $output .= '</div>';
        }
        return $output;
    }
}
