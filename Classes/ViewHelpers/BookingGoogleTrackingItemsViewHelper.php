<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class BookingGoogleTrackingItemsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
    }

    /**
     * @return string
     */
    public function render()
    {

        $trip = $this->arguments['trip'];
        $gp = $this->arguments['gp'];

        /*
         * _gaq.push(['_addItem',
      '1234',
      '###value_code###',
      ' Sterntouren Waren/ Seenplatte',
      'Green Medium',
      '200',
      '2'
    ]);
         */

        $sumPrice = 0;
        $priceComplete = true;

        $return = array();

        foreach($gp['rooms'] as $room) {

            foreach($room['beds'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $priceComplete = false;
                } else {
                    $return[] = "_gaq.push(['_addItem',
      '1234',
      '".$trip->getCode()."',
      '".$room['name']."',
      'None',
      '".$priceValue."',
      '1'
    ]);";
                }

                if ($bed['rentalbike'] == 'yes') {
                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);

                    if (!$rentalpriceValue) {
                        $priceComplete = false;
                    } else {
                        $sumPrice += $rentalpriceValue;
                        $return[] = "_gaq.push(['_addItem',
      '1234',
      '".$trip->getCode()."',
      '".$trip->getRentalBikeName($bed['rentalbiketype'])."',
      'None',
      '".$rentalpriceValue."',
      '1'
    ]);";
                    }

                }
            }

            foreach($room['additionalBed'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $priceComplete = false;
                } else {
                    $sumPrice += $priceValue;
                    $return[] = "_gaq.push(['_addItem',
      '1234',
      '".$trip->getCode()."',
      '".$room['name']." (Aufbettung)',
      'None',
      '".$priceValue."',
      '1'
    ]);";
                }

                if ($bed['rentalbike'] == 'yes') {
                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);
                    if (!$rentalpriceValue) {
                        $priceComplete = false;
                    } else {
                        $return[] = "_gaq.push(['_addItem',
      '1234',
      '".$trip->getCode()."',
      '".$trip->getRentalBikeName($bed['rentalbiketype'])."',
      'None',
      '".$rentalpriceValue."',
      '1'
    ]);";
                    }
                }
            }
        }

        return implode("\n", $return);
    }
}