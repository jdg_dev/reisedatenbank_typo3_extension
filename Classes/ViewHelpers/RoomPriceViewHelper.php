<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class RoomPriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
        $this->registerArgument('person', 'int', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $price = $this->arguments['trip']->calculatePersonsPriceFinal(
            $this->arguments['gp'],
            $this->arguments['person']
        );


        return $price;
    }
}