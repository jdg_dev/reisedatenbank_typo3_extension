<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 14.04.16
 * Time: 14:16
 */

namespace JAKOTA\Reisedb\ViewHelpers;


use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class EventPackageShouldRenderedViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    protected $fluidVars = array();

    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument('eventPackage', 'JAKOTA\Reisedb\Domain\Model\EventPackage', '', TRUE);
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
    }

    /**
     * @return string
     */
    public function render()
    {
        /**
         * @var $eventPackage \JAKOTA\Reisedb\Domain\Model\EventPackage
         */
        $eventPackage = $this->arguments['eventPackage'];

        /**
         * @var $trip \JAKOTA\Reisedb\Domain\Model\Trip
         */
        $trip = $this->arguments['trip'];
        $gp = $this->arguments['gp'];

        $city = $eventPackage->getCity();
        $availablity = $eventPackage->getAvailability();

        $leagalDays = array();
        $i = 0;



        foreach($trip->getStations() as $station) {
            /**
             * @var $station \JAKOTA\Reisedb\Domain\Model\Station
             */
            $tripDay = new \DateTime($gp['arrival']);
            $tripDay->add(new \DateInterval('P'.$i.'D'));
            if ($city == '' || $city == $station->getCity()) {
                foreach($availablity as $dayOfAvailablity) {
                    /**
                     * @var $dayOfAvailablity \JAKOTA\Reisedb\Domain\Model\EventPackageAvailability
                     */
                    if($dayOfAvailablity->getDayofavailability()->format('dmY') == $tripDay->format('dmY')) {
                        $dayAlreadyPresent = false;
                        foreach($leagalDays as $legalDay) {
                            if ($legalDay['day']->format('dmY') == $tripDay->format('dmY')) $dayAlreadyPresent = true;
                        }
                        if (!$dayAlreadyPresent) {
                            array_push($leagalDays, array(
                                'day' => $tripDay,
                                'group' => $dayOfAvailablity->getAvailable(),
                            ));
                        }
                    }

                }
            }
            $i++;
        }

        if (count($leagalDays) > 0) {
            $this->assign('defaultDate', $leagalDays[0]);
            $this->assign('renderDateSelector', count($leagalDays) > 1);
            $this->assign('days', $leagalDays);
            $this->assign('daysJson', json_encode($leagalDays));
            $output = $this->renderOutput();
        }

        return $output;
    }

    private function renderOutput() {
        foreach ($this->fluidVars as $name => $value) {
            $this->templateVariableContainer->add($name, $value);
        }
        $output = $this->renderChildren();
        foreach ($this->fluidVars as $name => $value) {
            $this->templateVariableContainer->remove($name);
        }
        return $output;
    }

    private function assign($name, $value) {
        $this->fluidVars[$name] = $value;
    }
}