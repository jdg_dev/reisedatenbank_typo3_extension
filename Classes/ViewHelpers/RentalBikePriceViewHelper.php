<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class RentalBikePriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('id', 'int', '', TRUE);
        $this->registerArgument('date', 'string', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $aditionalServices = $this->arguments['trip']->getAdditionalServices();

        foreach($aditionalServices as $aditionalService) {
            if ($aditionalService->getIsRentalBike()) {
                $name = $aditionalService->getName();
                $uid = $aditionalService->getUid();

                if ($uid == $this->arguments['id']) {
                    $prices = $aditionalService->getPrice();
                    $lastYear = 0;
                    $lastPrice = null;
                    $year = date('Y', strtotime($this->arguments['date']));
                    foreach($prices as $price) {
                        if ($price->getYear() > $lastYear) $lastPrice = $price;
                        if ($price->getYear() == $year) return $price->getPrice();
                    }
                    if ($lastPrice) {
                        return $lastPrice->getPrice();
                    } else {
                        return false;
                    }
                }
            }
        }


    }
}