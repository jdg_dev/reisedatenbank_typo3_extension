<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class BookingPdfViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
    }

    public function getDiscountRules($personUid) {
        $roomDiscount = $this->arguments['trip']->roomDiscount(
            $this->arguments['gp'],
            $personUid
        );

        $earlyBirdDiscount = $this->arguments['trip']->earlyBirdDiscount(
            $this->arguments['gp'],
            $personUid
        );

        $seasonPrice = $this->arguments['trip']->seasonPrice(
            $this->arguments['gp'],
            $personUid
        );

        $groupDiscount = $this->arguments['trip']->groupDiscount(
            $this->arguments['gp'],
            $personUid
        );

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $childDiscount = 'Kinderermäßigung';
            $ebDiscount = 'Frühbucherrabatt';
            $seasonAdd = 'Saisonzuschlag';
            $seasonDiscount = 'Saisonrabatt';
            $gpDiscount = 'Gruppenrabatt';
        } else {
            $childDiscount = 'Child Discount';
            $ebDiscount = 'Early Bird Discount';
            $seasonAdd = 'Season Surcharge';
            $seasonDiscount = 'Season Discount';
            $gpDiscount = 'Group Discount';
        }

        $return = array();

        if ($seasonPrice) {
            switch ($seasonPrice['type']) {
                case 'percent':
                    if ($seasonPrice['operator'] == '+') {
                        $return[] = $seasonAdd.' +'.round($seasonPrice['value']).'%';
                    } else {
                        $return[] = $seasonDiscount.' -'.round($seasonPrice['value']).'%';
                    }

                    break;
                case 'total':
                    if ($seasonPrice['operator'] == '+') {
                        $return[] = $seasonAdd.' +'.number_format($seasonPrice['value'], 2, ',', '.').' €';
                    } else {
                        $return[] = $seasonDiscount.' -'.number_format($seasonPrice['value'], 2, ',', '.').' €';
                    }
                    break;
            }
        }

        if ($roomDiscount) {
            switch ($roomDiscount['type']) {
                case 'percent':  $return[] = $childDiscount.' -'.round($roomDiscount['value']).'%'; break;
                case 'total':  $return[] = $childDiscount.' -'.number_format($roomDiscount['value'], 2, ',', '.').' €'; break;
            }
        }

        if ($groupDiscount && !$roomDiscount) {
            switch ($groupDiscount['type']) {
                case 'percent':  $return[] = $gpDiscount.' -'.round($groupDiscount['value']).'%'; break;
                case 'total':  $return[] = $gpDiscount.' -'.number_format($groupDiscount['value'], 2, ',', '.').' €'; break;
            }
        }

        if ($earlyBirdDiscount && !$roomDiscount) {
            switch ($earlyBirdDiscount['type']) {
                case 'percent':  $return[] = $ebDiscount.' -'.round($earlyBirdDiscount['value']).'%'; break;
                case 'total':  $return[] = $ebDiscount.' -'.number_format($earlyBirdDiscount['value'], 2, ',', '.').' €'; break;
            }
        }


        if (count($return) > 0) {
            return implode('<br />', $return);
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    public function render() {

        $trip = $this->arguments['trip'];
        $gp = $this->arguments['gp'];

        $filename = '/uploads/bookingpdf/'.md5(serialize($gp)).'.pdf';

        $pdf = new \JAKOTA\Reisedb\Utils\TemplateTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Die Mecklenburger Radtour GmbH');
        $pdf->SetTitle('Reiseanmeldung');
        $pdf->SetSubject('Reiseanmeldung');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');



// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);


// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------

// set default font subsetting mode
        $pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
        $pdf->AddPage();
        $pdf->ImageSVG($file=$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/reisedb/Resources/Public/PDFAssets/logo.svg', $x=20, $y=7, $w='68', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false);
        $pdf->SetFont('helvetica', 'B', 20);
        // Title
        $pdf->Text(130.7, 11, $GLOBALS['TSFE']->sys_language_uid==0?'Reiseanmeldung':'Travel Registration');


        $pdf->SetFont('helvetica', '', 10);
        $pdf->WriteHTMLCell(0,0,25,46.26,"<strong>Die Mecklenburger Radtour GmbH</strong><br>Zunftstraße 4<br>18437 Stralsund<br>GERMANY");

        if ($GLOBALS['TSFE']->loginUser) {
            $pdf->WriteHTMLCell(0,0,130.7,30.71,'<strong>Vermittelt durch:</strong><br><br>'.$GLOBALS['TSFE']->fe_user->user['company'].'<br>'.$GLOBALS['TSFE']->fe_user->user['address'].'<br>'.$GLOBALS['TSFE']->fe_user->user['zip'].' '.$GLOBALS['TSFE']->fe_user->user['city'].'<br><br>Tel.: '.$GLOBALS['TSFE']->fe_user->user['telephone'].'<br>Fax: '.$GLOBALS['TSFE']->fe_user->user['fax'].'<br>'.$GLOBALS['TSFE']->fe_user->user['email'].'');
        } else {
            $pdf->WriteHTMLCell(0,0,130.7,30.71,"<strong>Fax: +49 (0) 3831 / 30676-19</strong><br><br>Die Mecklenburger Radtour GmbH<br>Zunftstr. 4<br>18437 Stralsund<br>GERMANY<br><br>Tel.: +49 (0) 3831 / 30676-0<br>Fax: +49 (0) 3831 / 30676-19<br>info@mecklenburger-radtour.de<br>www.mecklenburger-radtour.de");
        }

        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(50, 50, 50));
        $pdf->Line(0, 87, 15, 87, $style);
        $pdf->Line(195, 87, 210, 87, $style);
        $pdf->Line(0, 192, 15, 192, $style);
        $pdf->Line(195, 192, 210, 192, $style);

        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(100, 100, 100));
        $pdf->Line(0, 105, 10, 105, $style);
        $pdf->Line(200, 105, 210, 105, $style);
        $pdf->Line(0, 210, 10, 210, $style);
        $pdf->Line(200, 210, 210, 210, $style);

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html = '<p>Hiermit melde ich mich und die aufgeführten Personen<br>zu folgender Mecklenburger Radtour-Reise an:</p>';
        } else {
            $html = '<p>I would like to book the following tour:</p>';
        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h4>Reisedaten</h4>';
        } else {
            $html .= '<h4>Tour Information</h4>';
        }

        $prices = $this->arguments['trip']->getPrices();
        $rooms = array();
        $categories = array();
        foreach($prices as $price) {
            $room = $price->getRoomcategory();
            $category = $price->getPricecategory();

            $rooms[$room->getUid()] = $room;
            $categories[$category->getUid()] = $category;
        }
        $priceCategoryName = '';
        foreach($categories as $category) {
            if ($category->getUid() == $gp['pricecategory']) $priceCategoryName = $category->getTitle();
        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<p>
            Tour-Nr.: <strong>'.$trip->getCode().'</strong><br>
            Tour: <strong>'.$trip->getTitle().'</strong><br>
            Anreise: <strong>'.$gp['arrival'].'</strong><br>
            Preiskategorie: <strong>'.$priceCategoryName.'</strong><br>
            </p>';
        } else {
            $html .= '<p>
            Tour Code: <strong>'.$trip->getCode().'</strong><br>
            Tour Name: <strong>'.$trip->getTitle().'</strong><br>
            Arrival Date: <strong>'.$gp['arrival'].'</strong><br>
            Price Category: <strong>'.$priceCategoryName.'</strong><br>
            </p>';
        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h4>Zimmer & Personen (konkrete Aufschlüsselung auf den Folgeseiten)</h4>';
        } else {
            $html .= '<h4>Rooms & Participants (Details on next page)</h4>';
        }

        $roomTypes = array();
        foreach($gp['rooms'] as $room) {
            if (array_key_exists($room['uid'], $roomTypes)) {
                $roomTypes[$room['uid']]['count'] = $roomTypes[$room['uid']]['count']+1;
            } else {
                $roomTypes[$room['uid']] = array(
                    'count' => 1,
                    'countAdditionalBed' => 0,
                    'name' => $room['name'],
                    'additionalBedStatus' => $room['additionalBedStatus'],
                );
            }

            if ($room['additionalBedStatus'] && count($room['additionalBed']) > 0) {
                $roomTypes[$room['uid']]['countAdditionalBed'] = $roomTypes[$room['uid']]['countAdditionalBed']+1;
            }
        }

        $text = array();
        foreach($roomTypes as $roomtype) {

            $string = $roomtype['count'].' '.$roomtype['name'];
            if ($roomtype['additionalBedStatus']) {
                $string .= ' ('.$roomtype['countAdditionalBed'].' '.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'mit Aufbettung':'with additional bed').')';
            }
            $text[] = $string;
        }
        $text = implode(', ', $text);


        $html .= '<p>'.$text.'</p>';

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h4>Adresse des Anmelders</h4>';
        } else {
            $html .= '<h4>Details of Party Leader to whom all correspondence will be sent</h4>';
        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
            <td>'.$gp['firstname'].' '.$gp['lastname'].'<br>'.$gp['address'].'<br>'.$gp['postcode'].' '.$gp['city'].'<br>'.$gp['country'].'</td>
            <td>Telefon: '.$gp['phone'].'<br>Fax: '.$gp['fax'].'<br>E-Mail: '.$gp['email'].'</td>
            </tr></table>';
        } else {
            $html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
            <td>'.$gp['firstname'].' '.$gp['lastname'].'<br>'.$gp['address'].'<br>'.$gp['postcode'].' '.$gp['city'].'<br>'.$gp['country'].'</td>
            <td>Phone: '.$gp['phone'].'<br>Fax: '.$gp['fax'].'<br>Email: '.$gp['email'].'</td>
            </tr></table>';
        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h4>Bemerkungen und Wünsche</h4>';
        } else {
            $html .= '<h4>Comments / Preferences</h4>';
        }

        $html .= '<p>'.$gp['message'].'</p>';

        $pdf->WriteHTMLCell(170,0,20,100,$html);

        $pdf->AddPage();
        $html = '';
        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h3>Personen</h3>';
        } else {
            $html .= '<h3>Persons</h3>';
        }
        $sumPrice = 0;
        $priceComplete = true;
        foreach($gp['rooms'] as $room) {

            $html .= '<h4>'.$room['name'].'</h4>';
            $html .= '<table border="1" width="100%" cellspacing="0" cellpadding="2"><tr>
                <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Name':'Name').'</th>
                <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Anschrift':'Address').'</th>
                <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Geburtsdatum':'Birthday').'</th>
                <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Leihrad':'Rental Bike').'</th>
                <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Preis p.P.':'Price p.P').'</th>
            </tr>';

            foreach($room['beds'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $priceComplete = false;
                    $price = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Zimmer':'Room').': <strong>n/a</strong>';
                } else {
                    $sumPrice += $priceValue;
                    $price = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Zimmer':'Room').': '.$this->getDiscountRules($bed['uid']).'<br /><strong>'.number_format($priceValue, 2, ',', '.').' €</strong>';
                }

                $leihrad = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Nein':'No');

                if ($bed['rentalbike'] == 'yes') {

                    $leihrad = $trip->getRentalBikeName($bed['rentalbiketype'])."<br>";
                    if ($bed['rentalbikegear'] == '7gang') $leihrad .= '8 '.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Gang':'gears')."<br>";
                    if ($bed['rentalbikegear'] == '24gang') $leihrad .= '24 '.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Gang':'gears')."<br>";
                    if ($bed['rentalbikesex'] == 'Damenrad') $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Damenrad':"Wommen's Bike")."<br>";
                    if ($bed['rentalbikesex'] == 'Herrenrad') $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Herrenrad':"Men's Bike")."<br>";
                    $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Größe: ':"Height: ").$bed['rentalbikeheight'].'cm';

                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);

                    if (!$rentalpriceValue) {
                        $priceComplete = false;
                        $price .= '<br>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Leihrad':'Rental Bike').': <strong>n/a</strong>';
                    } else {
                        $sumPrice += $rentalpriceValue;
                        $price .= '<br>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Leihrad':'Rental Bike').': <strong>'.number_format($rentalpriceValue, 2, ',', '.').'</strong>';
                    }

                }

                $html .= '<tr>
                <td>'.$bed['firstname'].' '.$bed['lastname'].'</td>
                <td>'.$bed['address'].'<br>'.$bed['postcode'].' '.$bed['city'].'<br>'.$bed['country'].'</td>
                <td>'.($bed['birthday']?$bed['birthday']:'').'</td>
                <td>'.$leihrad.'</td>
                <td align="right">'.$price.'</td>
            </tr>';
            }

            foreach($room['additionalBed'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $price = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Zimmer':'Room').': <strong>n/a</strong>';
                } else {
                    $sumPrice += $priceValue;
                    $price = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Zimmer':'Room').': '.$this->getDiscountRules($bed['uid']).'<br /><strong>'.number_format($priceValue, 2, ',', '.').' €</strong>';
                }

                $leihrad = ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Nein':'No');

                if ($bed['rentalbike'] == 'yes') {

                    $leihrad = $trip->getRentalBikeName($bed['rentalbiketype'])."<br>";
                    if ($bed['rentalbikegear'] == '7gang') $leihrad .= '8 '.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Gang':'gears')."<br>";
                    if ($bed['rentalbikegear'] == '24gang') $leihrad .= '24 '.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Gang':'gears')."<br>";
                    if ($bed['rentalbikesex'] == 'Damenrad') $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Damenrad':"Wommen's Bike")."<br>";
                    if ($bed['rentalbikesex'] == 'Herrenrad') $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Herrenrad':"Men's Bike")."<br>";
                    $leihrad .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Größe: ':"Height: ").$bed['rentalbikeheight'].'cm';

                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);

                    if (!$rentalpriceValue) {
                        $price .= '<br>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Leihrad':'Rental Bike').': <strong>n/a</strong>';
                    } else {
                        $sumPrice += $rentalpriceValue;
                        $price .= '<br>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Leihrad':'Rental Bike').': <strong>'.number_format($rentalpriceValue, 2, ',', '.').'</strong>';
                    }

                }

                $html .= '<tr>
                <td>'.$bed['firstname'].' '.$bed['lastname'].'<br><i>('.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Aufbettung':'Additional bed').')</i></td>
                <td>'.$bed['address'].'<br>'.$bed['postcode'].' '.$bed['city'].'<br>'.$bed['country'].'</td>
                <td>'.($bed['birthday']?$bed['birthday']:'').'</td>
                <td>'.$leihrad.'</td>
                <td align="right">'.$price.'</td>
            </tr>';
            }
            $html .= '</table>';

        }

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<h3>Erlebnispakete</h3>';
        } else {
            $html .= '<h3>Event Packages</h3>';
        }
        foreach($gp['eventPackages'] as $eventPackage) {

            $html .= '<h4>' . $eventPackage['title'] . '</h4>';
            if ($eventPackage['city'] != '') {
                $html .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Stadt':'City').': <strong>'.$eventPackage['city'].'</strong><br />';
            }
            $html .= ($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Datum':'Date').': <strong>'.$eventPackage['date'].'</strong><br />';
            foreach($eventPackage['extraData'] as $extraData) {
                $html .= $extraData['extraData']->getTitle().': <strong>'.$extraData['value'].'</strong><br />';
            }
            if ($eventPackage['typeofprice'] != 3) {
                $html .= '<table border="1" width="100%" cellspacing="0" cellpadding="2"><tr>
                    <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Name':'Name').'</th>
                    <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Preis':'Price').'</th>
                </tr>';
                foreach($eventPackage['participants'] as $person) {
                    $sumPrice += $person['price'];
                    $price = '<strong>'.number_format($person['price'], 2, ',', '.').' €</strong>';
                    $html .= '<tr>
                        <td>'.$person['person']['firstname'].' '.$person['person']['lastname'].'</td>
                        <td align="right">'.$price.'</td>
                    </tr>';
                }
                $html .= '</table>';
            } else {
                $html .= '<table border="1" width="100%" cellspacing="0" cellpadding="2"><tr>
                    <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Zimmer':'Room').'</th>
                    <th>'.($GLOBALS['TSFE']->sys_language_uid == 0 ? 'Preis':'Price').'</th>
                </tr>';
                foreach($eventPackage['rooms'] as $room) {
                    if ($room['count'] > 0) {
                        $sumPrice += $room['pricetotal'];
                        $pricetotal = '<strong>'.number_format($room['pricetotal'], 2, ',', '.').' €</strong>';
                        $price = number_format($room['price'], 2, ',', '.').' €';
                        $html .= '<tr>
                            <td>'.$room['room']->getTitle().' (a '.$price.')</td>
                            <td align="right">'.$pricetotal.'</td>
                        </tr>';
                    }
                }
                $html .= '</table>';
            }
        }

        $priceWarning = '';
        if (!$priceComplete) {
            if ($GLOBALS['TSFE']->sys_language_uid == 0) {
                $priceWarning = ' Es konnten nicht alle Preise angegeben werden. Den kompletten Preis erfahren Sie von uns nach der Anmeldung.';
            } else {
                $priceWarning = ' It could not be all prices stated. The complete pricing information from us upon registration.';
            }
        }

        $html .= '<span align="right">Summe*: '.number_format($sumPrice, 2, ',', '.').' €</span>';
        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<p><small>* Vorläufiger Preis der Katalogpreis ist ausschlaggebend.'.$priceWarning.'</small></p>';
        } else {
            $html .= '<p><small>* Prices on the website are tentative. Prices printed in our catalogue are the determining prices.'.$priceWarning.'</small></p>';
        }


        $html .= '<p></p>';
        $html .= '<p></p>';
        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<p>Mit Erhalt unserer Reisebestätigung senden wir Ihnen Informationsmaterial zum Abschluss einer Reiseversicherung zu. Bitte beachten Sie die Abschlussfristen. Hiermit melde ich die oben aufgeführten Gäste zu einer Reise mit der Mecklenburger Radtour verbindlich an. Mir liegen die Reisebedingungen vor. Ich erkenne diese zugleich im Auftrag der anderen aufgeführten Personen an.</p>';
        } else {
            $html .= '<p>Upon receipt of our booking confirmation we will send you further information to take out travel insurance. Please note the final deadlines. I hereby guests listed above to be binding for a trip with the Mecklenburg cycle tour. I have received the travel conditions. I recognize this at the same time, on behalf of the other persons listed.</p>';
        }

        $html .= '<p></p>';
        $html .= '<p></p>';
        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $html .= '<small>Datum/Unterschift<br/></small><hr>';
        } else {
            $html .= '<small>Date, signature<br/></small><hr>';
        }

        $pdf->SetFont('helvetica', '', 8);
        $pdf->WriteHTMLCell(170,0,20,20,$html);

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $this->files = array(
                #$_SERVER['DOCUMENT_ROOT'] . '/typo3conf/ext/reisedb/Resources/Public/PDFAssets/erlebnispakete_0.pdf',
                $_SERVER['DOCUMENT_ROOT'] . '/typo3conf/ext/reisedb/Resources/Public/PDFAssets/anmeldung_reisebedinungen_0.pdf'
            );
        } else {
            $this->files = array(
                #$_SERVER['DOCUMENT_ROOT'] . '/typo3conf/ext/reisedb/Resources/Public/PDFAssets/erlebnispakete_1.pdf',
                $_SERVER['DOCUMENT_ROOT'] . '/typo3conf/ext/reisedb/Resources/Public/PDFAssets/anmeldung_reisebedinungen_1.pdf'
            );
        }

        foreach($this->files AS $file) {
            $pagecount = $pdf->setSourceFile($file);
            for ($i = 1; $i <= $pagecount; $i++) {
                $tplidx = $pdf->ImportPage($i);
                $pdf->AddPage();
                $pdf->useTemplate($tplidx);
            }
        }

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.

        try {
            $pdf->Output($_SERVER['DOCUMENT_ROOT'].$filename, 'F');
            return $filename;
        } catch (\Exception $e) {
            return 'none';
        }



    }
}