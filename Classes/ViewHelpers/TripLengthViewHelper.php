<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 05.11.15
 * Time: 15:12
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class TripLengthViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param array $stations The Stations of the Trip
     * @return string
     */
    public function render($stations) {

        $lengthCount = 0;
        foreach($stations as $station) {
            $lengthCount += $station->getTracklength();
        }

        return $lengthCount;
    }
}