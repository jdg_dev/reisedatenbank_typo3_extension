<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class BookingPriceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
    }

    /**
     * @return string
     */
    public function render()
    {

        $trip = $this->arguments['trip'];
        $gp = $this->arguments['gp'];

        $sumPrice = 0;
        $priceComplete = true;
        foreach($gp['rooms'] as $room) {

            foreach($room['beds'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $priceComplete = false;
                } else {
                    $sumPrice += $priceValue;
                }

                if ($bed['rentalbike'] == 'yes') {
                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);

                    if (!$rentalpriceValue) {
                        $priceComplete = false;
                    } else {
                        $sumPrice += $rentalpriceValue;
                    }

                }
            }

            foreach($room['additionalBed'] as $bed) {

                $priceValue = $trip->calculatePersonsPriceFinal($gp, $bed['uid']);
                if (!$priceValue) {
                    $priceComplete = false;
                } else {
                    $sumPrice += $priceValue;
                }

                if ($bed['rentalbike'] == 'yes') {
                    $rentalpriceValue = $trip->getRentalBikePrice($bed['rentalbiketype'], $gp['arrival']);
                    if (!$rentalpriceValue) {
                        $priceComplete = false;
                    } else {
                        $sumPrice += $rentalpriceValue;
                    }
                }
            }
        }

        return $sumPrice;
    }
}