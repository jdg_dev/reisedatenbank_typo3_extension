<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 13:52
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class RatingViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param mixed $reviews
     * @return float|int
     */
    public function render($reviews = NULL) {

        $sum = 0;
        $c = 0;
        foreach($reviews as $review) {
            if ($review->getRatingOverall() > 0) {
                $c++;
                $sum += $review->getRatingOverall();
            }
        }

        if ($c > 0) {
            return round($sum/$c);
        } else {
            return 0;
        }

    }
}