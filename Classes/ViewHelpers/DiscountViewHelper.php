<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class DiscountViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('gp', 'array', '', TRUE);
        $this->registerArgument('person', 'int', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $roomDiscount = $this->arguments['trip']->roomDiscount(
            $this->arguments['gp'],
            $this->arguments['person']
        );

        $earlyBirdDiscount = $this->arguments['trip']->earlyBirdDiscount(
            $this->arguments['gp'],
            $this->arguments['person']
        );

        $seasonPrice = $this->arguments['trip']->seasonPrice(
            $this->arguments['gp'],
            $this->arguments['person']
        );

        $groupDiscount = $this->arguments['trip']->groupDiscount(
            $this->arguments['gp'],
            $this->arguments['person']
        );

        if ($GLOBALS['TSFE']->sys_language_uid == 0) {
            $childDiscount = 'Kinderermäßigung';
            $ebDiscount = 'Frühbucherrabatt';
            $seasonAdd = 'Saisonzuschlag';
            $seasonDiscount = 'Saisonrabatt';
            $gpDiscount = 'Gruppenrabatt';
        } else {
            $childDiscount = 'Child Discount';
            $ebDiscount = 'Early Bird Discount';
            $seasonAdd = 'Season Surcharge';
            $seasonDiscount = 'Season Discount';
            $gpDiscount = 'Group Discount';
        }

        $return = array();

        if ($seasonPrice) {
            switch ($seasonPrice['type']) {
                case 'percent':
                    if ($seasonPrice['operator'] == '+') {
                        $return[] = $seasonAdd.' +'.round($seasonPrice['value']).'%';
                    } else {
                        $return[] = $seasonDiscount.' -'.round($seasonPrice['value']).'%';
                    }

                    break;
                case 'total':
                    if ($seasonPrice['operator'] == '+') {
                        $return[] = $seasonAdd.' +'.number_format($seasonPrice['value'], 2, ',', '.').' €';
                    } else {
                        $return[] = $seasonDiscount.' -'.number_format($seasonPrice['value'], 2, ',', '.').' €';
                    }
                    break;
            }
        }

        if ($roomDiscount) {
            switch ($roomDiscount['type']) {
                case 'percent':  $return[] = $childDiscount.' -'.round($roomDiscount['value']).'%'; break;
                case 'total':  $return[] = $childDiscount.' -'.number_format($roomDiscount['value'], 2, ',', '.').' €'; break;
            }
        }

        if ($groupDiscount && !$roomDiscount) {
            switch ($groupDiscount['type']) {
                case 'percent':  $return[] = $gpDiscount.' -'.round($groupDiscount['value']).'%'; break;
                case 'total':  $return[] = $gpDiscount.' -'.number_format($groupDiscount['value'], 2, ',', '.').' €'; break;
            }
        }

        if ($earlyBirdDiscount && !$roomDiscount) {
            switch ($earlyBirdDiscount['type']) {
                case 'percent':  $return[] = $ebDiscount.' -'.round($earlyBirdDiscount['value']).'%'; break;
                case 'total':  $return[] = $ebDiscount.' -'.number_format($earlyBirdDiscount['value'], 2, ',', '.').' €'; break;
            }
        }


        if (count($return) > 0) {
            return implode('<br />', $return);
        } else {
            return '';
        }
    }
}