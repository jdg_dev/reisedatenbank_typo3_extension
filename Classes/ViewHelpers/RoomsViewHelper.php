<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class RoomsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    public function initializeArguments() {
        $this->registerArgument('trip', 'JAKOTA\Reisedb\Domain\Model\Trip', '', TRUE);
        $this->registerArgument('as', 'string', '', TRUE);
    }

    /**
     * @return string
     */
    public function render() {

        $prices = $this->arguments['trip']->getPrices();
        $rooms = array();
        $categories = array();
        foreach($prices as $price) {
            $room = $price->getRoomcategory();
            $category = $price->getPricecategory();

            $rooms[$room->getUid()] = $room;
            $categories[$category->getUid()] = $category;
        }

        $this->templateVariableContainer->add($this->arguments['as'], $rooms);

        $output = $this->renderChildren();

        $this->templateVariableContainer->remove($this->arguments['as']);

        return $output;
    }
}