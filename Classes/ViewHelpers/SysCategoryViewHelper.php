<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 09.11.15
 * Time: 15:08
 */

namespace JAKOTA\Reisedb\ViewHelpers;


class SysCategoryViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param int $syscategory The uid of the sys_category
     * @return string
     */
    public function render($syscategory) {
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('title', 'sys_category', 'uid='.$syscategory);
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
		 
		return $row['title'];
    }
}