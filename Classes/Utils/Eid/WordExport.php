<?php
namespace JAKOTA\Reisedb\Utils\Eid;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Core\Bootstrap;
use TYPO3\CMS\Frontend\Utility\EidUtility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * This class could called with AJAX via eID
 *
 * @author	Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 * @package	TYPO3
 * @subpackage	ReisedbWordExport
 */
class WordExport {

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration;

    /**
     * bootstrap
     *
     * @var array
     */
    protected $bootstrap;

    /**
     * Generates the output
     *
     * @return string		from action
     */
    public function run() {
        return $this->bootstrap->run('', $this->configuration);
    }

    /**
     * Initialize Extbase
     *
     * @param array $TYPO3_CONF_VARS
     */
    public function __construct($TYPO3_CONF_VARS) {
        $this->configuration = array(
            'pluginName' => 'Reisedb',
            'vendorName' => 'JAKOTA',
            'extensionName' => 'Reisedb',
            'controller' => 'Trip',
            'action' => 'wordExport',
            'settings' => array('storagePid' => 13)
        );
        $_POST['tx_reisedb_reisedb']['action'] = 'wordExport';
        $_POST['tx_reisedb_reisedb']['controller'] = 'Trip';


        $this->bootstrap = new Bootstrap();

        $userObj = EidUtility::initFeUser();
        //EidUtility::initLanguage('en');
        $pid = (GeneralUtility::_GET('id') ? GeneralUtility::_GET('id') : 1);
        $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController',
            $TYPO3_CONF_VARS,
            $pid,
            0,
            TRUE
        );
        $GLOBALS['TSFE']->sys_language_uid = (GeneralUtility::_GET('L') ? GeneralUtility::_GET('L') : 0);
        EidUtility::initLanguage();

        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->fe_user = $userObj;
        $GLOBALS['TSFE']->id = $pid;


        $GLOBALS['TSFE']->set_no_cache();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();
        $GLOBALS['TSFE']->cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        $GLOBALS['TSFE']->settingLanguage();
        $GLOBALS['TSFE']->settingLocale();
    }
}

$eid = GeneralUtility::makeInstance('JAKOTA\\Reisedb\\Utils\\Eid\\WordExport', $GLOBALS['TYPO3_CONF_VARS']);
echo $eid->run();