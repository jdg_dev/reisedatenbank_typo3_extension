<?php
namespace JAKOTA\Reisedb\Utils;
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 19.11.15
 * Time: 12:03
 */

if (TYPO3_MODE == 'BE') {
    require_once('../../../Resources/PHP/tcpdf/tcpdf.php');
    require_once('../../../Resources/PHP/fpdi/fpdi.php');
} else {
    require_once('typo3conf/ext/reisedb/Resources/PHP/tcpdf/tcpdf.php');
    require_once('typo3conf/ext/reisedb/Resources/PHP/fpdi/fpdi.php');
}

class TemplateTCPDF extends \FPDI {

    public function __construct() {
        parent::__construct();
    }

    //Page header
    public function Header() {

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}