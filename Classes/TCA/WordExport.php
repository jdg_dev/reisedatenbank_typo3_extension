<?php
namespace JAKOTA\Reisedb\TCA;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class WordExport
{

    public function render($PA, $fObj)
    {
        $output = '<a href="/?eID=reisedbWordExport&L=' . $PA['row']['sys_language_uid'] . '&tx_reisedb_reisedb[tripToRender]=' . $PA['row']['uid'] . '&q=' . md5(rand(0,1000)) . '" style="background-image: url(/typo3/sysext/t3skin/images/backgrounds/button.png); border:1px solid #7C7C7C; padding: 3px 6px; color:#434343;"><img src="/typo3conf/ext/reisedb/1353939828_Download.png" style="vertical-align:middle;" />&nbsp;Word Download (' . $PA['row']['code'] . ' ' . $PA['row']['title'] . '.docx)</a>';
        if (preg_match('/new[0-9a-z]{1,}/', $PA['row']['uid'])) {
            return "<p>Das Word Dokument kann erst nach dem Speichern geladen werden";
        } else {
            return $output;
        }
    }
}