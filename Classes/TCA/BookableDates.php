<?php
namespace JAKOTA\Reisedb\TCA;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class BookableDates {

    public function render($PA, $fObj) {


//        $color = (isset($PA['parameters']['color'])) ? $PA['parameters']['color'] : 'red';
//        $formField  = '<div style="padding: 5px; background-color: ' . $color . ';">';
//        $formField .= '<input type="text" name="' . $PA['itemFormElName'] . '"';
//        $formField .= ' value="' . htmlspecialchars($PA['itemFormElValue']) . '"';
//        $formField .= ' onchange="' . htmlspecialchars(implode('', $PA['fieldChangeFunc'])) . '"';
//        $formField .= $PA['onFocus'];
//        $formField .= ' /></div>';
//        return $formField;


        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        $output = '<div class="app"></div>';
        $output .= '<script type="text/javascript">
                var eventPackage_uid = "'.$PA['row']['uid'].'";

        require([
        "jquery",
        "../typo3conf/ext/reisedb/Resources/Public/JS/moment",
        "../typo3conf/ext/reisedb/Resources/Public/JS/underscore",
        "../typo3conf/ext/reisedb/Resources/Public/JS/backbone"
        ] , function ($, moment) {

        '.file_get_contents($_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/reisedb/Resources/Public/JS/app.js').'

        });


        </script>';
        $output .= '<link rel="stylesheet" type="text/css" href="../../../typo3conf/ext/reisedb/Resources/Public/CSS/app.css?c='.rand(0,100000).'">';
        $output .= '<script type="template/html" id="eventPackageCalendarView">
<button id="previous">&laquo;</button>
<strong><%= year %></strong>
<button id="next">&raquo;</button>
<div class="clearFix"></div>
<hr />
<button id="clear"     class="btn" data-status="0"><span class="arc group0"></span>Gesperrt</button>
<button id="btnGroup1" class="btn" data-status="1"><span class="arc group1"></span>Verfügbar</button>
<button id="btnGroup2" class="btn" data-status="2"><span class="arc group2"></span>eingeschränkt Verfügbar</button>
<hr />
<div class="clearFix"></div>
<div>
    <% var month = 0 %>
    <% moment.locale("de"); %>
    <% _.each(dates, function(date) { %>
    <% var dateObject = new Date(date.get(\'date\')*1000);%>

        <% if (month != parseInt(moment(dateObject).format("M"))) { %>
            <% if (month == 0) { %><div class="month"><% } else { %><div class="clearFix"></div></div><div class="month"><% } %>
            <% month = parseInt(moment(dateObject).format("M")) %>
            <strong data-month="<%= moment(dateObject).format("M") %>" class="monthTitle"><%= moment(dateObject).format("MMMM") %></strong>
            <div data-weekday="0" data-month="<%= moment(dateObject).format("M") %>" class="day workday weekday0 title">Mo</div>
            <div data-weekday="1" data-month="<%= moment(dateObject).format("M") %>" class="day workday weekday1 title">Di</div>
            <div data-weekday="2" data-month="<%= moment(dateObject).format("M") %>" class="day workday weekday2 title">Mi</div>
            <div data-weekday="3" data-month="<%= moment(dateObject).format("M") %>" class="day workday weekday3 title">Do</div>
            <div data-weekday="4" data-month="<%= moment(dateObject).format("M") %>" class="day workday weekday4 title">Fr</div>
            <div data-weekday="5" data-month="<%= moment(dateObject).format("M") %>" class="day weekend weekday5 title">Sa</div>
            <div data-weekday="6" data-month="<%= moment(dateObject).format("M") %>" class="day weekend weekday6 title">So</div>
            <div class="clearFix"></div>
            <% for(var i = 0; i < parseInt(moment(dateObject).format("e")); i++) { %>
                <div class="day filler"></div>
            <% } %>
        <% } %>

        <% if (parseInt(moment(dateObject).format("e")) == 6 || parseInt(moment(dateObject).format("e")) == 5) { var dayType = "weekend" } else { var dayType = "workday" } %>


        <div data-id="<%= date.get("uid") %>" class="day border <%= dayType %> group<%= date.get(\'status\') %>  weekday<%= parseInt(moment(dateObject).format("e")) %> month<%= parseInt(moment(dateObject).format("M")) %>"><%= moment(dateObject).format("D") %></div>


        <% if (parseInt(moment(dateObject).format("e")) == 6) { %><div class="clearFix"></div><% } %>



    <% }); %>
    <div class="clearFix"></div></div>
    <div class="clearFix"></div></div>
</script>';

        /*
{}
        <%= date.get(\'dataGroup\') %>
        <%= date.get(\'priceGroup\') %>
        <%= date.get(\'available\') %>
         */


        if (preg_match('/new[0-9a-z]{1,}/', $PA['row']['uid'])) {
            return "<p>Die Buchbarkeit kann erst nach dem ersten Speichern bearbeitet werden. Bitte speichern Sie das Erlebnispaket ab.";
        } else {
            return $output;
        }
    }
}