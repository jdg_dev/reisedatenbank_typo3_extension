<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "reisedb"
 *
 * Auto generated by Extension Builder 2015-11-09
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Reisedatenbank',
	'description' => 'Reisedatenbank der mecklenburger Radtour, hier wird auch die Ausgabe geregelt und die Buchungen durchgeführt',
	'category' => 'plugin',
	'author' => 'Martin Fünning',
	'author_email' => 'fuenning@jakota.de',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.1.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);