#
# Table structure for table 'tx_reisedb_domain_model_trip'
#
CREATE TABLE tx_reisedb_domain_model_trip (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	code varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	additional_info int(11) DEFAULT '0' NOT NULL,
	seo_title varchar(255) DEFAULT '' NOT NULL,
	seo_keywords varchar(255) DEFAULT '' NOT NULL,
	seo_description text NOT NULL,
	seo_canonical varchar(255) DEFAULT '' NOT NULL,
	seo_noindex tinyint(4) unsigned DEFAULT '0' NOT NULL,
	nights int(11) DEFAULT '0' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	documents int(11) unsigned DEFAULT '0' NOT NULL,
	images int(11) unsigned DEFAULT '0' NOT NULL,
	map int(11) unsigned NOT NULL default '0',
	is_startour tinyint(1) unsigned DEFAULT '0' NOT NULL,
	short_description text NOT NULL,
	verbal_bookable text NOT NULL,
	infoboxes int(11) unsigned DEFAULT '0' NOT NULL,
	stations int(11) unsigned DEFAULT '0' NOT NULL,
	prices int(11) unsigned DEFAULT '0' NOT NULL,
	additional_services int(11) unsigned DEFAULT '0' NOT NULL,
	season_prices int(11) unsigned DEFAULT '0' NOT NULL,
	group_discounts int(11) unsigned DEFAULT '0' NOT NULL,
	child_discounts int(11) unsigned DEFAULT '0' NOT NULL,
	bookable_dates int(11) unsigned DEFAULT '0' NOT NULL,
	event_packages int(11) unsigned DEFAULT '0' NOT NULL,
	reviews int(11) unsigned DEFAULT '0' NOT NULL,
	highlights text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_infobox'
#
CREATE TABLE tx_reisedb_domain_model_infobox (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	content text NOT NULL,
	as_tab tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_station'
#
CREATE TABLE tx_reisedb_domain_model_station (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	code varchar(255) DEFAULT '' NOT NULL,
	tracklength double(11,2) DEFAULT '0.00' NOT NULL,
	tracklength_approx varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	image int(11) unsigned NOT NULL default '0',
	additional_night_description text NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
	addtional_night_prices int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_price'
#
CREATE TABLE tx_reisedb_domain_model_price (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,
	station int(11) unsigned DEFAULT '0' NOT NULL,
	additionalservice int(11) unsigned DEFAULT '0' NOT NULL,

	from_price tinyint(1) unsigned DEFAULT '0' NOT NULL,
	price double(11,2) DEFAULT '0.00' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	year int(11) DEFAULT '0' NOT NULL,
	halfboard double(11,2) DEFAULT '0.00' NOT NULL,
	pricecategory int(11) unsigned DEFAULT '0',
	roomcategory int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_pricecategory'
#
CREATE TABLE tx_reisedb_domain_model_pricecategory (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_roomcategory'
#
CREATE TABLE tx_reisedb_domain_model_roomcategory (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	beds int(11) DEFAULT '0' NOT NULL,
	additional_bed tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_additionalservice'
#
CREATE TABLE tx_reisedb_domain_model_additionalservice (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
	is_rental_bike tinyint(1) unsigned DEFAULT '0' NOT NULL,
	price int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_seasonprice'
#
CREATE TABLE tx_reisedb_domain_model_seasonprice (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	value double(11,2) DEFAULT '0.00' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	from_date int(11) DEFAULT '0' NOT NULL,
	to_date int(11) DEFAULT '0' NOT NULL,
	in_pricecategory int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_groupdiscount'
#
CREATE TABLE tx_reisedb_domain_model_groupdiscount (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	from_day int(11) DEFAULT '0' NOT NULL,
	to_day int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_childdiscount'
#
CREATE TABLE tx_reisedb_domain_model_childdiscount (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	from_age int(11) DEFAULT '0' NOT NULL,
	to_age int(11) DEFAULT '0' NOT NULL,
	value double(11,2) DEFAULT '0.00' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	number_children int(11) DEFAULT '0' NOT NULL,
	number_adult int(11) DEFAULT '0' NOT NULL,
	type_of_bed int(11) DEFAULT '0' NOT NULL,
	in_pricecategory int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_bookabledate'
#
CREATE TABLE tx_reisedb_domain_model_bookabledate (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	date int(11) DEFAULT '0' NOT NULL,
	status int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,

	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_reisedb_domain_model_review'
#
CREATE TABLE tx_reisedb_domain_model_review (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	trip int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	rating_luggage int(11) DEFAULT '0' NOT NULL,
	rating_documents int(11) DEFAULT '0' NOT NULL,
	rating_care int(11) DEFAULT '0' NOT NULL,
	rating_consultation int(11) DEFAULT '0' NOT NULL,
	rating_hotels int(11) DEFAULT '0' NOT NULL,
	rating_overall int(11) DEFAULT '0' NOT NULL,
	rating_quality int(11) DEFAULT '0' NOT NULL,
	quality int(11) DEFAULT '0' NOT NULL,
	images int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_infobox'
#
CREATE TABLE tx_reisedb_domain_model_infobox (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_station'
#
CREATE TABLE tx_reisedb_domain_model_station (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_price'
#
CREATE TABLE tx_reisedb_domain_model_price (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_additionalservice'
#
CREATE TABLE tx_reisedb_domain_model_additionalservice (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_seasonprice'
#
CREATE TABLE tx_reisedb_domain_model_seasonprice (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_groupdiscount'
#
CREATE TABLE tx_reisedb_domain_model_groupdiscount (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_childdiscount'
#
CREATE TABLE tx_reisedb_domain_model_childdiscount (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_bookabledate'
#
CREATE TABLE tx_reisedb_domain_model_bookabledate (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_review'
#
CREATE TABLE tx_reisedb_domain_model_review (

	trip  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_trip'
#
CREATE TABLE tx_reisedb_domain_model_trip (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_reisedb_domain_model_price'
#
CREATE TABLE tx_reisedb_domain_model_price (

	station  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_domain_model_price'
#
CREATE TABLE tx_reisedb_domain_model_price (

	additionalservice  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_reisedb_seasonprice_pricecategory_mm'
#
CREATE TABLE tx_reisedb_seasonprice_pricecategory_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_reisedb_childdiscount_pricecategory_mm'
#
CREATE TABLE tx_reisedb_childdiscount_pricecategory_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (

	review  int(11) unsigned DEFAULT '0' NOT NULL,

);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

# Erlebnispakete
#
CREATE TABLE `tx_reisedb_domain_model_eventpackage` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`hidden` int(11) DEFAULT '0' NOT NULL,
	`deleted` int(11) DEFAULT '0' NOT NULL,
	`sys_language_uid` int(11) DEFAULT '0' NOT NULL,
	`title` varchar(255) default '',
	`description` text NOT NULL,
	`city` varchar(255) default '',
	`detailpage` int(11) DEFAULT '0' NOT NULL,
	`extra_nights` int(11) DEFAULT '0' NOT NULL,
	`group_package` int(11) DEFAULT '0' NOT NULL,
	`extra_data` int(11) DEFAULT '0' NOT NULL,
	`fixprice` int(11) DEFAULT '0' NOT NULL,
	`typeofprice` int(11) DEFAULT '0' NOT NULL,
	`scaledprice` int(11) DEFAULT '0' NOT NULL,
	`roomprice` int(11) DEFAULT '0' NOT NULL,
	`availability` int(11) DEFAULT '0' NOT NULL,
	`trips` int(11) unsigned DEFAULT '0' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);

#
# Table structure for table 'tx_reisedb_trip_event_package_mm'
#
CREATE TABLE tx_reisedb_trip_event_package_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

CREATE TABLE `tx_reisedb_domain_model_eventpackageextradata` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`hidden` int(11) DEFAULT '0' NOT NULL,
	`deleted` int(11) DEFAULT '0' NOT NULL,
	`sys_language_uid` int(11) DEFAULT '0' NOT NULL,
	`event_package` int(11) DEFAULT '0' NOT NULL,
	`title` varchar(255) default '',
	`fieldtype` varchar(255) default '',
	`select_fields` text,
	`dategroup` int(11) DEFAULT '1' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);

CREATE TABLE `tx_reisedb_domain_model_eventpackageavailability` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`event_package` int(11) DEFAULT '0' NOT NULL,
	`dayofavailability` int(11) DEFAULT '0' NOT NULL,
	`available` int(11) DEFAULT '0' NOT NULL,
	`hidden` tinyint(4) unsigned DEFAULT '0' NOT NULL,
	`data_group` int(11) DEFAULT '1' NOT NULL,
	`price_group` int(11) DEFAULT '1' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);

CREATE TABLE `tx_reisedb_domain_model_eventpackagefixedprice` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`hidden` int(11) DEFAULT '0' NOT NULL,
	`deleted` int(11) DEFAULT '0' NOT NULL,
	`sys_language_uid` int(11) DEFAULT '0' NOT NULL,
	`event_package` int(11) DEFAULT '0' NOT NULL,
	`title` varchar(255) default '',
	`price` double(11,2) DEFAULT '0.00' NOT NULL,
	`dategroup` int(11) DEFAULT '1' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);

CREATE TABLE `tx_reisedb_domain_model_eventpackagescaledprice` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`hidden` int(11) DEFAULT '0' NOT NULL,
	`deleted` int(11) DEFAULT '0' NOT NULL,
	`sys_language_uid` int(11) DEFAULT '0' NOT NULL,
	`event_package` int(11) DEFAULT '0' NOT NULL,
	`title` varchar(255) default '',
	`from_age` varchar(255) default '',
	`to_age` varchar(255) default '',
	`price` double(11,2) DEFAULT '0.00' NOT NULL,
	`dategroup` int(11) DEFAULT '1' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);

CREATE TABLE `tx_reisedb_domain_model_eventpackageroomprice` (
	`uid` int(11) auto_increment,
	`pid` int(11) DEFAULT '0' NOT NULL,
	`hidden` int(11) DEFAULT '0' NOT NULL,
	`deleted` int(11) DEFAULT '0' NOT NULL,
	`sys_language_uid` int(11) DEFAULT '0' NOT NULL,
	`event_package` int(11) DEFAULT '0' NOT NULL,
	`pricecategory` int(11) unsigned DEFAULT '0',
	`roomcategory` int(11) unsigned DEFAULT '0',
	`price` double(11,2) DEFAULT '0.00' NOT NULL,
	`dategroup` int(11) DEFAULT '1' NOT NULL,
	`l10n_parent` int(11) DEFAULT '0' NOT NULL,
	`l10n_diffsource` mediumblob,
	`tstamp` int(11) DEFAULT '0' NOT NULL,
	`crdate` int(11) DEFAULT '0' NOT NULL,
	`cruser_id` int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`)
);