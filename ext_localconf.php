<?php

if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'JAKOTA.' . $_EXTKEY,
	'Reisedb',
	array(
		'Trip' => 'list, show, search, booking, categoryList, singleTrip, review, showReview, wordExport',
		
	),
	// non-cacheable actions
	array(
		'Trip' => 'search, booking, review, wordExport',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'JAKOTA.' . $_EXTKEY,
	'categoryList',
	array(
		'Trip' => 'categoryList',

	)
);

// eID for File Upload (FE)
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['reisedbWordExport'] = 'EXT:reisedb/Classes/Utils/Eid/WordExport.php';


## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['JAKOTA\Reisedb\Tasks\CleanupBookableDates'] = array(
	'extension' => $_EXTKEY,
	'title' => 'Reisedatenbank: Bookable Dates aufräumen',
	'description' => 'Setzt abgelaufende Tage, oder Tage die zu weit in der Zukunft liegen auf hidden.',
);




function user_force($uid) {
	$logger = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
	$logger->info('Everything went fine.');

	return true;
	#return \JAKOTA\Reisedb\Condition\SetNoIndex::check($uid);

}
