<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Reisedb',
	'Reisedatenbank'
);

// Include flex forms
$pluginName='Reisedb'; // siehe Tx_Extbase_Utility_Extension::registerPlugin
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);
$pluginSignature = strtolower($extensionName) . '_'.strtolower($pluginName);
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' .  $_EXTKEY . '/Configuration/FlexForm/Trip.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Reisedatenbank');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_trip', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_trip.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_trip');
$GLOBALS['TCA']['tx_reisedb_domain_model_trip'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip',
		'label' => 'code',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,code,description,seo_title,seo_keywords,seo_description,nights,type,documents,images,map,is_startour,short_description,verbal_bookable,infoboxes,stations,prices,additional_services,season_prices,group_discounts,child_discounts,bookable_dates,reviews,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Trip.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_trip.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_infobox', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_infobox.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_infobox');
$GLOBALS['TCA']['tx_reisedb_domain_model_infobox'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_infobox',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'title,content,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Infobox.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_infobox.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_station', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_station.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_station');
$GLOBALS['TCA']['tx_reisedb_domain_model_station'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'title,code,tracklength,tracklength_approx,description,image,additional_night_description,city,addtional_night_prices,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Station.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_station.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_price', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_price.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_price');
$GLOBALS['TCA']['tx_reisedb_domain_model_price'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price',
		'label' => 'from_price',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'from_price,price,type,year,halfboard,pricecategory,roomcategory,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Price.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_price.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_pricecategory', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_pricecategory.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_pricecategory');
$GLOBALS['TCA']['tx_reisedb_domain_model_pricecategory'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_pricecategory',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(

		),
		'searchFields' => 'title,description,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Pricecategory.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_pricecategory.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_roomcategory', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_roomcategory.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_roomcategory');
$GLOBALS['TCA']['tx_reisedb_domain_model_roomcategory'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_roomcategory',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(

		),
		'searchFields' => 'title,description,beds,additional_bed,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Roomcategory.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_roomcategory.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_additionalservice', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_additionalservice.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_additionalservice');
$GLOBALS['TCA']['tx_reisedb_domain_model_additionalservice'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_additionalservice',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,is_rental_bike,price,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/AdditionalService.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_additionalservice.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_seasonprice', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_seasonprice.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_seasonprice');
$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice',
		'label' => 'value',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'value,type,from_date,to_date,in_pricecategory,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/SeasonPrice.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_seasonprice.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_groupdiscount', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_groupdiscount.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_groupdiscount');
$GLOBALS['TCA']['tx_reisedb_domain_model_groupdiscount'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_groupdiscount',
		'label' => 'from_day',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'from_day,to_day,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/GroupDiscount.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_groupdiscount.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_childdiscount', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_childdiscount.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_childdiscount');
$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount',
		'label' => 'from_age',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'from_age,to_age,value,type,number_children,number_adult,type_of_bed,in_pricecategory,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/ChildDiscount.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_childdiscount.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_bookabledate', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_bookabledate.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_bookabledate');
$GLOBALS['TCA']['tx_reisedb_domain_model_bookabledate'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_bookabledate',
		'label' => 'date',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'date,status,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/BookableDate.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_bookabledate.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_review', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_review.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_review');
$GLOBALS['TCA']['tx_reisedb_domain_model_review'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'title,description,name,email,rating_luggage,rating_documents,rating_care,rating_consultation,rating_hotels,rating_overall,quality,images,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Review.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_review.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_reisedb_domain_model_trip'
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$GLOBALS['TCA']['tx_reisedb_domain_model_bookabledate']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_groupdiscount']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_additionalservice']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['ctrl']['hideTable'] = true;
$GLOBALS['TCA']['tx_reisedb_domain_model_infobox']['ctrl']['hideTable'] = true;

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_trip.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_infobox']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_infobox.gif';
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_station.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_price.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_pricecategory']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_pricecategory.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_roomcategory']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_roomcategory.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_additionalservice']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_additionalservice.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_seasonprice.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_groupdiscount']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_groupdiscount.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_childdiscount.png';
$GLOBALS['TCA']['tx_reisedb_domain_model_bookabledate']['ctrl']['iconfile'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_bookabledate.gif';

//region TCA Init: tx_reisedb_domain_model_event_package
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackage', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackage');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackage'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'type'      => 'typeofprice',
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'title,city',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackage.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_event_package.png'
	),
);
//endregion
//region TCA Init: tx_reisedb_domain_model_event_package_extra_data
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackageextradata', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package_extra_data.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackageextradata');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageextradata'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package_extra_data',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'type'      => 'fieldtype',
		'hideTable' => true,
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'title',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackageExtraData.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_event_package_extra_data.png'
	),
);
//endregion
//region TCA Init: tx_reisedb_domain_model_event_package_fixed_price
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackagefixedprice', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package_fixed_price.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackagefixedprice');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackagefixedprice'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package_fixed_price',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'hideTable' => true,
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => '',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackageFixedPrice.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_event_package_price.png'
	),
);
//endregion
//region TCA Init: tx_reisedb_domain_model_event_package_scaled_price
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackagescaledprice', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package_scaled_price.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackagescaledprice');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackagescaledprice'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package_scaled_price',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'hideTable' => true,
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => '',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackageScaledPrice.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_event_package_price.png'
	),
);
//endregion
//region TCA Init: tx_reisedb_domain_model_event_package_room_price
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackageroomprice', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package_room_price.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackageroomprice');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageroomprice'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package_room_price',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'hideTable' => true,
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => '',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackageRoomPrice.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_event_package_price.png'
	),
);
//endregion
//region TCA Init: tx_reisedb_domain_model_event_package_availability
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_reisedb_domain_model_eventpackageavailability', 'EXT:reisedb/Resources/Private/Language/locallang_csh_tx_reisedb_domain_model_event_package_availability.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_reisedb_domain_model_eventpackageavailability');
$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageavailability'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_event_package_availability',
		'label' => 'dayofavailability',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'hideTable' => true,
		'dividers2tabs' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'dayofavailability,available,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EventPackageAvailability.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_reisedb_domain_model_bookabledate.gif'
	),
);
//endregion