<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\Trip.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class TripTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\Trip
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\Trip();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() {
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCodeReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getCode()
		);
	}

	/**
	 * @test
	 */
	public function setCodeForStringSetsCode() {
		$this->subject->setCode('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'code',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() {
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSeoTitleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getSeoTitle()
		);
	}

	/**
	 * @test
	 */
	public function setSeoTitleForStringSetsSeoTitle() {
		$this->subject->setSeoTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'seoTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSeoKeywordsReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getSeoKeywords()
		);
	}

	/**
	 * @test
	 */
	public function setSeoKeywordsForStringSetsSeoKeywords() {
		$this->subject->setSeoKeywords('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'seoKeywords',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSeoDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getSeoDescription()
		);
	}

	/**
	 * @test
	 */
	public function setSeoDescriptionForStringSetsSeoDescription() {
		$this->subject->setSeoDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'seoDescription',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getNightsReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getNights()
		);
	}

	/**
	 * @test
	 */
	public function setNightsForIntegerSetsNights() {
		$this->subject->setNights(12);

		$this->assertAttributeEquals(
			12,
			'nights',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTypeReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getType()
		);
	}

	/**
	 * @test
	 */
	public function setTypeForIntegerSetsType() {
		$this->subject->setType(12);

		$this->assertAttributeEquals(
			12,
			'type',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDocumentsReturnsInitialValueForFileReference() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getDocuments()
		);
	}

	/**
	 * @test
	 */
	public function setDocumentsForFileReferenceSetsDocuments() {
		$document = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$objectStorageHoldingExactlyOneDocuments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneDocuments->attach($document);
		$this->subject->setDocuments($objectStorageHoldingExactlyOneDocuments);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneDocuments,
			'documents',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addDocumentToObjectStorageHoldingDocuments() {
		$document = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$documentsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$documentsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($document));
		$this->inject($this->subject, 'documents', $documentsObjectStorageMock);

		$this->subject->addDocument($document);
	}

	/**
	 * @test
	 */
	public function removeDocumentFromObjectStorageHoldingDocuments() {
		$document = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$documentsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$documentsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($document));
		$this->inject($this->subject, 'documents', $documentsObjectStorageMock);

		$this->subject->removeDocument($document);

	}

	/**
	 * @test
	 */
	public function getImagesReturnsInitialValueForFileReference() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getImages()
		);
	}

	/**
	 * @test
	 */
	public function setImagesForFileReferenceSetsImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$objectStorageHoldingExactlyOneImages = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneImages->attach($image);
		$this->subject->setImages($objectStorageHoldingExactlyOneImages);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneImages,
			'images',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addImageToObjectStorageHoldingImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$imagesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$imagesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($image));
		$this->inject($this->subject, 'images', $imagesObjectStorageMock);

		$this->subject->addImage($image);
	}

	/**
	 * @test
	 */
	public function removeImageFromObjectStorageHoldingImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$imagesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$imagesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($image));
		$this->inject($this->subject, 'images', $imagesObjectStorageMock);

		$this->subject->removeImage($image);

	}

	/**
	 * @test
	 */
	public function getMapReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getMap()
		);
	}

	/**
	 * @test
	 */
	public function setMapForFileReferenceSetsMap() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setMap($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'map',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIsStartourReturnsInitialValueForBoolean() {
		$this->assertSame(
			FALSE,
			$this->subject->getIsStartour()
		);
	}

	/**
	 * @test
	 */
	public function setIsStartourForBooleanSetsIsStartour() {
		$this->subject->setIsStartour(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'isStartour',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getShortDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getShortDescription()
		);
	}

	/**
	 * @test
	 */
	public function setShortDescriptionForStringSetsShortDescription() {
		$this->subject->setShortDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'shortDescription',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getVerbalBookableReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getVerbalBookable()
		);
	}

	/**
	 * @test
	 */
	public function setVerbalBookableForStringSetsVerbalBookable() {
		$this->subject->setVerbalBookable('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'verbalBookable',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getInfoboxesReturnsInitialValueForInfobox() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getInfoboxes()
		);
	}

	/**
	 * @test
	 */
	public function setInfoboxesForObjectStorageContainingInfoboxSetsInfoboxes() {
		$infobox = new \JAKOTA\Reisedb\Domain\Model\Infobox();
		$objectStorageHoldingExactlyOneInfoboxes = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneInfoboxes->attach($infobox);
		$this->subject->setInfoboxes($objectStorageHoldingExactlyOneInfoboxes);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneInfoboxes,
			'infoboxes',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addInfoboxToObjectStorageHoldingInfoboxes() {
		$infobox = new \JAKOTA\Reisedb\Domain\Model\Infobox();
		$infoboxesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$infoboxesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($infobox));
		$this->inject($this->subject, 'infoboxes', $infoboxesObjectStorageMock);

		$this->subject->addInfobox($infobox);
	}

	/**
	 * @test
	 */
	public function removeInfoboxFromObjectStorageHoldingInfoboxes() {
		$infobox = new \JAKOTA\Reisedb\Domain\Model\Infobox();
		$infoboxesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$infoboxesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($infobox));
		$this->inject($this->subject, 'infoboxes', $infoboxesObjectStorageMock);

		$this->subject->removeInfobox($infobox);

	}

	/**
	 * @test
	 */
	public function getStationsReturnsInitialValueForStation() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getStations()
		);
	}

	/**
	 * @test
	 */
	public function setStationsForObjectStorageContainingStationSetsStations() {
		$station = new \JAKOTA\Reisedb\Domain\Model\Station();
		$objectStorageHoldingExactlyOneStations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneStations->attach($station);
		$this->subject->setStations($objectStorageHoldingExactlyOneStations);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneStations,
			'stations',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addStationToObjectStorageHoldingStations() {
		$station = new \JAKOTA\Reisedb\Domain\Model\Station();
		$stationsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$stationsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($station));
		$this->inject($this->subject, 'stations', $stationsObjectStorageMock);

		$this->subject->addStation($station);
	}

	/**
	 * @test
	 */
	public function removeStationFromObjectStorageHoldingStations() {
		$station = new \JAKOTA\Reisedb\Domain\Model\Station();
		$stationsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$stationsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($station));
		$this->inject($this->subject, 'stations', $stationsObjectStorageMock);

		$this->subject->removeStation($station);

	}

	/**
	 * @test
	 */
	public function getPricesReturnsInitialValueForPrice() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getPrices()
		);
	}

	/**
	 * @test
	 */
	public function setPricesForObjectStorageContainingPriceSetsPrices() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$objectStorageHoldingExactlyOnePrices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOnePrices->attach($price);
		$this->subject->setPrices($objectStorageHoldingExactlyOnePrices);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOnePrices,
			'prices',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addPriceToObjectStorageHoldingPrices() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$pricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$pricesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($price));
		$this->inject($this->subject, 'prices', $pricesObjectStorageMock);

		$this->subject->addPrice($price);
	}

	/**
	 * @test
	 */
	public function removePriceFromObjectStorageHoldingPrices() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$pricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$pricesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($price));
		$this->inject($this->subject, 'prices', $pricesObjectStorageMock);

		$this->subject->removePrice($price);

	}

	/**
	 * @test
	 */
	public function getAdditionalServicesReturnsInitialValueForAdditionalService() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getAdditionalServices()
		);
	}

	/**
	 * @test
	 */
	public function setAdditionalServicesForObjectStorageContainingAdditionalServiceSetsAdditionalServices() {
		$additionalService = new \JAKOTA\Reisedb\Domain\Model\AdditionalService();
		$objectStorageHoldingExactlyOneAdditionalServices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneAdditionalServices->attach($additionalService);
		$this->subject->setAdditionalServices($objectStorageHoldingExactlyOneAdditionalServices);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneAdditionalServices,
			'additionalServices',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addAdditionalServiceToObjectStorageHoldingAdditionalServices() {
		$additionalService = new \JAKOTA\Reisedb\Domain\Model\AdditionalService();
		$additionalServicesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$additionalServicesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($additionalService));
		$this->inject($this->subject, 'additionalServices', $additionalServicesObjectStorageMock);

		$this->subject->addAdditionalService($additionalService);
	}

	/**
	 * @test
	 */
	public function removeAdditionalServiceFromObjectStorageHoldingAdditionalServices() {
		$additionalService = new \JAKOTA\Reisedb\Domain\Model\AdditionalService();
		$additionalServicesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$additionalServicesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($additionalService));
		$this->inject($this->subject, 'additionalServices', $additionalServicesObjectStorageMock);

		$this->subject->removeAdditionalService($additionalService);

	}

	/**
	 * @test
	 */
	public function getSeasonPricesReturnsInitialValueForSeasonPrice() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getSeasonPrices()
		);
	}

	/**
	 * @test
	 */
	public function setSeasonPricesForObjectStorageContainingSeasonPriceSetsSeasonPrices() {
		$seasonPrice = new \JAKOTA\Reisedb\Domain\Model\SeasonPrice();
		$objectStorageHoldingExactlyOneSeasonPrices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneSeasonPrices->attach($seasonPrice);
		$this->subject->setSeasonPrices($objectStorageHoldingExactlyOneSeasonPrices);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneSeasonPrices,
			'seasonPrices',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addSeasonPriceToObjectStorageHoldingSeasonPrices() {
		$seasonPrice = new \JAKOTA\Reisedb\Domain\Model\SeasonPrice();
		$seasonPricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$seasonPricesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($seasonPrice));
		$this->inject($this->subject, 'seasonPrices', $seasonPricesObjectStorageMock);

		$this->subject->addSeasonPrice($seasonPrice);
	}

	/**
	 * @test
	 */
	public function removeSeasonPriceFromObjectStorageHoldingSeasonPrices() {
		$seasonPrice = new \JAKOTA\Reisedb\Domain\Model\SeasonPrice();
		$seasonPricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$seasonPricesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($seasonPrice));
		$this->inject($this->subject, 'seasonPrices', $seasonPricesObjectStorageMock);

		$this->subject->removeSeasonPrice($seasonPrice);

	}

	/**
	 * @test
	 */
	public function getGroupDiscountsReturnsInitialValueForGroupDiscount() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getGroupDiscounts()
		);
	}

	/**
	 * @test
	 */
	public function setGroupDiscountsForObjectStorageContainingGroupDiscountSetsGroupDiscounts() {
		$groupDiscount = new \JAKOTA\Reisedb\Domain\Model\GroupDiscount();
		$objectStorageHoldingExactlyOneGroupDiscounts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneGroupDiscounts->attach($groupDiscount);
		$this->subject->setGroupDiscounts($objectStorageHoldingExactlyOneGroupDiscounts);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneGroupDiscounts,
			'groupDiscounts',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addGroupDiscountToObjectStorageHoldingGroupDiscounts() {
		$groupDiscount = new \JAKOTA\Reisedb\Domain\Model\GroupDiscount();
		$groupDiscountsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$groupDiscountsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($groupDiscount));
		$this->inject($this->subject, 'groupDiscounts', $groupDiscountsObjectStorageMock);

		$this->subject->addGroupDiscount($groupDiscount);
	}

	/**
	 * @test
	 */
	public function removeGroupDiscountFromObjectStorageHoldingGroupDiscounts() {
		$groupDiscount = new \JAKOTA\Reisedb\Domain\Model\GroupDiscount();
		$groupDiscountsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$groupDiscountsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($groupDiscount));
		$this->inject($this->subject, 'groupDiscounts', $groupDiscountsObjectStorageMock);

		$this->subject->removeGroupDiscount($groupDiscount);

	}

	/**
	 * @test
	 */
	public function getChildDiscountsReturnsInitialValueForChildDiscount() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getChildDiscounts()
		);
	}

	/**
	 * @test
	 */
	public function setChildDiscountsForObjectStorageContainingChildDiscountSetsChildDiscounts() {
		$childDiscount = new \JAKOTA\Reisedb\Domain\Model\ChildDiscount();
		$objectStorageHoldingExactlyOneChildDiscounts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneChildDiscounts->attach($childDiscount);
		$this->subject->setChildDiscounts($objectStorageHoldingExactlyOneChildDiscounts);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneChildDiscounts,
			'childDiscounts',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addChildDiscountToObjectStorageHoldingChildDiscounts() {
		$childDiscount = new \JAKOTA\Reisedb\Domain\Model\ChildDiscount();
		$childDiscountsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$childDiscountsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($childDiscount));
		$this->inject($this->subject, 'childDiscounts', $childDiscountsObjectStorageMock);

		$this->subject->addChildDiscount($childDiscount);
	}

	/**
	 * @test
	 */
	public function removeChildDiscountFromObjectStorageHoldingChildDiscounts() {
		$childDiscount = new \JAKOTA\Reisedb\Domain\Model\ChildDiscount();
		$childDiscountsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$childDiscountsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($childDiscount));
		$this->inject($this->subject, 'childDiscounts', $childDiscountsObjectStorageMock);

		$this->subject->removeChildDiscount($childDiscount);

	}

	/**
	 * @test
	 */
	public function getBookableDatesReturnsInitialValueForBookableDate() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getBookableDates()
		);
	}

	/**
	 * @test
	 */
	public function setBookableDatesForObjectStorageContainingBookableDateSetsBookableDates() {
		$bookableDate = new \JAKOTA\Reisedb\Domain\Model\BookableDate();
		$objectStorageHoldingExactlyOneBookableDates = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneBookableDates->attach($bookableDate);
		$this->subject->setBookableDates($objectStorageHoldingExactlyOneBookableDates);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneBookableDates,
			'bookableDates',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addBookableDateToObjectStorageHoldingBookableDates() {
		$bookableDate = new \JAKOTA\Reisedb\Domain\Model\BookableDate();
		$bookableDatesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$bookableDatesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($bookableDate));
		$this->inject($this->subject, 'bookableDates', $bookableDatesObjectStorageMock);

		$this->subject->addBookableDate($bookableDate);
	}

	/**
	 * @test
	 */
	public function removeBookableDateFromObjectStorageHoldingBookableDates() {
		$bookableDate = new \JAKOTA\Reisedb\Domain\Model\BookableDate();
		$bookableDatesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$bookableDatesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($bookableDate));
		$this->inject($this->subject, 'bookableDates', $bookableDatesObjectStorageMock);

		$this->subject->removeBookableDate($bookableDate);

	}

	/**
	 * @test
	 */
	public function getReviewsReturnsInitialValueForReview() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getReviews()
		);
	}

	/**
	 * @test
	 */
	public function setReviewsForObjectStorageContainingReviewSetsReviews() {
		$review = new \JAKOTA\Reisedb\Domain\Model\Review();
		$objectStorageHoldingExactlyOneReviews = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneReviews->attach($review);
		$this->subject->setReviews($objectStorageHoldingExactlyOneReviews);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneReviews,
			'reviews',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addReviewToObjectStorageHoldingReviews() {
		$review = new \JAKOTA\Reisedb\Domain\Model\Review();
		$reviewsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$reviewsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($review));
		$this->inject($this->subject, 'reviews', $reviewsObjectStorageMock);

		$this->subject->addReview($review);
	}

	/**
	 * @test
	 */
	public function removeReviewFromObjectStorageHoldingReviews() {
		$review = new \JAKOTA\Reisedb\Domain\Model\Review();
		$reviewsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$reviewsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($review));
		$this->inject($this->subject, 'reviews', $reviewsObjectStorageMock);

		$this->subject->removeReview($review);

	}
}
