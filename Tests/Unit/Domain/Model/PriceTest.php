<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\Price.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class PriceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\Price
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\Price();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getFromPriceReturnsInitialValueForBoolean() {
		$this->assertSame(
			FALSE,
			$this->subject->getFromPrice()
		);
	}

	/**
	 * @test
	 */
	public function setFromPriceForBooleanSetsFromPrice() {
		$this->subject->setFromPrice(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'fromPrice',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPriceReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getPrice()
		);
	}

	/**
	 * @test
	 */
	public function setPriceForFloatSetsPrice() {
		$this->subject->setPrice(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'price',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getTypeReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getType()
		);
	}

	/**
	 * @test
	 */
	public function setTypeForIntegerSetsType() {
		$this->subject->setType(12);

		$this->assertAttributeEquals(
			12,
			'type',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getYearReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getYear()
		);
	}

	/**
	 * @test
	 */
	public function setYearForIntegerSetsYear() {
		$this->subject->setYear(12);

		$this->assertAttributeEquals(
			12,
			'year',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getHalfboardReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getHalfboard()
		);
	}

	/**
	 * @test
	 */
	public function setHalfboardForFloatSetsHalfboard() {
		$this->subject->setHalfboard(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'halfboard',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getPricecategoryReturnsInitialValueForPricecategory() {
		$this->assertEquals(
			NULL,
			$this->subject->getPricecategory()
		);
	}

	/**
	 * @test
	 */
	public function setPricecategoryForPricecategorySetsPricecategory() {
		$pricecategoryFixture = new \JAKOTA\Reisedb\Domain\Model\Pricecategory();
		$this->subject->setPricecategory($pricecategoryFixture);

		$this->assertAttributeEquals(
			$pricecategoryFixture,
			'pricecategory',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRoomcategoryReturnsInitialValueForRoomcategory() {
		$this->assertEquals(
			NULL,
			$this->subject->getRoomcategory()
		);
	}

	/**
	 * @test
	 */
	public function setRoomcategoryForRoomcategorySetsRoomcategory() {
		$roomcategoryFixture = new \JAKOTA\Reisedb\Domain\Model\Roomcategory();
		$this->subject->setRoomcategory($roomcategoryFixture);

		$this->assertAttributeEquals(
			$roomcategoryFixture,
			'roomcategory',
			$this->subject
		);
	}
}
