<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\SeasonPrice.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class SeasonPriceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\SeasonPrice
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\SeasonPrice();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getValueReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getValue()
		);
	}

	/**
	 * @test
	 */
	public function setValueForFloatSetsValue() {
		$this->subject->setValue(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'value',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getTypeReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getType()
		);
	}

	/**
	 * @test
	 */
	public function setTypeForIntegerSetsType() {
		$this->subject->setType(12);

		$this->assertAttributeEquals(
			12,
			'type',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFromDateReturnsInitialValueForDateTime() {
		$this->assertEquals(
			NULL,
			$this->subject->getFromDate()
		);
	}

	/**
	 * @test
	 */
	public function setFromDateForDateTimeSetsFromDate() {
		$dateTimeFixture = new \DateTime();
		$this->subject->setFromDate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'fromDate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getToDateReturnsInitialValueForDateTime() {
		$this->assertEquals(
			NULL,
			$this->subject->getToDate()
		);
	}

	/**
	 * @test
	 */
	public function setToDateForDateTimeSetsToDate() {
		$dateTimeFixture = new \DateTime();
		$this->subject->setToDate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'toDate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getInPricecategoryReturnsInitialValueForPricecategory() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getInPricecategory()
		);
	}

	/**
	 * @test
	 */
	public function setInPricecategoryForObjectStorageContainingPricecategorySetsInPricecategory() {
		$inPricecategory = new \JAKOTA\Reisedb\Domain\Model\Pricecategory();
		$objectStorageHoldingExactlyOneInPricecategory = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneInPricecategory->attach($inPricecategory);
		$this->subject->setInPricecategory($objectStorageHoldingExactlyOneInPricecategory);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneInPricecategory,
			'inPricecategory',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addInPricecategoryToObjectStorageHoldingInPricecategory() {
		$inPricecategory = new \JAKOTA\Reisedb\Domain\Model\Pricecategory();
		$inPricecategoryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$inPricecategoryObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($inPricecategory));
		$this->inject($this->subject, 'inPricecategory', $inPricecategoryObjectStorageMock);

		$this->subject->addInPricecategory($inPricecategory);
	}

	/**
	 * @test
	 */
	public function removeInPricecategoryFromObjectStorageHoldingInPricecategory() {
		$inPricecategory = new \JAKOTA\Reisedb\Domain\Model\Pricecategory();
		$inPricecategoryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$inPricecategoryObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($inPricecategory));
		$this->inject($this->subject, 'inPricecategory', $inPricecategoryObjectStorageMock);

		$this->subject->removeInPricecategory($inPricecategory);

	}
}
