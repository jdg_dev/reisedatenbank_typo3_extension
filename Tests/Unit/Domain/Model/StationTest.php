<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\Station.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class StationTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\Station
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\Station();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() {
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCodeReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getCode()
		);
	}

	/**
	 * @test
	 */
	public function setCodeForStringSetsCode() {
		$this->subject->setCode('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'code',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTracklengthReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getTracklength()
		);
	}

	/**
	 * @test
	 */
	public function setTracklengthForFloatSetsTracklength() {
		$this->subject->setTracklength(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'tracklength',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getTracklengthApproxReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTracklengthApprox()
		);
	}

	/**
	 * @test
	 */
	public function setTracklengthApproxForStringSetsTracklengthApprox() {
		$this->subject->setTracklengthApprox('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'tracklengthApprox',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() {
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForFileReferenceSetsImage() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImage($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAdditionalNightDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getAdditionalNightDescription()
		);
	}

	/**
	 * @test
	 */
	public function setAdditionalNightDescriptionForStringSetsAdditionalNightDescription() {
		$this->subject->setAdditionalNightDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'additionalNightDescription',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCityReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getCity()
		);
	}

	/**
	 * @test
	 */
	public function setCityForStringSetsCity() {
		$this->subject->setCity('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'city',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAddtionalNightPricesReturnsInitialValueForPrice() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getAddtionalNightPrices()
		);
	}

	/**
	 * @test
	 */
	public function setAddtionalNightPricesForObjectStorageContainingPriceSetsAddtionalNightPrices() {
		$addtionalNightPrice = new \JAKOTA\Reisedb\Domain\Model\Price();
		$objectStorageHoldingExactlyOneAddtionalNightPrices = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneAddtionalNightPrices->attach($addtionalNightPrice);
		$this->subject->setAddtionalNightPrices($objectStorageHoldingExactlyOneAddtionalNightPrices);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneAddtionalNightPrices,
			'addtionalNightPrices',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addAddtionalNightPriceToObjectStorageHoldingAddtionalNightPrices() {
		$addtionalNightPrice = new \JAKOTA\Reisedb\Domain\Model\Price();
		$addtionalNightPricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$addtionalNightPricesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($addtionalNightPrice));
		$this->inject($this->subject, 'addtionalNightPrices', $addtionalNightPricesObjectStorageMock);

		$this->subject->addAddtionalNightPrice($addtionalNightPrice);
	}

	/**
	 * @test
	 */
	public function removeAddtionalNightPriceFromObjectStorageHoldingAddtionalNightPrices() {
		$addtionalNightPrice = new \JAKOTA\Reisedb\Domain\Model\Price();
		$addtionalNightPricesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$addtionalNightPricesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($addtionalNightPrice));
		$this->inject($this->subject, 'addtionalNightPrices', $addtionalNightPricesObjectStorageMock);

		$this->subject->removeAddtionalNightPrice($addtionalNightPrice);

	}
}
