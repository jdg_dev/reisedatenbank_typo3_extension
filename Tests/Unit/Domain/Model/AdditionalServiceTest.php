<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\AdditionalService.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class AdditionalServiceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\AdditionalService
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\AdditionalService();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName() {
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIsRentalBikeReturnsInitialValueForBoolean() {
		$this->assertSame(
			FALSE,
			$this->subject->getIsRentalBike()
		);
	}

	/**
	 * @test
	 */
	public function setIsRentalBikeForBooleanSetsIsRentalBike() {
		$this->subject->setIsRentalBike(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'isRentalBike',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPriceReturnsInitialValueForPrice() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getPrice()
		);
	}

	/**
	 * @test
	 */
	public function setPriceForObjectStorageContainingPriceSetsPrice() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$objectStorageHoldingExactlyOnePrice = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOnePrice->attach($price);
		$this->subject->setPrice($objectStorageHoldingExactlyOnePrice);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOnePrice,
			'price',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addPriceToObjectStorageHoldingPrice() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$priceObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$priceObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($price));
		$this->inject($this->subject, 'price', $priceObjectStorageMock);

		$this->subject->addPrice($price);
	}

	/**
	 * @test
	 */
	public function removePriceFromObjectStorageHoldingPrice() {
		$price = new \JAKOTA\Reisedb\Domain\Model\Price();
		$priceObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$priceObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($price));
		$this->inject($this->subject, 'price', $priceObjectStorageMock);

		$this->subject->removePrice($price);

	}
}
