<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\GroupDiscount.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class GroupDiscountTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\GroupDiscount
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\GroupDiscount();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getFromDayReturnsInitialValueForDateTime() {
		$this->assertEquals(
			NULL,
			$this->subject->getFromDay()
		);
	}

	/**
	 * @test
	 */
	public function setFromDayForDateTimeSetsFromDay() {
		$dateTimeFixture = new \DateTime();
		$this->subject->setFromDay($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'fromDay',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getToDayReturnsInitialValueForDateTime() {
		$this->assertEquals(
			NULL,
			$this->subject->getToDay()
		);
	}

	/**
	 * @test
	 */
	public function setToDayForDateTimeSetsToDay() {
		$dateTimeFixture = new \DateTime();
		$this->subject->setToDay($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'toDay',
			$this->subject
		);
	}
}
