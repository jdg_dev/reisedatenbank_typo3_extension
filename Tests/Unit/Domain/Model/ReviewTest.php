<?php

namespace JAKOTA\Reisedb\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \JAKOTA\Reisedb\Domain\Model\Review.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class ReviewTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \JAKOTA\Reisedb\Domain\Model\Review
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \JAKOTA\Reisedb\Domain\Model\Review();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() {
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() {
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName() {
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEmailReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getEmail()
		);
	}

	/**
	 * @test
	 */
	public function setEmailForStringSetsEmail() {
		$this->subject->setEmail('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'email',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingLuggageReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingLuggage()
		);
	}

	/**
	 * @test
	 */
	public function setRatingLuggageForIntegerSetsRatingLuggage() {
		$this->subject->setRatingLuggage(12);

		$this->assertAttributeEquals(
			12,
			'ratingLuggage',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingDocumentsReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingDocuments()
		);
	}

	/**
	 * @test
	 */
	public function setRatingDocumentsForIntegerSetsRatingDocuments() {
		$this->subject->setRatingDocuments(12);

		$this->assertAttributeEquals(
			12,
			'ratingDocuments',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingCareReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingCare()
		);
	}

	/**
	 * @test
	 */
	public function setRatingCareForIntegerSetsRatingCare() {
		$this->subject->setRatingCare(12);

		$this->assertAttributeEquals(
			12,
			'ratingCare',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingConsultationReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingConsultation()
		);
	}

	/**
	 * @test
	 */
	public function setRatingConsultationForIntegerSetsRatingConsultation() {
		$this->subject->setRatingConsultation(12);

		$this->assertAttributeEquals(
			12,
			'ratingConsultation',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingHotelsReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingHotels()
		);
	}

	/**
	 * @test
	 */
	public function setRatingHotelsForIntegerSetsRatingHotels() {
		$this->subject->setRatingHotels(12);

		$this->assertAttributeEquals(
			12,
			'ratingHotels',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRatingOverallReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getRatingOverall()
		);
	}

	/**
	 * @test
	 */
	public function setRatingOverallForIntegerSetsRatingOverall() {
		$this->subject->setRatingOverall(12);

		$this->assertAttributeEquals(
			12,
			'ratingOverall',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getQualityReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getQuality()
		);
	}

	/**
	 * @test
	 */
	public function setQualityForIntegerSetsQuality() {
		$this->subject->setQuality(12);

		$this->assertAttributeEquals(
			12,
			'quality',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImagesReturnsInitialValueForFileReference() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getImages()
		);
	}

	/**
	 * @test
	 */
	public function setImagesForFileReferenceSetsImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$objectStorageHoldingExactlyOneImages = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneImages->attach($image);
		$this->subject->setImages($objectStorageHoldingExactlyOneImages);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneImages,
			'images',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addImageToObjectStorageHoldingImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$imagesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$imagesObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($image));
		$this->inject($this->subject, 'images', $imagesObjectStorageMock);

		$this->subject->addImage($image);
	}

	/**
	 * @test
	 */
	public function removeImageFromObjectStorageHoldingImages() {
		$image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$imagesObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$imagesObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($image));
		$this->inject($this->subject, 'images', $imagesObjectStorageMock);

		$this->subject->removeImage($image);

	}
}
