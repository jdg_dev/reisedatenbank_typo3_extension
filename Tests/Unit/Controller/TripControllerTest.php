<?php
namespace JAKOTA\Reisedb\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class JAKOTA\Reisedb\Controller\TripController.
 *
 * @author Martin Fünning <fuenning@jakota.de>
 */
class TripControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \JAKOTA\Reisedb\Controller\TripController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('JAKOTA\\Reisedb\\Controller\\TripController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllTripsFromRepositoryAndAssignsThemToView() {

		$allTrips = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$tripRepository = $this->getMock('JAKOTA\\Reisedb\\Domain\\Repository\\TripRepository', array('findAll'), array(), '', FALSE);
		$tripRepository->expects($this->once())->method('findAll')->will($this->returnValue($allTrips));
		$this->inject($this->subject, 'tripRepository', $tripRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('trips', $allTrips);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenTripToView() {
		$trip = new \JAKOTA\Reisedb\Domain\Model\Trip();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('trip', $trip);

		$this->subject->showAction($trip);
	}
}
