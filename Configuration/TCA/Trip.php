<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_trip'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_trip']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, code, description, seo_title, seo_keywords, seo_description, nights, type, documents, images, map, is_startour, short_description, verbal_bookable, infoboxes, stations, prices, additional_services, season_prices, group_discounts, child_discounts, bookable_dates, reviews, highlights',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, code, description;;;richtext:rte_transform[mode=ts_links], seo_title, seo_keywords, seo_description, nights, type, documents, images, map, is_startour, short_description, verbal_bookable, infoboxes, stations, prices, additional_services, season_prices, group_discounts, child_discounts, bookable_dates, reviews, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime,highlights'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_trip',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_trip.pid=###CURRENT_PID### AND tx_reisedb_domain_model_trip.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'code' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.code',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'highlights' => array(
			'exclude' => 1,
			'label' => 'Highlights dieser Radtour',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
			'defaultExtras' => 'richtext[]'
		),
		'seo_title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.seo_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'seo_keywords' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.seo_keywords',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'seo_description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.seo_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'nights' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.nights',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'documents' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.documents',
			'config' => 
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'documents',
				array('maxitems' => 5)
			),

		),
		'images' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.images',
			'config' => 
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array('maxitems' => 5),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),

		),
		'map' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.map',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'map',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'is_startour' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.is_startour',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'short_description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.short_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'verbal_bookable' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.verbal_bookable',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'infoboxes' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.infoboxes',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_infobox',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'stations' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.stations',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_station',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'prices' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.prices',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_price',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'additional_services' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.additional_services',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_additionalservice',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'season_prices' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.season_prices',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_seasonprice',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'group_discounts' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.group_discounts',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_groupdiscount',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'child_discounts' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.child_discounts',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_childdiscount',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'bookable_dates' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.bookable_dates',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_bookabledate',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'reviews' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.reviews',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_review',
				'foreign_field' => 'trip',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['types'] = array(
    '1' => array('showitem' => 'word_download, --palette--;Allgemeines;name, short_description;Kurzbeschreibung, description;Reisebeschreibung;;richtext:rte_transform[mode=ts_links], highlights;Highlights dieser Radtour,categories, additional_info;Zusatzinformationen;;richtext:rte_transform[mode=ts_links], --palette--;Sichtbarkeit;access, --palette--;Hidden;donotshow,
	--div--;SEO, --palette--;SEO;seo,
	--div--;Medien, images;Reisebilder (max. 5 Stück), map;Routenkarte (max. eine), documents;Dokumente (max. 5 Stück),
	--div--;Infoboxen, infoboxes;Infoboxen,
	--div--;Stationen, is_startour;Ist eine Sterntour, stations;Stationen,
	--div--;Preise, prices;Preise, additional_services;Zusatzleistungen, season_prices;Saisonpreise, group_discounts;Gruppenrabatte, child_discounts;Kinderermäßigungen (nur Partnertouren),
	--div--;Buchbarkeit, verbal_bookable;Buchbarkeit formuliert, bookable_dates;Buchbare Tage,
	--div--;Bewertungen, reviews,
	--div--;Extras, event_packages,
	'),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['palettes'] = array(
    '1' => array('showitem' => ''),
    'name' => array('showitem' => 'code;Reisecode, title;Name, --linebreak--, nights;Anzahl der Übernachtungen, type;Art', 'canNotCollapse' => 1),
    'access' => array('showitem' => 'starttime, endtime, hidden;;1', 'canNotCollapse' => 1),
    'seo' => array('showitem' => 'seo_title;Seitentitel, seo_keywords;Meta Keywords, --linebreak--, seo_description;Meta Description, --linebreak--, seo_canonical;Canonical URL, seo_noindex;NoIndex/NoFollow', 'canNotCollapse' => 1),
    'donotshow' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['code'] = array(
    'exclude' => 1,

    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.code',
    'config' => array(
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim'
    ),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['type'] = array(
    'exclude' => 1,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.type',
    'config' => array(
        'type' => 'select',
        'items' => array(
            array('Originaltour', 0),
            array('Partnertour', 1),
        ),
        'size' => 1,
        'maxitems' => 1,
        'eval' => ''
    ),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['verbal_bookable'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.verbal_bookable',
	'config' => array(
		'type' => 'text',
		'cols' => 40,
		'rows' => 5,
		'eval' => 'trim'
	)
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['prices'] = array(
    'exclude' => 1,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.prices',
    'config' => array(
        'type' => 'inline',
        'foreign_table' => 'tx_reisedb_domain_model_price',
        'foreign_field' => 'trip',
        'foreign_record_defaults' => array(
            'type' => 0,
            'year' => date('Y'),
        ),
        'maxitems' => 9999,
        'appearance' => array(
            'collapseAll' => 0,
            'levelLinksPosition' => 'top',
            'showSynchronizationLink' => 1,
            'showPossibleLocalizationRecords' => 1,
            'showAllLocalizationLink' => 1
        ),
    ),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['documents'] = array(
    'exclude' => 1,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.documents',
    'config' =>
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'documents',
            array(
                'maxitems' => 5,
                'appearance' => array(
                    'useSortable' => TRUE,
                    'headerThumbnail' => array(
                        'field' => 'uid_local',
                        'width' => '45',
                        'height' => '45c',
                    ),
                    'showPossibleLocalizationRecords' => TRUE,
                    'showRemovedLocalizationRecords' => TRUE,
                    'showSynchronizationLink' => TRUE,
                    'showAllLocalizationLink' => TRUE,

                    'enabledControls' => array(
                        'info' => TRUE,
                        'new' => FALSE,
                        'dragdrop' => TRUE,
                        'sort' => FALSE,
                        'hide' => TRUE,
                        'delete' => TRUE,
                        'localize' => TRUE,
                    ),
                ),
            )
        ),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['images'] = array(
    'exclude' => 1,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.images',
    'config' =>
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'images',
            array(
                'maxitems' => 5,
                'appearance' => array(
                    'useSortable' => TRUE,
                    'headerThumbnail' => array(
                        'field' => 'uid_local',
                        'width' => '45',
                        'height' => '45c',
                    ),
                    'showPossibleLocalizationRecords' => TRUE,
                    'showRemovedLocalizationRecords' => TRUE,
                    'showSynchronizationLink' => TRUE,
                    'showAllLocalizationLink' => TRUE,

                    'enabledControls' => array(
                        'info' => TRUE,
                        'new' => FALSE,
                        'dragdrop' => TRUE,
                        'sort' => FALSE,
                        'hide' => TRUE,
                        'delete' => TRUE,
                        'localize' => TRUE,
                    ),
                ),
            ),
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),

);
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['map'] = array(
    'exclude' => 1,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.map',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'map',
        array(
            'maxitems' => 1,
            'appearance' => array(
                'useSortable' => TRUE,
                'headerThumbnail' => array(
                    'field' => 'uid_local',
                    'width' => '45',
                    'height' => '45c',
                ),
                'showPossibleLocalizationRecords' => TRUE,
                'showRemovedLocalizationRecords' => TRUE,
                'showSynchronizationLink' => TRUE,
                'showAllLocalizationLink' => TRUE,

                'enabledControls' => array(
                    'info' => TRUE,
                    'new' => FALSE,
                    'dragdrop' => TRUE,
                    'sort' => FALSE,
                    'hide' => TRUE,
                    'delete' => TRUE,
                    'localize' => TRUE,
                ),
            ),
        ),
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
    ),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['bookable_dates'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.bookable_dates',
	'config' => array(
		'type' => 'user',
		'userFunc' => 'JAKOTA\\Reisedb\\TCA\\BookableDates->render',
		'foreign_table' => 'tx_reisedb_domain_model_bookabledate',
		'foreign_field' => 'trip',
		'foreign_sortby' => 'date',
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['word_download'] = array(
	'exclude' => 1,
	'label' => 'Word Download',
	'config' => array(
		'type' => 'user',
		'userFunc' => 'JAKOTA\\Reisedb\\TCA\\WordExport->render',
	),
);


$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['short_description'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.short_description',
	'config' => array(
		'type' => 'text',
		'cols' => 40,
		'rows' => 5,
		'eval' => 'trim'
	)
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['stations'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.stations',
	'config' => array(
		'type' => 'inline',
		'foreign_table' => 'tx_reisedb_domain_model_station',
		'foreign_field' => 'trip',
		'foreign_sortby' => 'sorting',
		'maxitems'      => 9999,
		'appearance' => array(
			'collapseAll' => 0,
			'levelLinksPosition' => 'top',
			'showSynchronizationLink' => 1,
			'showPossibleLocalizationRecords' => 1,
			'useSortable' => 1,
			'showAllLocalizationLink' => 1
		),
	),

);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['reviews'] = array(
    'exclude' => 0,
    'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.reviews',
    'config' => array(
        'type' => 'inline',
        'foreign_table' => 'tx_reisedb_domain_model_review',
        'foreign_field' => 'trip',
        'foreign_sortby' => 'crdate',
        'maxitems'      => 9999,
        'appearance' => array(
            'collapseAll' => 1,
            'levelLinksPosition' => 'top',
            'showSynchronizationLink' => 0,
            'showPossibleLocalizationRecords' => 0,
            'showAllLocalizationLink' => 0
        ),
    ),

);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['event_packages'] = array(
	'exclude' => 0,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_trip.event_packages',
	'config' => array(
		'type' => 'select',
		'foreign_table' => 'tx_reisedb_domain_model_eventpackage',
		'MM' => 'tx_reisedb_trip_event_package_mm',
		'size' => 10,
		'autoSizeMax' => 30,
		'maxitems' => 9999,
		'multiple' => 0,
		'wizards' => array(
			'_PADDING' => 1,
			'_VERTICAL' => 1,
			'edit' => array(
				'type' => 'popup',
				'title' => 'Edit',
				'script' => 'wizard_edit.php',
				'icon' => 'edit2.gif',
				'popup_onlyOpenIfSelected' => 1,
				'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
			),
			'add' => Array(
				'type' => 'script',
				'title' => 'Create new',
				'icon' => 'add.gif',
				'params' => array(
					'table' => 'tx_reisedb_domain_model_eventpackage',
					'pid' => '###CURRENT_PID###',
					'setValue' => 'prepend'
				),
				'script' => 'wizard_add.php',
			),
		),
	),

);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['seo_canonical'] = array(
	'exclude' => 0,
	'label' => 'SEO Canonical URL',
	'config' => array(
		'type' => 'input',
		'size' => 30,
		'eval' => 'trim'
	),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['seo_noindex'] = array(
	'exclude' => 0,
	'label' => 'SEO NoIndex/NoFollow',
	'config' => array(
		'type' => 'check',
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['additional_info'] = array(
	'exclude' => 0,
	'label' => 'Seite mit Zusatzinformationen',
	'l10n_mode' => '',
	'config' => Array(
		'type' => 'group',
		'internal_type' => 'db',
		'allowed' => 'pages',
		'size' => '1',
		'maxitems' => '1',
		'minitems' => '0',
		'show_thumbs' => '1',
		'wizards' => array(
			'suggest' => array(
				'type' => 'suggest'
			),
		),
	)
);


$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['code']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['additional_info']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['nights']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['type']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['categories']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['is_startour']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['prices']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['season_prices']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['group_discounts']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['child_discounts']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['bookable_dates']['l10n_mode'] = 'exclude';

$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['infoboxes']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['stations']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['prices']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['additional_services']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['season_prices']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['group_discounts']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['child_discounts']['config']['appearance']['collapseAll'] = 1;
$GLOBALS['TCA']['tx_reisedb_domain_model_trip']['columns']['child_discounts']['config']['appearance']['collapseAll'] = 1;