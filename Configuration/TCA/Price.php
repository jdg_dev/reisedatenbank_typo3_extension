<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_price'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_price']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, from_price, price, type, year, halfboard, pricecategory, roomcategory',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, from_price, price, type, year, halfboard, pricecategory, roomcategory, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_price',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_price.pid=###CURRENT_PID### AND tx_reisedb_domain_model_price.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'from_price' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.from_price',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'price' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.price',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			)
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'year' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.year',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'halfboard' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.halfboard',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			)
		),
		'pricecategory' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.pricecategory',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'roomcategory' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.roomcategory',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_reisedb_domain_model_roomcategory',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
		'trip' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'station' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'additionalservice' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['types'] = array(
	'1' => array('showitem' => '--palette--;;donotshow, --palette--;;price'),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_price']['palettes'] = array(
	'price' => array('showitem' => 'price;Preis, halfboard;HP-Zuschlag, from_price;ab-Preis, year;Jahrgang, pricecategory;Preiskategorie, roomcategory;Zimmerkategorie', 'canNotCollapse' => 1),
	'donotshow' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, type', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_price']['columns']['type'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.type',
	'config' => array(
		'type' => 'select',
		'items' => array(
			array('Roomprice', 0),
			array('SinglePrice', 1),
		),
		'size' => 1,
		'maxitems' => 1,
		'eval' => ''
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_price']['columns']['pricecategory'] = array(
	'displayCond' => 'FIELD:type:=:0',
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.pricecategory',
	'config' => array(
		'type' => 'select',
		'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
		'foreign_table_where' => 'AND tx_reisedb_domain_model_pricecategory.sys_language_uid = 0',
		'minitems' => 0,
		'maxitems' => 1,
	),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['columns']['roomcategory'] = array(
	'displayCond' => 'FIELD:type:=:0',
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.roomcategory',
	'config' => array(
		'type' => 'select',
		'foreign_table' => 'tx_reisedb_domain_model_roomcategory',
		'minitems' => 0,
		'maxitems' => 1,
	),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['columns']['price'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.price',
	'config' => array(
		'type' => 'input',
		'size' => 5,
		'eval' => 'double2'
	)
);
$GLOBALS['TCA']['tx_reisedb_domain_model_price']['columns']['halfboard'] = array(
	'displayCond' => 'FIELD:type:=:0',
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_price.halfboard',
	'config' => array(
		'type' => 'input',
		'size' => 5,
		'eval' => 'double2'
	)
);