<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_review'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_review']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, name, email, rating_luggage, rating_documents, rating_care, rating_consultation, rating_hotels, rating_overall, quality, images',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, description, name, email, rating_luggage, rating_documents, rating_care, rating_consultation, rating_hotels, rating_overall, quality, images, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_review',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_review.pid=###CURRENT_PID### AND tx_reisedb_domain_model_review.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'rating_luggage' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_luggage',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'rating_documents' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_documents',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'rating_care' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_care',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'rating_consultation' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_consultation',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'rating_hotels' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_hotels',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'rating_overall' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.rating_overall',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'quality' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.quality',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'images' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_review.images',
			'config' => 
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array('maxitems' => 9999),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),

		),
		
		'trip' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['types'] = array(
    '1' => array('showitem' => 'title;Überschrift, description;Text, images;Bilder, --palette--;Name;name, --palette--;Bewertung;bewertung, quality;Bewertungsqualität,
	'),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_review']['palettes'] = array(
    '1' => array('showitem' => ''),
    'name' => array('showitem' => 'name;Name, email;E-Mail', 'canNotCollapse' => 1),
    'bewertung' => array('showitem' => 'rating_luggage;Gepäcktransport, rating_documents;Reiseunterlagen, rating_care;Betreuung, --linebreak--, rating_consultation;Beratung, rating_hotels;Hotels, rating_overall;Gesamtbewertung', 'canNotCollapse' => 1),
    'donotshow' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_luggage']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_documents']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_care']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_consultation']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_hotels']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['rating_overall']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['quality']['config']['size'] = 2;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['name']['config']['size'] = 15;
$GLOBALS['TCA']['tx_reisedb_domain_model_review']['columns']['email']['config']['size'] = 15;