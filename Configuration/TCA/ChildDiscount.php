<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, from_age, to_age, value, type, number_children, number_adult, type_of_bed, in_pricecategory',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, from_age, to_age, value, type, number_children, number_adult, type_of_bed, in_pricecategory, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_childdiscount',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_childdiscount.pid=###CURRENT_PID### AND tx_reisedb_domain_model_childdiscount.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'from_age' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.from_age',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'to_age' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.to_age',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'value' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.value',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			)
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'number_children' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.number_children',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'number_adult' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.number_adult',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'type_of_bed' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.type_of_bed',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'in_pricecategory' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.in_pricecategory',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
				'MM' => 'tx_reisedb_childdiscount_pricecategory_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_reisedb_domain_model_pricecategory',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		
		'trip' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['types'] = array(
	'1' => array('showitem' => '--palette--;;donotshow, --palette--;Alter;age, --palette--;Betrag;value, --palette--;Bedinung;condition, in_pricecategory;Preiskategorien'),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['palettes'] = array(
	'1' => array('showitem' => ''),
	'age' => array('showitem' => 'from_age;von, to_age;bis', 'canNotCollapse' => 1),
	'value' => array('showitem' => 'value;Betrag (Positiv = Zuschlag / Negativ = Abschlag), type;art', 'canNotCollapse' => 1),
	'condition' => array('showitem' => 'number_children;Anzahl Kinder, number_adult;Anzahl Erwachsene, type_of_bed;Unterbringung', 'canNotCollapse' => 1),
	'donotshow' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, starttime, endtime', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['columns']['type'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.type',
	'config' => array(
		'type' => 'select',
		'items' => array(
			array('Euro (€)', 0),
			array('Prozent (%)', 1),
		),
		'size' => 1,
		'maxitems' => 1,
		'eval' => ''
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['columns']['type_of_bed'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.type_of_bed',
	'config' => array(
		'type' => 'select',
		'items' => array(
			array('bei 2 Vollzahlern im DZ', 0),
			array('Im eigenen Zimmer', 1),
		),
		'size' => 1,
		'maxitems' => 1,
		'eval' => ''
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_childdiscount']['columns']['in_pricecategory'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_childdiscount.in_pricecategory',
	'config' => array(
		'type' => 'select',
		'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
		'foreign_table_where' => 'AND tx_reisedb_domain_model_pricecategory.sys_language_uid = 0',
		'MM' => 'tx_reisedb_childdiscount_pricecategory_mm',
		'size' => 10,
		'autoSizeMax' => 30,
		'maxitems' => 9999,
		'multiple' => 0,
		'wizards' => array(
			'_PADDING' => 1,
			'_VERTICAL' => 1,
			'edit' => array(
				'type' => 'popup',
				'title' => 'Edit',
				'script' => 'wizard_edit.php',
				'icon' => 'edit2.gif',
				'popup_onlyOpenIfSelected' => 1,
				'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
			),
			'add' => Array(
				'type' => 'script',
				'title' => 'Create new',
				'icon' => 'add.gif',
				'params' => array(
					'table' => 'tx_reisedb_domain_model_pricecategory',
					'pid' => '###CURRENT_PID###',
					'setValue' => 'prepend'
				),
				'script' => 'wizard_add.php',
			),
		),
	),
);