<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageextradata'] = array(
    'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageextradata']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'title',
    ),
    'types' => Array(
        '0' => Array('showitem' => 'fieldtype,dategroup,title,select_fields,'),
        '1' => Array('showitem' => 'fieldtype,dategroup,title,'),
        '2' => Array('showitem' => 'fieldtype,dategroup,title,'),
    ),
    'palettes' => array(

    ),
    'columns' => array(
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),

        'title' => Array (
            'label' => 'Name:',
            'l10n_mode' => '',
            'config' => Array (
                'type' => 'input',
                'size' => '40',
                'max' => '256'
            )
        ),

        'fieldtype' => Array (
            'label' => 'Typ:',
            'l10n_mode' => '',
            'config' => array (
                'type' => 'select',
                'items' => array (
                    array('Auswahl', '0'),
                    array('Freitext', '2'),
                ),
                'size' => 1,
                'maxitems' => 1,
                'default' => '0'
            )
        ),

        'select_fields' => Array (
            'label' => 'Auswahlmöglichkeiten pro Zeile eine',
            'l10n_mode' => '',
            'config' => Array (
                'type' => 'text',
                'cols' => '40',
                'rows' => '15'
            )
        ),

        'dategroup' => Array (
            'label' => 'Gruppe (Für Kalender):',
            'l10n_mode' => '',
            'config' => array (
                'type' => 'select',
                'items' => array (
                    array('Gruppe 1', '1'),
                    array('Gruppe 2', '2'),
                    array('Gruppe 3', '3'),
                    array('Gruppe 4', '4'),
                    array('Gruppe 5', '5'),
                ),
                'size' => 1,
                'maxitems' => 1,
                'default' => '1'
            )
        ),

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),

        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_reisedb_domain_model_eventpackageextradata',
                'foreign_table_where' => 'AND tx_reisedb_domain_model_eventpackageextradata.pid=###CURRENT_PID### AND tx_reisedb_domain_model_eventpackageextradata.sys_language_uid IN (-1,0)',
            ),
        ),

        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
    ),
);