<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_station'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_station']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, code, tracklength, tracklength_approx, description, image, additional_night_description, city, addtional_night_prices',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, code, tracklength, tracklength_approx, description;;;richtext:rte_transform[mode=ts_links], image, additional_night_description;;;richtext:rte_transform[mode=ts_links], city, addtional_night_prices, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_station',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_station.pid=###CURRENT_PID### AND tx_reisedb_domain_model_station.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'code' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.code',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'tracklength' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.tracklength',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			)
		),
		'tracklength_approx' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.tracklength_approx',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'image',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'additional_night_description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.additional_night_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'city' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'addtional_night_prices' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.addtional_night_prices',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_reisedb_domain_model_price',
				'foreign_field' => 'station',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		
		'trip' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['types'] = array(
	'1' => array('showitem' => '--palette--;;donotshow, --palette--;;allgemein, image, description;;;richtext:rte_transform[mode=ts_links],
	--div--;Zusatznacht, additional_night_description;Informationen zur Zusatznacht;;richtext:rte_transform[mode=ts_links], addtional_night_prices;Preise, '),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['palettes'] = array(
	'allgemein' => array('showitem' => 'title;Überschrift, --linebreak--, code, tracklength;Streckenlänge Absolut, tracklength_approx;Streckenlänge ca., city;Stadt,', 'canNotCollapse' => 1),
	'donotshow' => array('showitem' => 'sorting, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['code'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.code',
	'config' => array(
		'type' => 'input',
		'size' => 5,
		'eval' => 'trim'
	),
);


$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['sorting'] = array(
	'config' => array(
		'type' => 'input',
		'size' => 30,
		'eval' => 'int'
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['tracklength'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.tracklength',
	'config' => array(
		'type' => 'input',
		'size' => 5,
		'eval' => 'double2'
	)
);
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['tracklength_approx'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.tracklength_approx',
	'config' => array(
		'type' => 'input',
		'size' => 5,
		'eval' => 'trim'
	),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['city'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.city',
	'config' => array(
		'type' => 'input',
		'size' => 10,
		'eval' => 'trim'
	),
);
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['addtional_night_prices'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.addtional_night_prices',
	'config' => array(
		'type' => 'inline',
		'foreign_table' => 'tx_reisedb_domain_model_price',
		'foreign_field' => 'station',
		'foreign_record_defaults' => array(
			'type' => 0,
			'year' => date('Y'),
		),
		'maxitems'      => 9999,
		'appearance' => array(
			'collapseAll' => 0,
			'levelLinksPosition' => 'top',
			'showSynchronizationLink' => 1,
			'showPossibleLocalizationRecords' => 1,
			'showAllLocalizationLink' => 1
		),
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['image'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_station.image',
	'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
		'image',
		array(
			'maxitems' => 1,
			'appearance' => array(
				'useSortable' => TRUE,
				'headerThumbnail' => array(
					'field' => 'uid_local',
					'width' => '45',
					'height' => '45c',
				),
				'showPossibleLocalizationRecords' => TRUE,
				'showRemovedLocalizationRecords' => TRUE,
				'showSynchronizationLink' => TRUE,
				'showAllLocalizationLink' => TRUE,

				'enabledControls' => array(
					'info' => TRUE,
					'new' => FALSE,
					'dragdrop' => TRUE,
					'sort' => FALSE,
					'hide' => TRUE,
					'delete' => TRUE,
					'localize' => TRUE,
				),
			),
		),
		$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['code']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['tracklength']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['tracklength_approx']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['addtional_night_prices']['l10n_mode'] = 'exclude';

$GLOBALS['TCA']['tx_reisedb_domain_model_station']['columns']['addtional_night_prices']['config']['appearance']['collapseAll'] = 1;