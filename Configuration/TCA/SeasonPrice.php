<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, value, type, from_date, to_date, in_pricecategory',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, value, type, from_date, to_date, in_pricecategory, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_reisedb_domain_model_seasonprice',
				'foreign_table_where' => 'AND tx_reisedb_domain_model_seasonprice.pid=###CURRENT_PID### AND tx_reisedb_domain_model_seasonprice.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'value' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.value',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			)
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'from_date' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.from_date',
			'config' => array(
				'type' => 'input',
				'size' => 7,
				'eval' => 'date',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'to_date' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.to_date',
			'config' => array(
				'type' => 'input',
				'size' => 7,
				'eval' => 'date',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'in_pricecategory' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.in_pricecategory',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
				'MM' => 'tx_reisedb_seasonprice_pricecategory_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_reisedb_domain_model_pricecategory',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		
		'trip' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['types'] = array(
	'1' => array('showitem' => '--palette--;;donotshow, --palette--;;date, --palette--;;price, in_pricecategory;Preiskategorien'),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['palettes'] = array(
	'1' => array('showitem' => ''),
	'date' => array('showitem' => 'from_date;Von, to_date;Bis', 'canNotCollapse' => 1),
	'price' => array('showitem' => 'value;Betrag (Positiv = Zuschlag / Negativ = Abschlag), type;Art', 'canNotCollapse' => 1),
	'donotshow' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, starttime, endtime', 'isHiddenPalette' => TRUE),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['columns']['value'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.value',
	'config' => array(
		'type' => 'input',
		'size' => 20,
		'eval' => 'double2'
	)
);

$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['columns']['type'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.type',
	'config' => array(
		'type' => 'select',
		'items' => array(
			array('Euro (€)', 0),
			array('Prozent (%)', 1),
		),
		'size' => 1,
		'maxitems' => 1,
		'eval' => ''
	),
);

$GLOBALS['TCA']['tx_reisedb_domain_model_seasonprice']['columns']['in_pricecategory'] = array(
	'exclude' => 1,
	'label' => 'LLL:EXT:reisedb/Resources/Private/Language/locallang_db.xlf:tx_reisedb_domain_model_seasonprice.in_pricecategory',
	'config' => array(
		'type' => 'select',
		'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
		'foreign_table_where' => 'AND tx_reisedb_domain_model_pricecategory.sys_language_uid = 0',
		'MM' => 'tx_reisedb_seasonprice_pricecategory_mm',
		'size' => 10,
		'autoSizeMax' => 30,
		'maxitems' => 9999,
		'multiple' => 0,
		'wizards' => array(
			'_PADDING' => 1,
			'_VERTICAL' => 1,
			'edit' => array(
				'type' => 'popup',
				'title' => 'Edit',
				'script' => 'wizard_edit.php',
				'icon' => 'edit2.gif',
				'popup_onlyOpenIfSelected' => 1,
				'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
			),
			'add' => Array(
				'type' => 'script',
				'title' => 'Create new',
				'icon' => 'add.gif',
				'params' => array(
					'table' => 'tx_reisedb_domain_model_pricecategory',
					'pid' => '###CURRENT_PID###',
					'setValue' => 'prepend'
				),
				'script' => 'wizard_add.php',
			),
		),
	),
);