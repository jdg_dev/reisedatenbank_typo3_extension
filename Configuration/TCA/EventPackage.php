<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackage'] = array(
    'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_eventpackage']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'title',
    ),
    'types' => Array(
        '0' => Array('showitem' => 'hidden,--palette--;Name;name,--palette--;Allgemeine Einstelungen;settings,extra_data,typeofprice,--div--;Kalender,availability'),
        '1' => Array('showitem' => 'hidden,--palette--;Name;name,--palette--;Allgemeine Einstelungen;settings,extra_data,typeofprice,fixprice,--div--;Kalender,availability'),
        '2' => Array('showitem' => 'hidden,--palette--;Name;name,--palette--;Allgemeine Einstelungen;settings,extra_data,typeofprice,scaledprice,--div--;Kalender,availability'),
        '3' => Array('showitem' => 'hidden,--palette--;Name;name,--palette--;Allgemeine Einstelungen;settings,extra_data,typeofprice,roomprice,--div--;Kalender,availability'),
    ),
    'palettes' => array(
        'name' => array('showitem' => '
			title, city, --linebreak--, description;;;richtext:rte_transform[mode=ts_links], --linebreak--, detailpage
		', 'canNotCollapse' => 1),
        'settings' => array('showitem' => '
			extra_nights, group_package
		', 'canNotCollapse' => 1),
    ),
    'columns' => array(
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),

        'title' => Array(
            'label' => 'Name:',
            'l10n_mode' => '',
            'config' => Array(
                'type' => 'input',
                'size' => '30',
                'max' => '256'
            )
        ),

        'description' => array(
            'exclude' => 1,
            'label' => 'Beschreibung:',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
            ),
        ),

        'city' => Array(
            'label' => 'Stadt:',
            'l10n_mode' => '',
            'config' => Array(
                'type' => 'input',
                'size' => '15',
                'max' => '256'
            )
        ),

        'detailpage' => Array(
            'label' => 'Detailseite:',
            'l10n_mode' => '',
            'config' => Array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'show_thumbs' => '1',
                'wizards' => array(
                    'suggest' => array(
                        'type' => 'suggest'
                    ),
                ),
            )
        ),

        'extra_nights' => Array(
            'label' => 'Zusatznächte:',
            'l10n_mode' => '',
            'config' => Array(
                'type' => 'input',
                'size' => '2',
                'max' => '256',
                'eval' => 'int',
                'default' => '0'
            )
        ),

        'group_package' => Array(
            'label' => 'Gruppenausflug:',
            'l10n_mode' => '',
            'config' => array(
                'type' => 'check',
            )
        ),

        'extra_data' => array(
            'exclude' => 0,
            'label' => 'Extra Daten (werden bei der Buchung vom User abgefragt)',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_reisedb_domain_model_eventpackageextradata',
                'foreign_field' => 'event_package',
                'maxitems' => 9999,
                'appearance' => array(
                    // 'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),

        'typeofprice' => Array(
            'label' => 'Preistyp:',
            'l10n_mode' => '',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('Gratis', '0'),
                    array('Festpreis', '1'),
                    array('Staffelpreise nach Alter', '2'),
                    array('Zimmerpreise', '3'),
                ),
                'size' => 1,
                'maxitems' => 1,
                'default' => '0'
            )
        ),

        'fixprice' => Array(
            'label' => 'Preis:',
            'l10n_mode' => '',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_reisedb_domain_model_eventpackagefixedprice',
                'foreign_field' => 'event_package',
                'maxitems' => 9999,
                'appearance' => array(
                    // 'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),

        'scaledprice' => array(
            'exclude' => 0,
            'label' => 'Preisstaffel',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_reisedb_domain_model_eventpackagescaledprice',
                'foreign_field' => 'event_package',
                'maxitems' => 9999,
                'appearance' => array(
                    // 'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),

        'roomprice' => array(
            'exclude' => 0,
            'label' => 'Zimmer',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_reisedb_domain_model_eventpackageroomprice',
                'foreign_field' => 'event_package',
                'maxitems' => 9999,
                'appearance' => array(
                    // 'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),

        'availability' => Array(
            'label' => 'Termine und Gruppen',
            'l10n_mode' => '',
            'config' => array(
                'type' => 'user',
                'userFunc' => 'JAKOTA\\Reisedb\\TCA\\DateGroups->render',
                'foreign_table' => 'tx_reisedb_domain_model_eventpackageavailability',
                'foreign_field' => 'event_package',
                'foreign_sortby' => 'dayofavailability',
            ),
        ),

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),

        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_reisedb_domain_model_eventpackage',
                'foreign_table_where' => 'AND tx_reisedb_domain_model_eventpackage.pid=###CURRENT_PID### AND tx_reisedb_domain_model_eventpackage.sys_language_uid IN (-1,0)',
            ),
        ),

        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
    ),
);