<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageroomprice'] = array(
    'ctrl' => $GLOBALS['TCA']['tx_reisedb_domain_model_eventpackageroomprice']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'roomtype',
    ),
    'types' => Array(
        '0' => Array('showitem' => '--palette--;;preis'),
    ),
    'palettes' => array(
        'preis' => array('showitem' => '
			roomcategory, pricecategory, price, dategroup
		','canNotCollapse' => 1),
    ),
    'columns' => array(
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),

        'roomcategory' => Array (
            'l10n_mode' => '',
            'label' => 'Zimmer',
            'config' => Array (
                'type' => 'select',
                'foreign_table' => 'tx_reisedb_domain_model_roomcategory',
                'foreign_table_where' => 'AND tx_reisedb_domain_model_roomcategory.sys_language_uid=0',
                'maxitems' => '1',
            )
        ),

        'pricecategory' => Array (
            'l10n_mode' => '',
            'label' => 'Preiskategorie',
            'config' => Array (
                'type' => 'select',
                'foreign_table' => 'tx_reisedb_domain_model_pricecategory',
                'foreign_table_where' => 'AND tx_reisedb_domain_model_pricecategory.sys_language_uid=0',
                'maxitems' => '1',
            )
        ),

        'price' => Array (
            'label' => 'Preis:',
            'l10n_mode' => '',
            'config' => Array (
                'type' => 'input',
                'size' => '10',
                'max' => '256',
                'eval' => 'double2',
            )
        ),

        'dategroup' => Array (
            'label' => 'Gruppe (Für Kalender):',
            'l10n_mode' => '',
            'config' => array (
                'type' => 'select',
                'items' => array (
                    array('Gruppe 1', '1'),
                    array('Gruppe 2', '2'),
                    array('Gruppe 3', '3'),
                    array('Gruppe 4', '4'),
                    array('Gruppe 5', '5'),
                ),
                'size' => 1,
                'maxitems' => 1,
                'default' => '1'
            )
        ),

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),

        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_reisedb_domain_model_eventpackageroomprice',
                'foreign_table_where' => 'AND tx_reisedb_domain_model_eventpackageroomprice.pid=###CURRENT_PID### AND tx_reisedb_domain_model_eventpackageroomprice.sys_language_uid IN (-1,0)',
            ),
        ),

        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
    ),
);