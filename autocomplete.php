<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 08.12.16
 * Time: 10:50
 */

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Content-Type: application/json", FALSE);

$config = (include '../../LocalConfiguration.php');

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);
$targetDB = new PDO("mysql:host=" . $config['DB']['host'] . ";dbname=" . $config['DB']['database'],
    $config['DB']['username'],
    $config['DB']['password'],
    $options);
$targetDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$responseObject = new \stdClass();
$responseObject->query = $_GET['query'];
$responseObject->queryfallback = '%'.$_GET['query'].'%';
$responseObject->codesearch = str_replace(array('ä', 'ö', 'ü'), array('Ä', 'Ö', 'Ü'), strtoupper($_GET['query'])).'%';
$responseObject->suggestions = array();

if (preg_match('/^[A-z|Ü|ü|Ä|ä|Ö|ö|ß]{2,5}[ |-]{0,1}[0-9|A-z|Ü|ü|Ä|ä|Ö|ö|ß]{0,3}/', $responseObject->query)) {
    // Ist wohl ein reisecode
    $sql = "SELECT uid, title, code FROM `tx_reisedb_domain_model_trip`
WHERE code LIKE :queryfallback AND sys_language_uid = 0 AND deleted = 0 AND hidden = 0 AND pid = 13 ORDER BY code DESC LIMIT 10";
    $res = $targetDB->prepare($sql);
    $res->execute(array(':queryfallback' => $responseObject->codesearch));
    $result = $res->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $item) {
        $responseObject->suggestions[] = $item['code'];
    }
}

if (count($responseObject->suggestions) == 0) {
    $sql = "SELECT uid, title, MATCH (title, code, description, seo_title, seo_keywords, seo_description)
    AGAINST (:query IN Natural language mode with query expansion) as relevance_fine, MATCH (title, code, description, seo_title, seo_keywords, seo_description)
    AGAINST (:query IN Natural language mode with query expansion) as relevance_full FROM `tx_reisedb_domain_model_trip` WHERE (MATCH (title, code, description, seo_title, seo_keywords, seo_description)
    AGAINST (REPLACE(:query, ' ', '* ') IN BOOLEAN MODE) OR title LIKE :queryfallback) AND sys_language_uid = 0 AND deleted = 0 AND hidden = 0 AND pid = 13 ORDER BY relevance_fine, relevance_full DESC LIMIT 10";
    $res = $targetDB->prepare($sql);
    $res->execute(array(':query' => $responseObject->query, ':queryfallback' => $responseObject->queryfallback));
    $result = $res->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $item) {
        $responseObject->suggestions[] = $item['title'];
    }
}



if (count($responseObject->suggestions) == 0) $responseObject->suggestions[] = '-';

echo json_encode($responseObject);